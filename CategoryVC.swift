//
//  CategoryVC.swift
//  MayaStojan
//

import Foundation

import UIKit
import SVProgressHUD

class CategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
}

class CategoryVC: UITableViewController {
    
    @IBOutlet weak var btnAdd: UIBarButtonItem!
    let segueGallery = "segueGallery"
    let segueCategory = "segueCategory"
    let segueEdit = "segueEdit"
    var category :[CategoryModel] = []
    
    var needRefresh = false
    
    var selectedIndex:Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0 , width: 50, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "logo_slim.png")
        imageView.image = image
        
        navigationItem.titleView = imageView
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back_arrow.png"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(popSelf), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 15, y: 20, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        if(ADMIN){
            self.btnAdd.tintColor = UIColor.white
            self.btnAdd.isEnabled = true
        }else{
            self.btnAdd.tintColor = UIColor.black
            self.btnAdd.isEnabled = false
        }
        
        self.loadCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(needRefresh){
            self.tableView.reloadData()
        }
    }
    func loadCategories(){
        
        let tag = String(format:"category/%d", KAppID)
        SVProgressHUD.show()
        NetworkManager.sharedClient.getRequest(tag: tag, parameters: [:]) { (error, result) in
            SVProgressHUD.dismiss()
            
            if (error != "") {
                self.showAlert(withTitle: "Error", message: error)
            }
            else{
                self.category.removeAll()
                var items = result.value(forKey: "categories") as! Array<Dictionary<String, AnyObject>>
                
                for i in 0 ..< items.count {
                    let item = (items[i] as Dictionary<String, AnyObject>)
                    let model = CategoryModel()
                    model.initWithDictionary(data: item)
                    self.category.append(model)
                    
                }
                self.tableView.reloadData()
            }
        }
        
    }
    
    func deleteCategories(){
        let tag = String(format:"category/%d", KAppID)
        let model = self.category[selectedIndex]
        let params = ["id":model.categoryId,"app_name":KAppName] as [String : Any]
        SVProgressHUD.show()
        NetworkManager.sharedClient.deleteRequest(tag: tag, parameters: params) { (error, result) in
            SVProgressHUD.dismiss()
            
            if (error != "") {
                self.showAlert(withTitle: "Error", message: error)
            }
            else{
                self.category.remove(at: self.selectedIndex)
                let indexPath = IndexPath(item: self.selectedIndex, section: 0)
                self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            }
        }
    }
    
    func showConfirmDialog(){
        let alert = UIAlertController(title: "Warning!", message: "Are you sure to delete this category?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.deleteCategories()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        present(alert, animated: true, completion: nil)
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return category.count
    }
    

    
    func popSelf() {
        self.dismiss(animated: true, completion: nil);
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as! CategoryTableViewCell
        
        let model = category[indexPath.row]
        cell.label?.text = model.title
        
        let imageUrl  =  self.downloadUrlWithFileName(fileName: model.image)
        cell.categoryImageView.sd_setImage(with: imageUrl)
        return cell
    }
    
    // method to run when table view cell is tapped
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: segueGallery,sender:self)
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return ADMIN
    }
//    
//    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
//        return UITableViewCellEditingStyle.delete
//    }
//    
//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        selectedIndex = indexPath.row
//        performSegue(withIdentifier: segueEdit,sender:self)
//    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let btnEdit = UITableViewRowAction.init(style: UITableViewRowActionStyle.default, title: "Edit") { (action, indexPath) in
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: self.segueEdit,sender:self)
        }
        btnEdit.backgroundColor = UIColor.blue
        let btnDelete = UITableViewRowAction.init(style: UITableViewRowActionStyle.default, title: "Delete") { (action, indexPath) in
            self.selectedIndex = indexPath.row
            self.showConfirmDialog()
        }
        btnDelete.backgroundColor = UIColor.red
        return [btnEdit,btnDelete]
    }
    
    // This function is called before the segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // get a reference to the second view controller
        if (segue.identifier == segueGallery) {
            let targetVC = segue.destination as! GalleryVC
            
            let model = self.category[selectedIndex]
            targetVC.categoryModel = model
            needRefresh = false
          
        }else if(segue.identifier == segueCategory){
            let targetVC = segue.destination as! CategoryLogoVC
            targetVC.categoryVC = self
            needRefresh = true
        }else if(segue.identifier == segueEdit){
            let targetVC = segue.destination as! CategoryLogoVC
            targetVC.categoryVC = self
            targetVC.categoryModel = self.category[selectedIndex]
            targetVC.editIndex = selectedIndex
            needRefresh = true
        }
    }
    
    @IBAction fileprivate func menuButtonPressed(_ buttn: UIButton) {
        self.dismiss(animated: true, completion: nil);
    }
    
    
}
