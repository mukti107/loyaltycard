//
//  ContactVC.swift
//  MayaStojan
//


import Foundation
import UIKit

class ContactVC: UIViewController, UIWebViewDelegate {
    
    var refreshController: UIRefreshControl!
    
    @IBOutlet weak var webView: UIWebView!
    // This variable will hold the data being passed from the First View Controller
    var receivedString = ""
    
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    var URLString: String = "http://sodamodels.com/static/api/loyaltycard/content/contact.html"
    
    func refresh(sender:AnyObject) {
        // Code to refresh table view
        print("Reloading...")
        
        webView.reload()
        self.refreshController.removeFromSuperview()
        self.refreshController.endRefreshing()
    }
    
    @objc private func refreshWebView() {
        print("Reloading...")
        
        webView.reload()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        webView.backgroundColor = UIColor.clear
        refreshController = UIRefreshControl()
        refreshController.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshController.backgroundColor  = UIColor.clear
        refreshController.addTarget(self, action: #selector(ContactVC.refreshWebView), for: .valueChanged)
        refreshController.attributedTitle = NSAttributedString(string: "Pull down to refresh...")
        webView.scrollView.addSubview(refreshController)
        
        
        if let url = URL(string: URLString) {
            let request = URLRequest(url: url)
            
            let status = Reach().connectionStatus()
            switch status {
            case .unknown, .offline:
                print("Not connected")
                
                /*
                // Create the alert controller
                let alertController = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                    self.dismiss(animated: true, completion: nil);
                }
                // Add the actions
                alertController.addAction(okAction)
                //alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
                
                */
                
                switch receivedString{
                    
                case "http://sodamodels.com/static/api/loyaltycard/content/contact.html":// CONTACT
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"content_img")
                    
                    break;
                default: break
                    
                    
                }
                
            case .online(.wwan):
                
                print("Connected via WWAN")
                
                print("Internet Connection Available!")
                
                switch receivedString{
                    
                case "http://sodamodels.com/static/api/loyaltycard/content/contact.html":// CONTACT
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"content_img")
                    
                    break;
                default: break
                }
                webView.loadRequest(request)
                
                
            case .online(.wiFi):
                print("Connected via WiFi")
                print("Internet Connection Available!")
                
                switch receivedString{
                    
                case "http://sodamodels.com/static/api/loyaltycard/content/contact.html":// CONTACT
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"content_img")
                    
                    break;
                default: break
                    
                }
                webView.loadRequest(request)
            }
        }
        
        /*
         if Reachability.isConnectedToNetwork() == true {
         print("Internet Connection Available!")
         
         
         if (receivedString=="http://getthinapp.com/about.html"){
         
         self.textLabel.text = "ABOUT"
         
         }else if (receivedString=="http://getthinapp.com/contact.html"){
         
         self.textLabel.text = "CONTACT"
         imageView.image=UIImage(named:"contact_image")
         
         }else{
         
         self.textLabel.text = "FAQ'S"
         }
         
         webView.loadRequest(request)
         
         
         } else {
         
         print("Internet Connection not Available!")
         
         
         if (receivedString=="http://getthinapp.com/about.html"){
         
         self.textLabel.text = "ABOUT"
         
         let url = Bundle.main.url(forResource: "about", withExtension:"html")
         let request = NSURLRequest(url: url!)
         webView.loadRequest(request as URLRequest)
         
         }else if (receivedString=="http://getthinapp.com/contact.html"){
         
         self.textLabel.text = "CONTACT"
         imageView.image=UIImage(named:"contact_image")
         
         
         let url = Bundle.main.url(forResource: "contact", withExtension:"html")
         let request = NSURLRequest(url: url!)
         webView.loadRequest(request as URLRequest)
         
         
         }else{
         
         self.textLabel.text = "FAQ'S"
         
         let url = Bundle.main.url(forResource: "faq", withExtension:"html")
         let request = NSURLRequest(url: url!)
         webView.loadRequest(request as URLRequest)
         }
         }
         */
    }
    
    @IBAction fileprivate func menuButtonPressed(_ buttn: UIButton) {
        self.dismiss(animated: true, completion: nil);
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        refreshController.endRefreshing()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        // SVProgressHUD.dismiss()
        refreshController.endRefreshing()
    }
    
}

