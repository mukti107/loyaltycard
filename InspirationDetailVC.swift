//
//  InspirationDetailVC.swift
//  MayaStojan
//

import Foundation
import UIKit
import SVProgressHUD
import SDWebImage

class InspirationDetailVC: UIViewController , UIWebViewDelegate {

    // This variable will hold the data being passed from the First View Controller
    @IBOutlet weak var lblmTitle: UILabel!
    //@IBOutlet weak var lblmContents: UILabel!
    @IBOutlet weak var lblmAuthor: UILabel!
    @IBOutlet weak var lblmLink: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var readMoreBtn: UIButton!
    
    var mID: String!
    var mTitle: String!
    var mContent: String!
    var mAuthor: String!
    var mImage: String!
    var mLink: String!
    var mReadMore: String!
    
    fileprivate let ShowWKWebViewControllerSegueIdentifer = "showWKWebView"
    
    @IBOutlet weak var webView: UIWebView!

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
     
        webView.loadHTMLString(mContent, baseURL: nil)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        readMoreBtn.backgroundColor = .clear
        readMoreBtn.layer.cornerRadius = 5
        readMoreBtn.layer.borderWidth = 1
     
        readMoreBtn.setTitleColor(UIColor.black, for: UIControlState.normal)
        //readMoreBtn.backgroundColor = UIColor.gray
        readMoreBtn.backgroundColor = UIColor(red: 214/255.0, green: 214/255.0, blue: 214/255.0, alpha: 1.0)

        
        readMoreBtn.layer.borderColor = UIColor.gray.cgColor
        readMoreBtn.titleEdgeInsets = UIEdgeInsetsMake(-10,-10,-10,-10)
        readMoreBtn.contentEdgeInsets = UIEdgeInsetsMake(5,5,5,5)
        readMoreBtn.setTitle(mReadMore, for: .normal)
        
        self.lblmTitle.text = mTitle
        self.lblmAuthor.text = mAuthor
        // self.lblmLink.text = mLink
        //self.lblmContents.text = mContent

        
        if let imageURL = URL(string: mImage ){
            DispatchQueue.main.async {
                
                SDWebImageManager.shared().downloadImage(with: imageURL, options: .continueInBackground, progress: {
                    (receivedSize :Int, ExpectedSize :Int) in
                    
                    self.imageView.image = UIImage(named: "loading_img")
                    
                }, completed: {
                    (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                     self.imageView.image = image
                     self.imageView.contentMode = UIViewContentMode.scaleAspectFill
                })
            }
        }
    }

    @IBAction fileprivate func btnBackPressed(_ buttn: UIButton) {
        self.dismiss(animated: true, completion: nil);
    }
    
    @IBAction fileprivate func homeButtonPressed(_ button: UIButton) {
        self.performSegue(withIdentifier:ShowWKWebViewControllerSegueIdentifer, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // get a reference to the second view controller
        if (segue.identifier == ShowWKWebViewControllerSegueIdentifer) {
            // set a variable in the second view controller with the String to pass
            let viewController = segue.destination as! WKWebViewVC
            viewController.URLString = mLink
        }
    }
}

