//
//  UpdateCardVC.swift
//  MayaStojan
//

import Foundation
import UIKit


class MyCardVC: UIViewController {
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var cardNo: UILabel!
    @IBOutlet weak var phoneNo: UILabel!
    
    var urlString: String!
    
    fileprivate let ShowWKWebViewControllerSegueIdentifer = "showWKWebView"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0 , width: 50, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "logo_slim.png")
        imageView.image = image
        
        navigationItem.titleView = imageView
        
        /*
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back_arrow.png"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(popSelf), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 15, y: 20, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let buttonRight = UIButton.init(type: .custom)
        buttonRight.setImage(UIImage.init(named: ""), for: UIControlState.normal)
        buttonRight.frame = CGRect.init(x: 15, y: 20, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        buttonRight.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15)
        let barButtonRight = UIBarButtonItem.init(customView: buttonRight)
        self.navigationItem.rightBarButtonItem = barButtonRight
        
        */
        
        let userDefaults = UserDefaults.standard
        
        self.cardNo.text = userDefaults.string(forKey: "cardNo");
        
        self.phoneNo.text = userDefaults.string(forKey: "phoneNo");
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back_arrow.png"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(popSelf), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 15, y: 20, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)

        
        self.view.addSubview(button)
        
    }
    
    func popSelf() {
        self.dismiss(animated: true, completion: nil);
    }
    
}

