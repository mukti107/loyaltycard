//
//  NetworkManager.swift
//  TapMeOut
//
//  Created by Kristaps Kuzmins on 3/16/17.
//  Copyright © 2017 Kristaps Kuzmins. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager: NSObject {
    static let sharedClient = NetworkManager()
    var baseURLString: String? = nil
    
    override init() {
        self.baseURLString = KServerURL
        super.init()
    }
    
    func postRequest(tag:String, parameters params:Parameters, completion:@escaping (_ error:String, _ response:NSDictionary)->Void){
        sendRequest(type: .post, urlTag: tag, parameters: params){ (error, result) in
            completion(error, result)
        }

    }
    func getRequest(tag:String, parameters params:Parameters, completion:@escaping (_ error:String, _ response:NSDictionary)->Void){
        sendRequest(type: .get, urlTag: tag, parameters: params){ (error, result) in
            completion(error, result)
        }
        
    }
    func putRequest(tag:String, parameters params:Parameters, completion:@escaping (_ error:String, _ response:NSDictionary)->Void){
        sendRequest(type: .put, urlTag: tag, parameters: params){ (error, result) in
            completion(error, result)
        }
        
    }
    func deleteRequest(tag:String, parameters params:Parameters, completion:@escaping (_ error:String, _ response:NSDictionary)->Void){
        sendRequest(type: .delete, urlTag: tag, parameters: params){ (error, result) in
            completion(error, result)
        }
        
    }
    fileprivate func sendRequest(type:HTTPMethod, urlTag tag:String, parameters params:Parameters, completion:@escaping (_ error:String, _ response:NSDictionary)->Void){

        //URLCache.shared.removeAllCachedResponses()
        
        
        
        let url =  "\(KServerURL)\(tag)"
        let Auth_header    = [ "Authorization1" : "authorize"]
        
       
        //Alamofire.request(url, method:type, parameters: params, encoding: URLEncoding.default, headers: Auth_header)
        Alamofire.SessionManager.default.requestWithoutCache(url, method:type, parameters: params, encoding: URLEncoding.default, headers: Auth_header)
            .validate()
            .responseJSON { (response) in
            
                //URLCache.shared.removeAllCachedResponses()
            if (response.result.value != nil)
            {
                
                let json = response.result.value as! NSDictionary
                print (json)
                let error = json.value(forKey: "error") as! Bool
                
                if !error
                {
                    completion("", json)
                }
                else
                {
                    let error_message = json.value(forKey: "message") as! String
                    completion(error_message,[:])
                }
                
            }
            else{
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Data: \(utf8Text)")
                }
                let statusCode = response.response?.statusCode
                let error = "HTTP Status:\(String(describing: statusCode))"
                completion(error, [:])
            }
        }
    }
    func uploadImageWithParameter(tag:String, file data:[[String : Any]], parameters params:NSDictionary, completion:@escaping (_ error:String, _ response:NSDictionary)->Void, progress:@escaping (_ progress:CGFloat)->Void) {
        let url =  "\(KServerURL)\(tag)"
        let auth_header    = [ "Authorization1" : "authorize"]
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.upload(multipartFormData: { multipartFormData in
           
            for i in 0..<data.count{
                let imageData = data[i]
                var imgData:Data? = nil
                var fileName:String = ""
                
                if let tData = imageData["image"] {
                    imgData = tData as? Data
                    fileName = imageData["name"] as! String
                }
                if (imgData != nil){
                    multipartFormData.append(imgData!, withName: String(format:"f_%d",i), fileName: fileName, mimeType: "image/jpg")
                }
            }

            
            for (key, value) in params {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
        },
        to:url,
        method:.post,
        headers:auth_header)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (p) in
                    progress(CGFloat(p.fractionCompleted))
                    print("Upload Progress: \(p.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    if (response.result.value != nil)
                    {
                        
                        let json = response.result.value as! NSDictionary
                        print (json)
                        
                        let error = json.value(forKey: "error") as! Bool
                        
                        if !error
                        {
                            completion("", json)
                        }
                        else
                        {
                            let error_message = json.value(forKey: "message") as! String
                            completion(error_message,[:])
                        }
                        
                    }
                    else{
                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                            print("Data: \(utf8Text)")
                        }
                        let statusCode = response.response?.statusCode
                        let error = "HTTP Status:\(String(describing: statusCode))"
                        completion(error, [:])
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)  
            }
        }
    }


}
extension Alamofire.SessionManager{
    @discardableResult
    open func requestWithoutCache(
        _ url: URLConvertible,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders? = nil)
        -> DataRequest
    {
        do {
            var urlRequest = try URLRequest(url: url, method: method, headers: headers)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <- Cache disabled
            let encodedURLRequest = try encoding.encode(urlRequest, with: parameters)
            return request(encodedURLRequest)
        } catch {
            print(error)
            return request(URLRequest(url: URL(string: "http://example.com/wrong_request")!))
        }
    }
}
