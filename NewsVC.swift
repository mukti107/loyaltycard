//
//  NewsVC.swift
//  MayaStojan
//

import UIKit
import WebKit
import SVProgressHUD



class NewsVC: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    // This variable will hold the data being passed from the First View Controller
    var receivedString = ""
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
    override func viewDidLoad() {
    super.viewDidLoad()
    
    webView.delegate = self
    
    if let url = URL(string: receivedString) {
        
    let request = URLRequest(url: url)
        
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            print("Not connected")
            
                // Create the alert controller
                let alertController = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", preferredStyle: .alert)
            
                // Create the actions
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                    self.dismiss(animated: true, completion: nil);
                }
         
            
                // Add the actions
                alertController.addAction(okAction)
                //alertController.addAction(cancelAction)
            
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
            
                case .online(.wwan):
                print("Connected via WWAN")
                print("Internet Connection Available!")
                webView.loadRequest(request)
            
            case .online(.wiFi):
                print("Connected via WiFi")
                print("Internet Connection Available!")
                webView.loadRequest(request)
            }
        }
    }


    @IBAction fileprivate func backButtonPressed(_ buttn: UIButton) {
        self.dismiss(animated: true, completion: nil);
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        SVProgressHUD.show(withStatus: "Loading...")

    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        
        SVProgressHUD.dismiss()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
         SVProgressHUD.dismiss()
    }
}

/*
class NewsVC: UIViewController , WKNavigationDelegate  {
    
    var receivedString = ""
    
    var URLString = ""
    
    @IBOutlet var backView: UIView!
    
    fileprivate var currentURL: URL?
    
    var webView: WKWebView?
    
    fileprivate let SCREEN_SIZE = UIScreen.main.bounds
    fileprivate let GAP_BETWEEN_VIEWS:CGFloat = 0.08
    
    
    override func viewDidDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
    func setupWebView() {
        
        webView = WKWebView()
        self.view = webView!
        webView!.navigationDelegate = self
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupWebView()
        
        let heightOfSubView = SCREEN_SIZE.height / 2 - SCREEN_SIZE.height * GAP_BETWEEN_VIEWS/2
        let widthOfSubView = SCREEN_SIZE.width / 2 - SCREEN_SIZE.height * GAP_BETWEEN_VIEWS/2
        
        backView = UIView(frame: CGRect(x: widthOfSubView + (SCREEN_SIZE.height * GAP_BETWEEN_VIEWS), y: heightOfSubView + (SCREEN_SIZE.height * GAP_BETWEEN_VIEWS), width: widthOfSubView, height: heightOfSubView))
        
        self.view.addSubview(backView)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapBlurButton))
        tapGestureRecognizer.numberOfTapsRequired = 3
        self.backView?.addGestureRecognizer(tapGestureRecognizer)
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
      //  let URLString = Settings.appSettings.baseURL.absoluteString + self.URLString
      //  self.reloadWebViewWithURLString(URLString)

        self.reloadWebViewWithURLString(receivedString)
        
    }
    
    func tapBlurButton(_ sender: UITapGestureRecognizer) {
        print("BackPressMenu")
        /*if let currentURLPath = self.currentURL?.path where currentURLPath == "/welcome/mobile" ||
         currentURLPath == "/auth/login" || currentURLPath == "/auth/register" {
         self.dismissViewControllerAnimated(true, completion: nil)
         }*/
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func backViewPressed() {
        if let currentURLPath = self.currentURL?.path  ,           currentURLPath == "/auth/login" || currentURLPath == "/auth/register" {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    fileprivate func reloadWebViewWithURLString(_ string: String) {
        guard let requestURL = URL(string: string) else {
            return
        }
        
        let request = URLRequest(url: requestURL, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 15.0)
        //self.webView?.load(request)
        self.webView!.load(request)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        SVProgressHUD.show(withStatus: "Loading...")
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        SVProgressHUD.dismiss()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if (navigationAction.request.url!.absoluteString.hasPrefix("completed://")) {
            self.navigationController!.popViewController(animated: true)
        }
        
        decisionHandler(WKNavigationActionPolicy.allow)
    }
    
    @IBAction fileprivate func backButtonPressed(_ buttn: UIButton) {
        self.dismiss(animated: true, completion: nil);
    }
}
*/



