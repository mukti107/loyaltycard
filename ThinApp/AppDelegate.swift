import UIKit
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import Alamofire
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate{
    
    var window: UIWindow?
    
    var deviceToken: String!
    var deviceOS: String!

    var manager=CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            let authOptions : UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_,_ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        watchLocation()
        
        
        // [END register_for_notifications]
        
        FIRApp.configure()
        
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: .firInstanceIDTokenRefresh,
                                               object: nil)
        
        //Fabric.with([Crashlytics.self])
        
        UINavigationBar.appearance().barTintColor = UIColor.black
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
    
        return true
    }
    
    func watchLocation(){
        manager.delegate=self
        manager.requestAlwaysAuthorization()
        manager.allowsBackgroundLocationUpdates=true
        manager.pausesLocationUpdatesAutomatically=false
        manager.desiredAccuracy=kCLLocationAccuracyBest
        manager.distanceFilter=100
        manager.startUpdatingLocation()
        manager.startMonitoringSignificantLocationChanges()
    }

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locationValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        //
        
        do{
            let url=URL(string:Settings.baseURL+"/index.php?command=getLocations")
            let data = try Data(contentsOf: url!)
            
            let locations=try JSONSerialization.jsonObject(with: data, options:[] ) as! [[String:String]]
            print(manager.monitoredRegions)
            
            var identifiers:[String]=[]
            var activeIdentifiers:[String]=[]
            
            for region in manager.monitoredRegions{
                identifiers.append(region.identifier)
            }
            
            for location in locations {
                let identifier = location["name"]!//"location_\(location["id"]!)"
                let position=CLLocationCoordinate2D(latitude:Double(location["latitude"]!)!,longitude:Double(location["longitude"]!)!)
                if !identifiers.contains(identifier){
                    let region = CLCircularRegion(center:position,radius:300,identifier:identifier)
                    region.notifyOnEntry=true
                    region.notifyOnExit=true
                    manager.startMonitoring(for: region)
                }
                activeIdentifiers.append(identifier)
            }
            
            for region in manager.monitoredRegions{
                if !activeIdentifiers.contains(region.identifier){
                    manager.stopMonitoring(for: region)
                }
            }
        }catch{}
        
        
        //if let nearby=UserDefaults.standard.string(forKey:"nearby_notification"){
        //    let message=nearby.replacingOccurrences(of: "$name", with: "test name")
        //    showNotification("", message)
        //}
        
        //showNotification("Location Updated", "\(locationValue.latitude),\(locationValue.longitude)")
        //print(locationValue)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        //showNotification("", "You are nearby \(region.identifier), If you would love to stop by")
        
        if let nearby=UserDefaults.standard.string(forKey:"nearby_notification"){
            let availableTickets=UserDefaults.standard.integer(forKey: "tickets")
            let message=nearby.replacingOccurrences(of: "$name", with: region.identifier).replacingOccurrences(of: "$num", with: "\(availableTickets)" )
            showNotification("", message)
        }
        
    }
    
    
    func showNotification(_ title:String, _ message:String){
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey:
            title, arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey:
            message, arguments: nil)
        
        // Deliver the notification in five seconds.
        content.sound = UNNotificationSound.default()
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5,
                                                        repeats: false)
        // Schedule the notification.
        let request = UNNotificationRequest(identifier: "location", content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request, withCompletionHandler: nil)
        
    }

    
    
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        print("Message ID: \(userInfo["gcm.message_id"]!)")
        
        self.handellEvent(userInfo["gcm.notification.eventType"] as! String)
        
        // Print full message.
        print("userINFO: %@", userInfo)
        
        
        
    }
    
    
    
    /*
    // [START refresh_token]
    func tokenRefreshNotification(notification: NSNotification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    // [END refresh_token]
    */
    
    // [START connect_to_fcm]
    func connectToFcm() {
        FIRMessaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
                
            }
        }
    }
    // [END connect_to_fcm]
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFcm()
    }
    
    // [START disconnect_from_fcm]
    func applicationDidEnterBackground(_ application: UIApplication) {
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }
    // [END disconnect_from_fcm]
    
    func printFCMToken() {
        if let token = FIRInstanceID.instanceID().token() {
            print("Your FCM token is \(token)")
            
            let userDefaults = UserDefaults.standard
            userDefaults.set(token, forKey: "deviceToken")
            userDefaults.synchronize()
            
        } else {
            print("You don't yet have an FCM token.")
        }
    }
    
    func tokenRefreshNotification(_ notification: NSNotification?) {
        if let token=FIRInstanceID.instanceID().token() {
            printFCMToken()
            // Do other work here like sending the FCM token to your server
            
            let params = ["command":"storeFCMToken", "udid": UIDevice.current.identifierForVendor?.uuidString ?? "" , "token":token ] as [String : Any]
            
            Alamofire.request(Settings.baseURL+"/index.php", method: .get, parameters: params).response { (DefaultDataResponse) in
            }
    
            //postData()
            
        } else {
            print("We don't have an FCM token yet")
            
            connectToFcm()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
       
        //FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.prod)
        
        //FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
        
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.unknown)
        
        /// GET refreshToken FIREBASE FOR PUSH
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            
            print("InstanceID token: \(refreshedToken)")
            
            let userDefaults = UserDefaults.standard
            userDefaults.set(refreshedToken, forKey: "deviceToken")
            userDefaults.synchronize()
            
            postData()
            
            // [START subscribe_topic]
            FIRMessaging.messaging().subscribe(toTopic: "/topics/ios")
            print("Subscribed to news topic")
            // [END subscribe_topic]
            
            
        }
        
    }
    
    func postData(){
        
        let userDefaults = UserDefaults.standard
        
        deviceToken = userDefaults.string(forKey: "deviceToken");
        
        deviceOS = "iOS";
        
        print("deviceToken: ",deviceToken);
        
        print("deviceOS: ",deviceOS);
        
        let params = [ "deviceToken": deviceToken , "deviceOS":deviceOS ] as [String : Any]
        
        Alamofire.request("http://sodamodels.com/static/api/loyaltycard/pushnotifications/apiDeviceToken.php", method: .post, parameters: params).response { (DefaultDataResponse) in
            
        }
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    
        // Print message ID.
        
        if let eventType = notification.request.content.userInfo["gcm.notification.eventType"] as? String{
            self.handellEvent(eventType)
        }
    
        
//
//        // Print full message.
//        print("%@", userInfo)
//        
//        var body = ""
//        var title = ""
//        
//        
//        guard let aps = userInfo["aps"] as? [String : AnyObject] else {
//            print("Error parsing aps")
//            return
//        }
//        
//        if let alert = aps["alert"] as? String {
//            body = alert
//        } else if let alert = aps["alert"] as? [String : String] {
//            body = alert["body"]!
//            title = alert["title"]!
//        }
//        
//        print("body =", body)
//        
//        print("title =", title)
//        
//        print("BRX IS HERE iOS10 MSG HANDLING")
//        
//        
//        let state = UIApplication.shared.applicationState
//        if state == .background {
//            print("App in Background")
//            
//            print("body =", body)
//            print("title =", title)
//            
//            print("BRX IS HERE iOS10 MSG HANDLING")
//            
//            let userDefaults = UserDefaults.standard
//            userDefaults.set(body, forKey: "message")
//            userDefaults.synchronize()
//            
//        }
//        if state == .active {
//            print("App in active")
//            
//            print("App in Background")
//            
//            print("body =", body)
//            print("title =", title)
//            
//            print("BRX IS HERE iOS10 MSG HANDLING")
//            
//            let userDefaults = UserDefaults.standard
//            userDefaults.set(body, forKey: "message")
//            userDefaults.synchronize()
//            
//        }
//        
//        if state == .inactive {
//            print("App in inactive")
//            print("App in Background")
//            print("body =", body)
//            print("title =", title)
//            print("BRX IS HERE iOS10 MSG HANDLING")
//            
//            let userDefaults = UserDefaults.standard
//            userDefaults.set(body, forKey: "message")
//            userDefaults.synchronize()
//        }
//        
//        let userDefaults = UserDefaults.standard
//        userDefaults.set(body, forKey: "message")
//        userDefaults.synchronize()
        
        completionHandler([.alert, .badge, .sound]) // BRX WILL PROMP USER

    }
    
    
    func handellEvent(_ event:String){
        let values=UserDefaults.standard
        switch  event{
        case "ticketReceived":
            NotificationCenter.default.post(name:NSNotification.Name(rawValue:"ticketReceived"),object: nil)
            
            break
            
        case "rewardReceived":
            NotificationCenter.default.post(name:NSNotification.Name(rawValue:"rewardReceived"),object: nil)
            
            break
        default:
            print(event)
            
        }
    }
    
    
}

extension AppDelegate : FIRMessagingDelegate {
    // Receive data message on iOS 10 devices.
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        
        
        
        
        NotificationCenter.default.post(name:NSNotification.Name(rawValue:"rewardReceived"),object: nil)
        
        
//        print(UIApplication.topViewController())
        print("%@", remoteMessage.appData)
    }
    
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
// [END ios_10_message_handling]
