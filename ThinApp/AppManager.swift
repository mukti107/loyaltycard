//
//  AppManager.swift
//  TheAgency
//
//  Created by mac on 4/17/17.
//  Copyright © 2017 Kristaps Kuzmins. All rights reserved.
//

import UIKit
import CoreLocation

class AppManager: NSObject {
    
    
    static let sharedInstance = AppManager()
    var myLocation = CLLocation(latitude: 0, longitude: 0)
    
    var selectedShopID = 0
    var balance : CGFloat = 0
    var reward : CGFloat = 0
   
    override init() {
        super.init()
        
    }
    
    func setUserId(userId:Int) {
        let defaults = UserDefaults.standard
        defaults.set(userId, forKey: KUserDefaultUserID)
        defaults.synchronize()
    }
    
    func getUserId() -> Int {
        let defaults = UserDefaults.standard
        let _userId = defaults.integer(forKey: KUserDefaultUserID)
        return _userId;
    }
    
    // apiKey
    func setApiKey(apiKey:String) {
        let defaults = UserDefaults.standard
        defaults.set(apiKey, forKey: KUserDefaultApiKey)
        defaults.synchronize()
    }
    func getApiKey()->String{
        let defaults = UserDefaults.standard
        var _apiKey = defaults.string(forKey: KUserDefaultApiKey)
        _apiKey = _apiKey == nil ? "":_apiKey
        return _apiKey!;
    }
    // email
    func setEmail(email:String) {
        let defaults = UserDefaults.standard
        defaults.set(email, forKey: KUserDefaultEmail)
        defaults.synchronize()
    }
    func getEmail() -> String {
        let defaults = UserDefaults.standard
        var _email = defaults.string(forKey: KUserDefaultEmail)
        _email = _email == nil ? "":_email
        return _email!;
    }
    // name
    func setName(name:String) {
        let defaults = UserDefaults.standard
        defaults.set(name, forKey: KUserDefaultUserName)
        defaults.synchronize()
    }
    func getName()->String{
        let defaults = UserDefaults.standard
        var _name = defaults.string(forKey: KUserDefaultUserName)
        _name = _name == nil ? "":_name
        return _name!;
    }
    // phone
    func setPhone(phone:String){
        let defaults = UserDefaults.standard
        defaults.set(phone, forKey: KUserDefaultPhone)
        defaults.synchronize()
    }
    func getPhone() -> String {
        let defaults = UserDefaults.standard
        var _phone = defaults.string(forKey: KUserDefaultPhone)
        _phone = _phone == nil ? "":_phone
        return _phone!;
    }
    // photo
    func setPhoto(photo:String) {
        let defaults = UserDefaults.standard
        defaults.set(photo, forKey: KUserDefaultPhoto)
        defaults.synchronize()
    }
    func getPhoto() -> String {
        let defaults = UserDefaults.standard
        var _photo = defaults.string(forKey: KUserDefaultPhoto)
        _photo = _photo == nil ? "":_photo
        return _photo!
    }
    func getProfileImageUrl()->URL{
        let photo = self.getPhoto()
        var isFullUrl = false
        if let url  = URL(string: photo) {
            if UIApplication.shared.canOpenURL(url) {
                isFullUrl = true
            }
        }
        
        var url:URL
        if(isFullUrl){
            url = URL(string: photo)!
        }else{
            url = AppManager.sharedInstance.downloadUrlWithFileName(fileName: photo)
        }
        return url
    }
    func setLoginStatu(login:Bool){
        let defaults = UserDefaults.standard
        defaults.set(login, forKey: KUserDefaultSignIn)
        defaults.synchronize()
    }
    func getLoginStatus()->Bool{
        let defaults = UserDefaults.standard
        let _loginIn = defaults.bool(forKey: KUserDefaultSignIn)
        return _loginIn;
    }
    
    func setAdminStatu(login:Bool){
        let defaults = UserDefaults.standard
        defaults.set(login, forKey: KUserAdmin)
        defaults.synchronize()
    }
    func getAdminStatus()->Bool{
        let defaults = UserDefaults.standard
        let _loginIn = defaults.bool(forKey: KUserAdmin)
        return _loginIn;
    }
    
    func setKeepLogin(isKeepLogin:Bool){
        let defaults = UserDefaults.standard
        defaults.set(isKeepLogin, forKey: KUserDefaultKeepSign)
        defaults.synchronize()
    }
    func getKeepLogin()->Bool{
        let defaults = UserDefaults.standard
        let _isKeepLogin = defaults.bool(forKey: KUserDefaultKeepSign)
        return _isKeepLogin;
    }
    
    func setRealTimeTrack(val:Bool){
        let defaults = UserDefaults.standard
        defaults.set(val, forKey: KUserDefaultRealTimeTrack)
        defaults.synchronize()
    }
    func getRealTimeTrack()->Bool{
        let defaults = UserDefaults.standard
        let _initVal = defaults.string(forKey: KUserDefaultRealTimeTrack)
        if _initVal == nil {
            setRealTimeTrack(val: true)
            return true
        }
        let _val = defaults.bool(forKey: KUserDefaultRealTimeTrack)
        return _val;
    }
    func setBackgroundTrack(val:Bool){
        let defaults = UserDefaults.standard
        defaults.set(val, forKey: KUserDefaultBackgroundTrack)
        defaults.synchronize()
    }
    func getBackgroundTrack()->Bool{
        let defaults = UserDefaults.standard
        let _initVal = defaults.string(forKey: KUserDefaultBackgroundTrack)
        if _initVal == nil {
            setBackgroundTrack(val: true)
            return true
        }
        let _val = defaults.bool(forKey: KUserDefaultBackgroundTrack)
        return _val;
    }
    func setDefaultLocation(val:String){
        let defaults = UserDefaults.standard
        defaults.set(val, forKey: KUserDefaultLoation)
        defaults.synchronize()
    }
    func getDefaultLocation()->String{
        let defaults = UserDefaults.standard
        let _val = defaults.string(forKey: KUserDefaultLoation)
        if _val == nil {
            return ""
        }
        return _val!;
    }
    func setDefaultAddress(val:String){
        let defaults = UserDefaults.standard
        defaults.set(val, forKey: KUserDefaultAddress)
        defaults.synchronize()
    }
    func getDefaultAddress()->String{
        let defaults = UserDefaults.standard
        let _val = defaults.string(forKey: KUserDefaultAddress)
        if _val == nil {
            return "Add your default address"
        }
        return _val!;
    }
}
