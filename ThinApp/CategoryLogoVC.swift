//
//  CategoryLogoVC.swift
//  MayaStojan
//
//  Created by developer on 5/22/17.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit
import TOCropViewController
import SVProgressHUD

class CategoryLogoVC: UIViewController , UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate{

    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var imgview: UIImageView!
    
    var imagePicker = UIImagePickerController()
    var croppedFrame : CGRect = CGRect()
    var angle:Int = 0
    
    var categoryVC = CategoryVC()
    var categoryModel = CategoryModel()
    var editIndex = -1
    
    var imageChanged = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(editIndex > -1){
            self.txtTitle.text = categoryModel.title
            let imageUrl  =  self.downloadUrlWithFileName(fileName: categoryModel.image)
            self.imgview.sd_setImage(with: imageUrl)
        }
        self.txtTitle.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func actionGoBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSave(_ sender: Any) {
        if(self.txtTitle.text == ""){
            self.showAlert(withTitle: "", message: "Please Type Category's Title")
            return
        }
        if(self.imgview.image == nil){
            self.showAlert(withTitle: "", message: "Please Select Category's Image")
            return
        }
        var image = UIImage()
        if(imageChanged == true){
            image = self.imgview.image!
        }
        self.uploadImage(image: image)
    }
    @IBAction func actionAddPhoto(_ sender: Any) {
//        if(self.imgview.image != nil){
//            self.didTapImageView()
//            return
//        }
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // Create and add the Cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            // Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        
        // Create and add first option action
        let takePictureAction = UIAlertAction(title: "Take a photo", style: .default) { action -> Void in
            self.takePhoto(gallery: false)
        }
        actionSheetController.addAction(takePictureAction)
        
        // Create and add a second option action
        let choosePictureAction = UIAlertAction(title: "Select from gallery", style: .default) { action -> Void in
            self.takePhoto(gallery: true)
        }
        actionSheetController.addAction(choosePictureAction)
        
        // We need to provide a popover sourceView when using it on iPad
        //actionSheetController.popoverPresentationController?.sourceView = sender as UIView
        
        // Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        dismiss(animated:true, completion: nil) //5
        self.presentCropViewController(image: chosenImage)
    }
    
    // Custom
    
    func takePhoto(gallery:Bool){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            if gallery {
                imagePicker.sourceType = .photoLibrary;
            }else{
                imagePicker.sourceType = .camera;
            }
            
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func presentCropViewController(image:UIImage)
    {
         let cropViewController = TOCropViewController.init(image: image)
         cropViewController.delegate = self
        self.present(cropViewController, animated: true, completion: nil)
        cropViewController.toolbar.clampButtonHidden = true
        cropViewController.toolbar.rotateButton.isHidden = true
        cropViewController.toolbar.rotateClockwiseButtonHidden = true
        cropViewController.toolbar.rotateCounterclockwiseButtonHidden = true
        cropViewController.cropView.setAspectRatio(CGSize(width: 2.4, height: 1), animated: false)
        cropViewController.cropView.cropBoxResizeEnabled = false
        
    }
    
    func didTapImageView()
    {
        let cropViewController = TOCropViewController.init(image: self.imgview.image!)
        cropViewController.delegate = self
        let viewFrame = self.view.convert(self.imgview.frame, to: self.navigationController?.view)

        cropViewController.presentAnimated(from: self, fromImage: self.imgview.image, fromView: nil, fromFrame: viewFrame, angle: self.angle, toFrame: self.croppedFrame, setup: {
            //self.imgview.isHidden = true
        }, completion: nil)
    }
    
    func uploadImage(image:UIImage){
        
        //let imageName = String(format: "%ld_profile.jpg",AppManager.sharedInstance.getUserId())
        
        var data:[String : Any]
        var imageName:String = ""
        let title = self.txtTitle.text!
        var params = [String:String]()
        if (UIImageJPEGRepresentation(image, 1) != nil) {
            let newImage = self.resizeImage(image: image, newWidth: MAX_IMAGE_WIDTH)
            let imgData = UIImageJPEGRepresentation(newImage, 1)
            imageName = String(format: "c_%.0f.jpg",Date().timeIntervalSince1970)
            data = [ "image": imgData! , "name":imageName ] as [String : Any]
            params = ["app_name": KAppName, "app_id":String(KAppID), "is_category":String(1), "image":imageName, "title":title]
        }else{
            data = [:]
            params = ["app_name": KAppName, "app_id":String(KAppID), "is_category":String(1), "title":title]
        }
        if(categoryModel.categoryId > -1){
            params["id"] = String(categoryModel.categoryId)
        }
        
        
        SVProgressHUD.show()
       
        NetworkManager.sharedClient.uploadImageWithParameter(tag: "gallery", file: [data], parameters: params as NSDictionary, completion: { (error, result) in
            SVProgressHUD.dismiss()
            if (error != "") {
                self.showAlert(withTitle: "Error", message: error)
            }
            else{
                let categoryId = self.getNumberFromData(data: result.value(forKey: "id")!)
                if(imageName != ""){
                    
                    if(self.editIndex > -1){
                        let model = self.categoryVC.category[self.editIndex]
                        model.image = imageName
                        model.title = title
                    }else{
                        let model = CategoryModel()
                        model.categoryId = categoryId
                        model.image = imageName
                        model.title = title
                        self.categoryVC.category.append(model)
                    }
                    
                }else{
                    let model = self.categoryVC.category[self.editIndex]
                    model.image = imageName
                    model.title = title
                }
                
                self.navigationController?.popViewController(animated: true)
            }
        }) { (progress) in
            SVProgressHUD.showProgress(Float(progress))
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension CategoryLogoVC : TOCropViewControllerDelegate{
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        self.croppedFrame = cropRect
        self.angle = angle
        self.imgview.image = image
        imageChanged = true
        self.btnAdd.setTitle("", for: UIControlState.normal)
        dismiss(animated:true, completion: nil) //5
    }
}
