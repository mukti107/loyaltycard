//
//  CategoryModel.swift
//  MayaStojan
//
//  Created by developer on 5/22/17.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit

class CategoryModel: NSObject {
    var categoryId: Int = -1
    var title : String = ""
    var image : String = ""

    func initWithDictionary(data:Dictionary<String, AnyObject>){
        if let val = data["id"] {
            self.categoryId = self.getNumberFromData(data: val)
        }
        
        if let val : String = data["title"] as? String  {
            self.title = val
        }
        if let val: String = data["image"] as? String{
            self.image = val
        }
    }

}
extension NSObject {
    func getNumberFromData(data:Any) -> Int {
        if let strData = data as? String {
            if(strData == ""){
                return 0
            }else{
                return Int("\(String(describing: strData))")!
            }
            
        }
        else if let strData = data as? Int {
            return strData
        }else{
            return 0
        }
    }
    func getDoubleFromData(data:Any) -> Double {
        if let strData = data as? String {
            if(strData == ""){
                return 0
            }else{
                return Double("\(String(describing: strData))")!
            }
        }
        else if let strData = data as? Double {
            return strData
        }else{
            return 0
        }
    }
    func getStringFromData(data:Any) -> String {
        if let strData = data as? String {
            return String(describing: strData)
        }
        else if let strData = data as? Int {
            return String(strData)
        }else{
            return ""
        }
    }
    
}
