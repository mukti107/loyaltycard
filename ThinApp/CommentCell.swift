//
//  CommentCell.swift
//  TheAgency
//
//  Created by developer on 6/20/17.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var txtComment: UITextView!
    
    weak var delegate: CommentTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        imgUser.layer.masksToBounds = true;
        imgUser.layer.cornerRadius = imgUser.frame.size.height / 2;
        imgUser.layer.borderColor = UIColor.white.cgColor
        imgUser.layer.borderWidth = 4
        imgUser.contentMode = .scaleAspectFill
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgUser.isUserInteractionEnabled = true
        imgUser.addGestureRecognizer(tapGestureRecognizer)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.delegate?.commentCellImageTapAt(cell: self)
    }

}
protocol CommentTableViewCellDelegate: class {
    func commentCellImageTapAt(cell: UITableViewCell)
}
