//
//  ContentPageViewController.swift
//  LoyaltyCard
//
//

import Foundation
import UIKit

class ContentPageViewController: UIViewController, UIWebViewDelegate {
    
    var refreshController: UIRefreshControl!
    
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!

    var receivedString = ""
    
    func refresh(sender:AnyObject) {
        // Code to refresh table view
        print("Reloading...")
        
        webView.reload()
        self.refreshController.removeFromSuperview()
        self.refreshController.endRefreshing()
    }
    
    @objc private func refreshWebView() {
        print("Reloading...")
        
        webView.reload()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        webView.backgroundColor = UIColor.clear
        refreshController = UIRefreshControl()
        refreshController.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshController.backgroundColor  = UIColor.clear
        refreshController.addTarget(self, action: #selector(ContentPageViewController.refreshWebView), for: .valueChanged)
        refreshController.attributedTitle = NSAttributedString(string: "Pull down to refresh...")
        webView.scrollView.addSubview(refreshController)
        
        receivedString = "http://sodamodels.com/static/api/loyaltycard/content/statuses.html"
        
        if let url = URL(string: receivedString) {
            let request = URLRequest(url: url)
            
            let status = Reach().connectionStatus()
            switch status {
            case .unknown, .offline:
                print("Not connected")
 
                switch receivedString{
                case "http://sodamodels.com/static/api/loyaltycard/content/statuses.html":// CONTACT
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"status_img")
                    break;
                default: break
                }
                
            case .online(.wwan):
                print("Connected via WWAN")
                print("Internet Connection Available!")
                switch receivedString{
                case "http://sodamodels.com/static/api/loyaltycard/content/statuses.html":// CONTACT
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"status_img")
                    break;
                default: break
                }
                webView.loadRequest(request)
                
            case .online(.wiFi):
                print("Connected via WiFi")
                print("Internet Connection Available!")
                switch receivedString{
                case "http://sodamodels.com/static/api/loyaltycard/content/statuses.html":// CONTACT
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"status_img")
                    break;
                default: break
                }
                webView.loadRequest(request)
            }
        }
           }
    
    @IBAction fileprivate func menuButtonPressed(_ buttn: UIButton) {
        self.dismiss(animated: true, completion: nil);
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        refreshController.endRefreshing()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        // SVProgressHUD.dismiss()
        refreshController.endRefreshing()
    }
    
}


