
import UIKit

class CustomImageFlowLayout: UICollectionViewFlowLayout {

    override init() {
        super.init()
        setupLayout()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }

    override var itemSize: CGSize {
        set {
            
        }
        get {
            let numberOfColumns: CGFloat = 3
            let spacing : CGFloat = 6
            let margin : CGFloat = 6

            let itemWidth = (self.collectionView!.frame.width - (numberOfColumns - 1 ) * spacing - margin) / numberOfColumns
            //return CGSize(width: itemWidth, height: itemWidth)
            
             return CGSize(width: itemWidth, height: 220/155 * itemWidth);
        }
    }

    func setupLayout() {
        minimumInteritemSpacing = 0
        minimumLineSpacing = 2
        scrollDirection = .vertical
        sectionInset = UIEdgeInsets(top: 0, left: 3, bottom: 0, right: 3)
    }

}
