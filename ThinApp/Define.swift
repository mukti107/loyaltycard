//
//  Define.swift
//  TapMeOut
//
//  Created by Kristaps Kuzmins on 3/16/17.
//  Copyright © 2017 Kristaps Kuzmins. All rights reserved.
//

import Foundation
import UIKit

let KAppID = 7                                                              //   APP ID   MAYA
let KAppName = "loyaltycard"
//   App's image folder name  http://sodamodels.com/static/images/users/maya
let KServerURL = "http://sodamodels.com/api/webservices/api/v1/"
let KServerImageURL = "http://sodamodels.com/static/images/users/"



let KUserDefaultUserID = "userId"
let KUserDefaultUserName = "name"
let KUserDefaultApiKey = "apiKey"
let KUserDefaultEmail =  "email"
let KUserDefaultPhone =  "phone"
let KUserDefaultPhoto =  "photo"
let KUserDefaultSignIn =  "signIn"
let KUserAdmin         =  "admin"
let KUserDefaultKeepSign = "isKeep"
let KUserDefaultRealTimeTrack = "track"
let KUserDefaultBackgroundTrack = "background"
let KUserDefaultAddress = "address"
let KUserDefaultLocation = "location"


let KUserDefaultContactList = "contacts"
let KUserDefaultInstall = "install"
let KUserDefaultMessage = "message"
let KUserDefaultLoation = "location"



// Public Profile Variable
let KMeetingWays:[String] = ["After Work","Breakfast","Coffee","Dinner","Lunch","On A Walk","video Call","Voice Call","Weekends"]
let KLookingFors:[String] = ["Colleaborators & talent to hire","Funding","Freelance projects","ideas & inspiration","Job opportunities","Mentorship","Potential investments","New fiends in town"]
let KInterests:[String] = ["Communication","NewMedia","Journalism","Strategiccommunications","SocialNetworking","Marketing"]



