//
//  EventCell.swift
//  TheAgency
//
//  Created by developer on 6/20/17.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var txtContent: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
