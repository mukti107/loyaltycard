//
//  EventDetailVC.swift
//  Ear2Ground
//
//  Created by Bruce Khin on 11/03/2017.
//  Copyright © 2017 RiskEater. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import SDWebImage

class EventDetailVC: UIViewController, UITextFieldDelegate {
    
    // This variable will hold the data being passed from the First View Controller
    @IBOutlet weak var lblmTitle: UILabel!
    @IBOutlet weak var lblmContents: UITextView!
    @IBOutlet weak var lblmAuthor: UILabel!
    @IBOutlet weak var lblmLink: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblTicketsCount: UILabel!
    @IBOutlet weak var viewSelectTicket: UIView!
    @IBOutlet weak var btnSelectTickets: UIButton!
    
    @IBOutlet weak var txtInputPhoneNumber4Ticket: UITextField!
    @IBOutlet weak var txtInputEmail4Ticket: UITextField!
    
    
    var mID: String!
    var mTitle: String!
    var mContent: String!
    var mAuthor: String!
    var mImage: String!
    var mLink: String!
    var mAttendee_id:String!
    var ticketCnt: Int!
    
    var activeField: UITextField?
    var keyboardShown:Bool = false
    var orgSelectTicketCenter:CGFloat = 0
    
    fileprivate let ticketQRCodeVCSgIdentifier = "ticketQRCodeVC"    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0 , width: 50, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "app_logo.png")
        imageView.image = image
        
        navigationItem.titleView = imageView
        
        self.lblmTitle.text = mTitle
        //self.lblmAuthor.text = "by " + mAuthor
        // self.lblmLink.text = mLink
        self.lblmContents.text = mContent
        lblTicketsCount.layer.borderColor = UIColor.white.cgColor
        lblTicketsCount.layer.borderWidth = 2.0
        self.viewSelectTicket.alpha = 0.0
        txtInputPhoneNumber4Ticket.delegate = self
        txtInputEmail4Ticket.delegate = self
        ticketCnt = 0
        if let url = URL(string: mImage ){
            DispatchQueue.main.async {
                SDWebImageManager.shared().downloadImage(with: url, options: .continueInBackground, progress: {
                    (receivedSize :Int, ExpectedSize :Int) in
                    
                    
                    self.imageView.image = UIImage(named: "loading_img")
                    
                }, completed: {
                    (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    self.imageView.image = image
                    self.imageView.contentMode = UIViewContentMode.scaleAspectFill
                })
            }
        }
        
        self.keyboardShown = false
        self.orgSelectTicketCenter = self.viewSelectTicket.center.y
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.deregisterFromKeyboardNotifications()
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
        var touch: UITouch? = touches.first as! UITouch?
        //location is relative to the current view
        // do something with the touched point
        if touch?.view != self.viewSelectTicket {
            if self.viewSelectTicket.alpha == 0{
                return
            }
            self.viewSelectTicket.alpha = 0
            self.btnSelectTickets.isHidden = false
        }

    }
    
    
    @IBAction fileprivate func btnBackPressed(_ buttn: UIButton) {
        self.dismiss(animated: true, completion: nil);
    }
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // get a reference to the second view controller
        if (segue.identifier == ticketQRCodeVCSgIdentifier) {
            // set a variable in the second view controller with the String to pass
            
            
            //let secondViewController = segue.destination as! UINavigationController
            //let targetController = secondViewController.topViewController as! TicketQRCodeVC
            // targetController.URLString = loadString

            let navVC = segue.destination as! UINavigationController
            let viewController = navVC.topViewController as! TicketQRCodeVC

            //   let viewController = segue.destination as! TicketQRCodeVC
            viewController.strEmail = txtInputEmail4Ticket.text!
            viewController.strPhoneNumber = txtInputPhoneNumber4Ticket.text!
            viewController.intTicketCnt = String.init(format: "%d", ticketCnt)
            viewController.strAttendee_id = self.mAttendee_id
        }
    }
   
    @IBAction func increaseTicketCnt(_ sender: Any) {
        ticketCnt = ticketCnt + 1
        lblTicketsCount.text = String.init(format: "%d", ticketCnt)
    }
    @IBAction func decreaseTicketCnt(_ sender: Any) {
        ticketCnt = ticketCnt - 1
        if(ticketCnt < 0){
            ticketCnt = 0
            return
        }
        lblTicketsCount.text = String.init(format: "%d", ticketCnt)
    }
    
    func showSelectTicketView(view: UIView){
        view.isHidden = true
        UIView.animate(withDuration: 2.0, animations: {
            view.center.y -= 246
        })
    }
    
    @IBAction func btnCloseSelectTicketView(_ sender: Any) {
        self.viewSelectTicket.alpha = 0
        self.btnSelectTickets.isHidden = false
    }
    @IBAction func selectTicketViewPresssed(_ sender: Any) {        
        self.btnSelectTickets.isHidden = true
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.viewSelectTicket.alpha = 1.0
        }, completion: nil)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func isValidPhoneNumber(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}\\d{3}\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    func showAlert(title:String, content:String, buttonName:String){
        let alertController = UIAlertController(title: title, message:
            content, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: buttonName, style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func confirmSelectTickets(_ sender: Any) {
        let phoneNumber = txtInputPhoneNumber4Ticket.text! as String
        let email = txtInputEmail4Ticket.text! as String
        if(!self.isValidEmail(testStr: email)){
            self.showAlert(title: "Error", content: "Invalid Email", buttonName: "OK")
            return;
        }
        
        if(!self.isValidPhoneNumber(value: phoneNumber)){
            self.showAlert(title: "Error", content: "Invalid Phone Number", buttonName: "OK")
            return;
        }
        
        var urlString : String = "https://thnapp.com/api/webservices/index.php?command=insertAttendee"
        urlString = urlString + "&device_id=" + UIDevice.current.identifierForVendor!.uuidString
        urlString = urlString + "&email=" + email
        urlString = urlString + "&phone=" + phoneNumber
        urlString = urlString + "&tickets=" + (NSString.init(format: "%d", ticketCnt) as String)
        urlString = urlString + "&event_id=" + mID
        urlString = urlString + "&qr_code=12345"
        
        let targetURL = NSURL(string: urlString)
        SVProgressHUD.show(withStatus: "Connecting...")
        performGetRequest(targetURL as URL!, completion: { (data, HTTPStatusCode, error) -> Void in
            if HTTPStatusCode == 200 && error == nil {
                do {
                    // Convert the JSON data into a dictionary.
                    var resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, AnyObject>
                    self.mAttendee_id = resultsDict["attendee_id"] as! String
                    // Get all playlist items ("items" array).
//                    let result: Int = resultsDict["result"] as! Int
//                    if(result == 1){
                        self.performSegue(withIdentifier: self.ticketQRCodeVCSgIdentifier, sender: self)
//                    }else{
//                        self.showAlert(title: "Server Error", content: "", buttonName: "OK")
//                    }
                } catch {
                    self.showAlert(title: "Error, Can not read from Server", content: "", buttonName: "OK")
                }
            }
            else {
                print("HTTP Status Code = \(HTTPStatusCode)")
                print("Error while loading channel videos: \(String(describing: error))")
            }
            
            
            SVProgressHUD.dismiss()
        })
        
    }
    
    func performGetRequest(_ targetURL: URL!, completion:@escaping (_ data: Data?, _ HTTPStatusCode: Int, _ error: Error?) -> Void) {
        
        var request = URLRequest(url: targetURL)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let sessionConfiguration = URLSessionConfiguration.default
        
        let Session = URLSession(configuration: sessionConfiguration)
        
        let tasksession = Session.dataTask(with: request) { data, response, error in
            if data != nil{
                DispatchQueue.main.async {
                    completion(data, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            else
            {
                print("Connection Lost")
                DispatchQueue.main.async {
                    completion(nil, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            
        }
        tasksession.resume()
    }
    
    
    ///For Keyboard auto moving.
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }

    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func adjustingHeight(show:Bool, notification:NSNotification) {
        if(show == keyboardShown){
            return
        }
        // 1
        var userInfo = notification.userInfo!
        // 2
        let keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        // 3
        let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        // 4
        let changeInHeight = (keyboardFrame.height + 20) * (show ? 1 : -1)
        //5
        UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
            self.viewSelectTicket.center.y -= changeInHeight
            if(show == false){
                self.viewSelectTicket.center.y = self.orgSelectTicketCenter
            }
        })
        
    }
    
    func keyboardWillShow(notification:NSNotification) {
        self.adjustingHeight(show: true, notification: notification)
        self.keyboardShown = true
    }
    
    func keyboardWillHide(notification:NSNotification) {
        self.adjustingHeight(show: false, notification: notification)
        self.keyboardShown = false
    }

    
}

