//
//  GalleryEditCell.swift
//  TheAgency
//
//  Created by developer on 5/16/17.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit

let kVibrateAnimation = "kVibrateAnimation"
let VIBRATE_DURATION: CGFloat = 0.1
let VIBRATE_RADIAN = CGFloat(CGFloat.pi/96)

class GalleryEditCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var btnDelete: UIButton!
    
   
    var isAddButton = false
    
    weak var delegate: GalleryEditCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnDelete.isHidden = true
    }
    
    
    @IBAction func actionDelete(_ sender: Any) {
        self.delegate?.galleryEditCellDeleteAt(cell: self)
    }
    
    private var vibrating: Bool {
        
        get {
            if let animationKeys = imageView?.layer.animationKeys() {
                return animationKeys.contains(kVibrateAnimation)
            }
            else {
                return false
            }
        }
        set {
            
            var _vibrating = false
            
            if let animationKeys = layer.animationKeys() {
                
                _vibrating = animationKeys.contains(kVibrateAnimation)
            }
            else {
                _vibrating = false
            }
            
            if _vibrating && !newValue {
                
                layer.removeAnimation(forKey: kVibrateAnimation)
            }
            else if !_vibrating && newValue {
                
                let vibrateAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
                vibrateAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                vibrateAnimation.fromValue = -VIBRATE_RADIAN
                vibrateAnimation.toValue = VIBRATE_RADIAN
                vibrateAnimation.autoreverses = true
                vibrateAnimation.duration = CFTimeInterval(VIBRATE_DURATION)
                vibrateAnimation.repeatCount = Float(CGFloat.greatestFiniteMagnitude)
                layer.add(vibrateAnimation, forKey: kVibrateAnimation)
            }
        }
    }
    
    var editing: Bool {
        
        get {
            return vibrating
        }
        set {
            if(self.isAddButton){
                btnDelete?.isHidden = true
            }else{
                vibrating = newValue
                btnDelete?.isHidden = !newValue
            }
        }
    }
    

}
protocol GalleryEditCollectionViewCellDelegate: class {
    
    func galleryEditCellDeleteAt( cell: GalleryEditCell)
}
