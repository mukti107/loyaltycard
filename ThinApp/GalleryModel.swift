//
//  GalleryModel.swift
//  MayaStojan
//
//  Created by developer on 5/22/17.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit

class GalleryModel: NSObject {
    var photoId: Int = 0
    var categoryId: Int = 0
    var image : String = ""
    var order : CGFloat = 0
    
    func initWithDictionary(data:Dictionary<String, AnyObject>){
        if let val = data["id"] {
            self.photoId = self.getNumberFromData(data: val)
        }
        if let val = data["category_id"] {
            self.categoryId = self.getNumberFromData(data: val)
        }
        if let val: String = data["photo"] as? String{
            self.image = val
        }
        if let val = data["sequence"] {
            self.order =  CGFloat( self.getDoubleFromData(data: val))
        }
    }
    
}
