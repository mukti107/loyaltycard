//
//  GalleryVC.swift
//  MayaStojan
//
//  Created by developer on 5/23/17.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD
import MWPhotoBrowser
import QBImagePickerController

let MAX_IMAGE_WIDTH :  CGFloat = 600

class GalleryVC: UICollectionViewController , UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    @IBOutlet weak var btnEdit: UIBarButtonItem!
    @IBOutlet weak var btnAdd: UIBarButtonItem!
    
    
    var imagePicker = UIImagePickerController()
    var addedImage = UIImage()
    var isEditCell:Bool = false
    var categoryModel = CategoryModel()
    var categoryId: Int  = -1
    
    let imageDetailSegueIdentifier = "imageDetailVC"
    
    var photos: [GalleryModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        categoryId = categoryModel.categoryId
        
        if(categoryId == -1){
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0 , width: 279, height: 50))
            imageView.contentMode = .scaleAspectFit
            let image = UIImage(named: "logo_slim.png")
            imageView.image = image
            
            navigationItem.titleView = imageView
 
        }else{
           self.title = categoryModel.title
        }
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back_arrow.png"), for: UIControlState.normal)
        
        button.addTarget(self, action:#selector(popSelf), for: UIControlEvents.touchUpInside)
        
        
        button.frame = CGRect.init(x: 15, y: 20, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton

        self.collectionView?.collectionViewLayout = CustomImageFlowLayout()
        
        if((ADMIN && categoryId > -1) || IS_LOOK_BOOK){
            self.btnAdd.tintColor = UIColor.white
            self.btnAdd.isEnabled = true
            self.btnEdit.tintColor = UIColor.white
            self.btnEdit.isEnabled = true
        }else{
            self.btnAdd.tintColor = UIColor.black
            self.btnAdd.isEnabled = false
            self.btnEdit.tintColor = UIColor.black
            self.btnEdit.isEnabled = false
        }
        
        loadImages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        SDWebImageManager.shared().cancelAll()
        SDImageCache.shared().clearMemory()
    }
    
    func loadImages(){
        if(photos.count > 0){
            return
        }
        let params = ["app_id":KAppID, "category_id":categoryId, "is_look_book":IS_LOOK_BOOK] as [String : Any]
        
        SVProgressHUD.show()
        NetworkManager.sharedClient.getRequest(tag: "gallery", parameters: params) { (error, result) in
            SVProgressHUD.dismiss()
            
            if (error != "") {
                self.showAlert(withTitle: "Error", message: error)
            }
            else{
                self.photos.removeAll()
                var items = result.value(forKey: "photos") as! Array<Dictionary<String, AnyObject>>
                
                for i in 0 ..< items.count {
                    let item = (items[i] as Dictionary<String, AnyObject>)
                    let model = GalleryModel()
                    model.initWithDictionary(data: item)
                    self.photos.append(model)
                    
                }
                self.collectionView?.reloadData()
            }
        }
        
    }
    func uploadImage(images:[UIImage]){
        
        
        if(images.count == 0){
            return
        }
        var data:[[String : Any]] = []
        var imageNames:[String] = []
        var orders:[String] = []
        var start = 0
        if(photos.count > 0){
            start = Int(photos[photos.count - 1].order)
        }
        
        for i in 0..<images.count {
            if (UIImageJPEGRepresentation(images[i], 1) != nil) {
                let newImage = self.resizeImage(image: images[i], newWidth: MAX_IMAGE_WIDTH)
                let imgData = UIImageJPEGRepresentation(newImage, 1)
                let imageName = String(format: "%.0f_%d.jpg",Date().timeIntervalSince1970, i)
                data.append([ "image": imgData! , "name":imageName ] as [String : Any])
                imageNames.append(imageName)
                
                start = start + 1
                orders.append(String(start))
            }
        }
        
        let params = ["app_name": KAppName, "app_id": String(KAppID), "category_id":String(categoryId), "is_category":String(0), "is_look_book":NSNumber(value:IS_LOOK_BOOK).stringValue, "image":imageNames.joined(separator: ","), "orders":orders.joined(separator: ",")] as [String : Any]
        
        SVProgressHUD.show()
       
        NetworkManager.sharedClient.uploadImageWithParameter(tag: "gallery", file: data, parameters: params as NSDictionary, completion: { (error, result) in
            SVProgressHUD.dismiss()
            if (error != "") {
                self.showAlert(withTitle: "Error", message: error)
            }
            else{
                let photoIds = result.value(forKey: "photo_id") as! [Int]
                var paths:[IndexPath] = []
                for i in 0..<photoIds.count{
                    if(photoIds[i] == -1){
                        continue
                    }
                    let model = GalleryModel()
                    model.photoId = photoIds[i]
                    model.categoryId = self.categoryId
                    model.image = imageNames[i]
                    self.photos.insert(model, at: 0)
                    let indexPath = IndexPath(item: i, section: 0)
                    paths.append(indexPath)
                    
                }
                self.collectionView?.insertItems(at: paths)
//                if(imageName != ""){
//
//                }else{
//                    self.collectionView?.reloadData()
//                    self.isEditCell = false
//                    self.btnEdit.title = "Edit"
//                }
                
                
            }
        }) { (progress) in
            SVProgressHUD.showProgress(Float(progress))
        }
        
        
    }
    func deleteCell(index:IndexPath){
        
        //let imageName = String(format: "%ld_profile.jpg",AppManager.sharedInstance.getUserId())
        let model = photos[index.row]
        let tag = "gallery/\(String(describing: model.photoId))"
        let params = ["is_look_book":NSNumber(value:IS_LOOK_BOOK).stringValue, "app_name": KAppName] as [String : Any]
        SVProgressHUD.show()
        NetworkManager.sharedClient.deleteRequest(tag: tag, parameters: params) { (error, result) in
            SVProgressHUD.dismiss()
            if (error != "") {
                self.showAlert(withTitle: "Error", message: error)
            }
            else{
                self.photos.remove(at: index.row)
                let indexPath = IndexPath(item: index.row, section: 0)
                self.collectionView?.deleteItems(at: [indexPath])
            }
        }
        
    }
    
    func reorderCell(moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath){
        
        let model = photos[sourceIndexPath.row]
        var order : CGFloat = 0
        if(destinationIndexPath.row == 0){
            let desModel = photos[destinationIndexPath.row]
            order = CGFloat(Int(desModel.order+1))
        }else if(destinationIndexPath.row == photos.count - 1){
            let desModel = photos[destinationIndexPath.row - 1]
            order = desModel.order / 2
        }else{
            let row = destinationIndexPath.row
            if(sourceIndexPath.row > destinationIndexPath.row){
                let p = photos[row - 1]
                let n = photos[row]
                order = (n.order + p.order) / 2
            }else{
                let p = photos[row + 1]
                let n = photos[row]
                order = (n.order + p.order) / 2
            }
            
        }
        
        let params = ["is_look_book":IS_LOOK_BOOK, "id": model.photoId, "order":order] as [String : Any]
        SVProgressHUD.show()
        NetworkManager.sharedClient.putRequest(tag: "gallery", parameters: params) { (error, result) in
            SVProgressHUD.dismiss()
            if (error != "") {
                self.showAlert(withTitle: "Error", message: error)
            }
            else{
                let temp = self.photos.remove(at: sourceIndexPath.item)
                self.photos.insert(temp, at: destinationIndexPath.item)
            }
            
            self.collectionView?.reloadData()
            self.isEditCell = false
            self.btnEdit.title = "Edit"
        }
        
    }
    
    func popSelf() {
        IS_LOOK_BOOK = false
        if(categoryId == -1){
            self.dismiss(animated: true, completion: nil)
        }else{
           self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction fileprivate func backButtonPressed(_ buttn: UIButton) {
        self.dismiss(animated: true, completion: nil);
    }
    
    @IBAction func actionEdit(_ sender: Any) {
        setEditCollectionView()
    }
    
    @IBAction func actionAdd(_ sender: Any) {
        
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // Create and add the Cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            // Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        
        // Create and add first option action
        let takePictureAction = UIAlertAction(title: "Take a photo", style: .default) { action -> Void in
            self.takePhoto(gallery: false)
        }
        actionSheetController.addAction(takePictureAction)
        
        // Create and add a second option action
        let choosePictureAction = UIAlertAction(title: "Select from gallery", style: .default) { action -> Void in
            self.takeImageFromGallery()
        }
        actionSheetController.addAction(choosePictureAction)
        
        // We need to provide a popover sourceView when using it on iPad
        //actionSheetController.popoverPresentationController?.sourceView = sender as UIView
        
        // Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
        

      

    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        self.uploadImage(images: [chosenImage])
        dismiss(animated:true, completion: nil) //5
    }
    
    // Custom
    
    func takePhoto(gallery:Bool){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            if gallery {
                imagePicker.sourceType = .photoLibrary;
            }else{
                imagePicker.sourceType = .camera;
            }
            
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func takeImageFromGallery(){
        let imagePickerController = QBImagePickerController()
        imagePickerController.maximumNumberOfSelection = 5
        imagePickerController.delegate = self
        imagePickerController.allowsMultipleSelection = true
        imagePickerController.mediaType = QBImagePickerMediaType.image
        imagePickerController.showsNumberOfSelectedAssets = true
        present(imagePickerController, animated: true, completion: nil)
    }
    func setEditCollectionView(){
        isEditCell = !isEditCell
        
        if(isEditCell){
            self.btnEdit.title = "Done"
        }else{
            self.btnEdit.title = "Edit"
        }
        
        for cell in (self.collectionView?.visibleCells)! {
            
            if cell is GalleryEditCell {
                
                let gridViewCell = cell as! GalleryEditCell
                gridViewCell.editing = isEditCell
            }
            else {
                assert(false, "LxGridView: Must use LxGridViewCell as your collectionViewCell class!")
            }
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return photos.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : GalleryEditCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)  as! GalleryEditCell
        
        let model = photos[indexPath.row]
        
        let imageUrl  =  self.downloadUrlWithFileName(fileName: model.image)
        var placeHolder:UIImage
        if UIImageJPEGRepresentation(self.addedImage, 1) != nil {
            placeHolder = self.addedImage
        }else{
            placeHolder = UIImage(named:"default_image")!
        }
        //cell.imageView.sd_cancelCurrentImageLoad()
        cell.imageView.sd_setImage(with: imageUrl, placeholderImage: placeHolder)
        cell.delegate = self
        cell.btnDelete.isHidden = !self.isEditCell
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let galleryCell : GalleryEditCell = cell  as! GalleryEditCell
        galleryCell.imageView.sd_cancelCurrentImageLoad()
    }
    
    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        self.reorderCell(moveItemAt: sourceIndexPath, to: destinationIndexPath)
    }
    
    override func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return self.isEditCell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(self.isEditCell == false){
            collectionView.deselectItem(at: indexPath, animated: true)
           
            var images = [MWPhoto]()
            for model in photos {
                let url = self.downloadUrlWithFileName(fileName: model.image)
                let photo = MWPhoto.init(url: url)
                images.append(photo!)
            }
            
            let browser = MWPhotoBrowser.init(photos: images)
            browser?.setCurrentPhotoIndex(UInt(indexPath.row))
            self.navigationController?.pushViewController(browser!, animated: true)
            
        }
    }

}

extension GalleryVC : QBImagePickerControllerDelegate{
    func qb_imagePickerController(_ imagePickerController: QBImagePickerController!, didFinishPickingAssets assets: [Any]!) {
        var images = [UIImage]()
        for asset in assets{
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.isSynchronous = true
            manager.requestImage(for: asset as! PHAsset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
                images.append(result!)
            })
        }
        self.uploadImage(images: images)
        imagePickerController.dismiss(animated: true, completion: nil)
    }
    func qb_imagePickerControllerDidCancel(_ imagePickerController: QBImagePickerController!) {
        imagePickerController.dismiss(animated: true, completion: nil)
    }
}
extension GalleryVC : GalleryEditCollectionViewCellDelegate{
    func galleryEditCellDeleteAt(cell: GalleryEditCell) {
        let indexPath = self.collectionView?.indexPath(for: cell)
        self.deleteCell(index: indexPath!)
    }
}

extension UIView {
    func snapshot() -> UIImage {
        UIGraphicsBeginImageContext(self.bounds.size)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

extension CGPoint {
    func distanceToPoint(p:CGPoint) -> CGFloat {
        return sqrt(pow((p.x - x), 2) + pow((p.y - y), 2))
    }
}

struct SwapDescription : Hashable {
    var firstItem : Int
    var secondItem : Int
    
    var hashValue: Int {
        get {
            return (firstItem * 10) + secondItem
        }
    }
}

func ==(lhs: SwapDescription, rhs: SwapDescription) -> Bool {
    return lhs.firstItem == rhs.firstItem && lhs.secondItem == rhs.secondItem
}
extension UIViewController {
    func showAlert(withTitle title: String, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
   /* func downloadUrlWithFileName(fileName:String) -> URL {
        let url = String(format: "%@/%@/%@", KServerImageURL , KAppName ,fileName)
        return URL(string: url)!
    }*/
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        
        
        image.draw(in: CGRect(x: 0, y: 0,width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}

extension NSObject{
    func downloadUrlWithFileName(fileName:String) -> URL {
        let url = String(format: "%@%@/%@", KServerImageURL , KAppName ,fileName)
        return URL(string: url)!
    }
}
