
import UIKit

class ImageDetailVC: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var image:UIImage? = nil
    
    
    private var myArray =  ["img1", "img2", "img3", "img4", "img5",
                            "img6", "img7", "img8", "img9", "img10",
                            "img11", "img12"]
    
    let maxImages = 11

    var myIndex: NSInteger = 0

    var flipped = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
 
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        let defaults = UserDefaults.standard
      
        myIndex = defaults.integer(forKey: "imgname");
        // if let message = defaults.string(forKey: "imgname") {
        // if myIndex = defaults.integerForKey("imgname") {
            
            
            //self.imageView.image = UIImage(named: message)
            //self.imageView.contentMode = UIViewContentMode.scaleAspectFit
            /*
            let width = UIScreen.main.bounds.size.width
            let height = UIScreen.main.bounds.size.height
            var imageViewObject :UIImageView
            imageViewObject = UIImageView(frame:CGRect(x: 0, y: 0, width: width, height: height));
            imageViewObject.image = UIImage(named: message)
            imageViewObject.contentMode = UIViewContentMode.scaleAspectFill
            self.view.addSubview(imageViewObject)
            */
        
     //   imageView.isUserInteractionEnabled = true//do not forget to right this line otherwise ...imageView's

        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        imageView.image = UIImage(named: myArray[myIndex])
        
        print("myIndex @%",myIndex)
      
        
        let userDefaults = UserDefaults.standard
        userDefaults.synchronize()
        userDefaults.set(nil, forKey: "imgname")
        

    

        
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
             
                // --currentnImageIndex
                
                if myIndex > 0{
                   myIndex -= 1
                }
              //  if myIndex >= 0{
                if myIndex >= 0{
                    
                    imageView.image = UIImage(named: myArray[myIndex])
                    
                    UIView.transition(with: imageView , duration: 1.0, options: [.transitionFlipFromLeft], animations: {
                        self.imageView . isHidden = false
                        
                    }, completion: { _ in })

                }
                // if curretnImageIndex
                
                
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
                
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                
                if myIndex < myArray.count - 1 {
                    myIndex += 1
                }
                
                if myIndex < myArray.count  {
                    
                    imageView.image = UIImage(named: myArray[myIndex])
                    
                  UIView.transition(with: imageView , duration: 1.0, options: [.transitionFlipFromRight], animations: {
                        self.imageView . isHidden = false
                        
                    }, completion: { _ in })
 
           
                }

            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }

    @IBAction fileprivate func closeButtonPressed(_ buttn: UIButton) {
        self.dismiss(animated: true, completion: nil);
    }
}

