//
//  LoyaltyViewController.swift
//  Ear2Ground
//
//  Created by Usman Ali on 7/6/17.
//  Copyright © 2017 RiskEater. All rights reserved.
//

import UIKit
import QRCode
import UserNotifications
import MessageUI

class LoyaltyViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate{
    
    
    @IBOutlet weak var rewardsButton: UIButton!
    @IBOutlet weak var QRImage: UIImageView!
    @IBOutlet weak var scannerToggleButton: UIButton!
    
    var reverseScanMode:Bool = true
    var availableTickets:Int=0
    var totalTickets:Int=0
    var udid : String!
    var name : String!
    var stampImage:UIImage=UIImage()
    var couponImages:[UIImageView]=[]
    var stampsCollectionHeightConstraint=NSLayoutConstraint()
    var qrCodeHeightConstraint=NSLayoutConstraint()

    var loyaltyCardStaticImageHeightConstraint=NSLayoutConstraint()

    @IBOutlet weak var totalVisits: UILabel!
    @IBOutlet weak var stampsParentView: UIView!
    @IBOutlet weak var loyalityBG: UIImageView!
    @IBOutlet weak var stampsCollection: UICollectionView!
    @IBOutlet weak var loyalityLogo: UIImageView!
    @IBOutlet weak var rewardDescription: UILabel!
    
    @IBOutlet weak var loyaltyCardStaticImage: UIImageView!
    
    @IBOutlet weak var vCardButtons: UIStackView!
    @IBOutlet weak var commentButton: UIButton!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCoupons), name: NSNotification.Name(rawValue: "ticketReceived"), object: nil)
        self.updateCoupons()
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //applySettings()
        
        let reverseScannerAction = UITapGestureRecognizer(target: self, action: #selector(self.reverseScanner))
        reverseScannerAction.numberOfTapsRequired=1
        QRImage.isUserInteractionEnabled=true
        QRImage.addGestureRecognizer(reverseScannerAction)
        
    
        stampsCollectionHeightConstraint=NSLayoutConstraint(item: stampsCollection, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        stampsCollectionHeightConstraint.priority=1000
        
        loyaltyCardStaticImageHeightConstraint=NSLayoutConstraint(item: loyaltyCardStaticImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        loyaltyCardStaticImageHeightConstraint.priority=1000
        
        qrCodeHeightConstraint=NSLayoutConstraint(item: QRImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        qrCodeHeightConstraint.priority=1000
        
        
        
        self.view.addConstraint(stampsCollectionHeightConstraint)
        self.view.addConstraint(qrCodeHeightConstraint)
        self.view.addConstraint(loyaltyCardStaticImageHeightConstraint)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(enterForeground), name: .UIApplicationWillEnterForeground, object: nil)
        

        
        udid = UIDevice.current.identifierForVendor?.uuidString
        name=UIDevice.current.name
        
        
        
        
        
        updateCoupons()
        
        // Do any additional setup after loading the view.
    }
    
    
    func reverseScanner(){
        let scannerMode=UserDefaults.standard.bool(forKey: "reverse_scanner_mode")
        
        if scannerMode{
            performSegue(withIdentifier: "reverseScanner", sender: self)
        }
        
    }
    
    func updateUIWithSettings(){
        
        let vcMode = UserDefaults.standard.bool(forKey: "vcard_mode")
        let scannerMode=UserDefaults.standard.bool(forKey: "reverse_scanner_mode")
        
        
        
        if(vcMode){
            stampsCollectionHeightConstraint.isActive = true
            qrCodeHeightConstraint.isActive = true
            loyaltyCardStaticImageHeightConstraint.isActive=true
            vCardButtons.isHidden=false
            rewardsButton.isHidden=true
            totalVisits.isHidden=true
            
        }else{
            vCardButtons.isHidden=true
            stampsCollectionHeightConstraint.isActive = false
            qrCodeHeightConstraint.isActive = false
            loyaltyCardStaticImageHeightConstraint.isActive=false
            
            vCardButtons.isHidden=true
            rewardsButton.isHidden=false
            totalVisits.isHidden=false
        }
        
        commentButton.isHidden = !vcMode
        commentButton.isEnabled = vcMode
        
        rewardsButton.isEnabled = !vcMode
        
        
        scannerToggleButton.isHidden = !scannerMode || vcMode
        
        if scannerMode && reverseScanMode {
            self.showScanner()
        }else{
            self.generateQRCode()
        }
        
        
        
        //self.view.setNeedsLayout()
        //self.view.layoutIfNeeded()
    }
    
    
    func enterForeground(){
        checkNewSettings()
        updateCoupons()
    }
    
    func applySettings(){
        
      
        totalTickets=UserDefaults.standard.integer(forKey:"tickets_for_reward")
        
        if let description=UserDefaults.standard.string(forKey:"reward_description"){
            rewardDescription.text=description
        }
        
        
            totalVisits.text="\(UserDefaults.standard.integer(forKey:"total_visits"))"
    
        
        if let data=UserDefaults.standard.data(forKey: "bg"){
            print (data)
            loyalityBG.image = UIImage(data:data)!
        }
        
        if let data=UserDefaults.standard.data(forKey: "logo"){
            print (data)
            loyalityLogo.image = UIImage(data:data)!
        }
        
        if let data=UserDefaults.standard.data(forKey: "stamp"){
            print (data)
            self.stampImage = UIImage(data:data)!
            rewardsButton.setImage(stampImage, for: .normal)
        }
        
        
        updateUIWithSettings()
        
    }
    
    override func viewDidLayoutSubviews() {
        let width=stampsCollection.frame.width
        let itemSize=width/3-3
        let layout=UICollectionViewFlowLayout()
        layout.sectionInset=UIEdgeInsetsMake(0, 0, 0, 0)
        
        layout.itemSize=CGSize(width: itemSize, height: itemSize)
        layout.minimumInteritemSpacing=3
        layout.minimumLineSpacing=3
        
        stampsCollection.collectionViewLayout=layout
        
        checkNewSettings()
        applySettings()
        
    }
    
    
    func resizeStamps(){
//        let height = stampsCollection.collectionViewLayout.collectionViewContentSize.height
//        stampsHeightConstraintHidden.constant=height
//        
//        self.view.setNeedsLayout()
//        self.view.layoutIfNeeded()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver("ticketReceived")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return totalTickets
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell=collectionView.dequeueReusableCell(withReuseIdentifier: "stampCell", for: indexPath) as! StampCellController
        cell.stamp.image=stampImage
        cell.isAvailable(availableTickets>indexPath.row)
        print(indexPath.row)
        return cell
    }
    
    @IBAction fileprivate func menuButtonPressed(_ buttn: UIButton) {
        let drawerController = navigationController?.parent as? KYDrawerController
        drawerController?.setDrawerState(.opened, animated: true)
        
    }
    
    
    @IBAction func callPhone(_ sender: Any) {
        
        
        
        
        if let num = UserDefaults.standard.string(forKey:"phone_number"), let url = URL(string: "tel://\(num)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func sendEmail(_ sender: Any) {
        
        if let email = UserDefaults.standard.string(forKey:"email_address"){
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.setToRecipients([email])
        //composeVC.setSubject("StoryBook Feedback")
        //composeVC.setMessageBody("Hey Josh! Here's my feedback.", isHTML: false)
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func openWebURL(_ sender: Any) {
        if let addr=UserDefaults.standard.string(forKey:"custom_url"), let url=URL(string:addr),UIApplication.shared.canOpenURL(url){
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func sendSMS(_ sender: Any) {
        
        if let number=UserDefaults.standard.string(forKey:"sms_text_number"){
        let messageVC = MFMessageComposeViewController()
            messageVC.messageComposeDelegate = self
            messageVC.recipients = [number]
        self.present(messageVC, animated: false, completion: nil)
        }
    }
    
    
    @IBAction func RewardsButtonPressed(_ sender: UIButton) {
        
        performSegue(withIdentifier: "sequeShowRewards", sender: self)
    }
    
    @IBAction func toggleReverseScan(_ sender: Any) {
        if reverseScanMode {
            self.generateQRCode()
        }else{
            self.showScanner()
        }
    }
    
    
    @IBAction func showContact(_ sender: Any) {
            performSegue(withIdentifier: "showContacts", sender: self)
    }
    
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func generateQRCode(){
        reverseScanMode=false
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: ["type":"checkin","udid":udid,"name":name], options: .prettyPrinted)
            
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            let qrCode = QRCode(jsonString)
            QRImage.image = qrCode?.image
        }catch{
            print(error.localizedDescription)
        }
        
    }
    
    
    func showScanner(){
        reverseScanMode=true
        QRImage.image=UIImage(named: "scan_btn")
    }
    
    
    
    func performGetRequest(_ targetURL: URL!, completion:@escaping (_ data: Data?, _ HTTPStatusCode: Int, _ error: Error?) -> Void) {
        
        var request = URLRequest(url: targetURL)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let sessionConfiguration = URLSessionConfiguration.default
        
        let Session = URLSession(configuration: sessionConfiguration)
        
        let tasksession = Session.dataTask(with: request) { data, response, error in
            if data != nil{
                DispatchQueue.main.async {
                    completion(data, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            else
            {
                print("Connection Lost")
                DispatchQueue.main.async {
                    completion(nil, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            
        }
        tasksession.resume()
    }
    
    func updateCoupons(){
        
        
        availableTickets=UserDefaults.standard.integer(forKey: "tickets")
        stampsCollection.reloadData()
        
        let udidEncoded = udid.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let url=Settings.baseURL+"/index.php?command=myPendingVisits&udid="+udidEncoded!
        let targetURL=URL(string:url)
        
        
        performGetRequest(targetURL, completion: {(data,HTTPStatusCode, erro) -> Void in
            
            if HTTPStatusCode == 200 && erro == nil {
                do{
                    let response = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    print(response)
                    
                    if let coupons = response["num_visits"] as? String , let totalVisits=response["total_visits"] as? String {
                        print(coupons)
                        DispatchQueue.main.async {
                            print(coupons)
                            UserDefaults.standard.set(Int(totalVisits)!, forKey:"total_visits")
                            
                            self.totalVisits.text=totalVisits
                            UserDefaults.standard.set(Int(coupons)!, forKey:"tickets")
                            self.availableTickets=Int(coupons)!
                            self.stampsCollection.reloadData()
                            self.resizeStamps()
                        }
                    }
                    
                }catch {
                    print(error)
                }
                
            }else{
            }
        } )
    }
    
    
    
    func checkNewSettings(){
        do{
            let url=URL(string:Settings.baseURL+"/index.php?command=getSettings")
            let data = try? Data(contentsOf: url!)
            let settings=try JSONSerialization.jsonObject(with: data!, options:[] ) as? [String:String]
            print ("Settings",settings!)
            
            let stored=UserDefaults.standard
            if let last_updated=stored.string(forKey: "last_updated"){
                if last_updated != settings?["last_updated"]{
                    updateSettings(settings!)
                }
            }else{
                updateSettings(settings!)
            }
        }catch{}
    }
    
    func updateSettings(_ settings:[String:String]){
        let store=UserDefaults.standard
        //store.set(settings["last_updated"], forKey:"last_updated")
        store.set(Int(settings["tickets_for_reward"]!), forKey:"tickets_for_reward")
        store.set(settings["reward"], forKey:"reward_description")
        store.set(settings["nearby_notification"], forKey:"nearby_notification")
        
        for key in ["vcard_mode","phone_number","email_address","custom_url","sms_text_number","reverse_scanner_mode","limit_user_per_day","admin_qr_value"]{
            
            store.set(settings[key], forKey:key)
            
        }

        
        for name in ["bg","logo","stamp"]{
            print(name)
            let url=URL(string:Settings.baseURL+"/"+settings[name]!)
            let data = try? Data(contentsOf: url!)
            store.set(data,forKey:name)
        }
        store.set(settings["last_updated"], forKey:"last_updated")
        self.applySettings()
    }
    
}
