//
//  ManageEventDetailVC.swift
//  Ear2Ground
//
//  Created by Bruce Khin on 11/03/2017.
//  Copyright © 2017 RiskEater. All rights reserved.
//
import Foundation
import UIKit
import AVFoundation
import SVProgressHUD
import SDWebImage


class ManageEventDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, AVCaptureMetadataOutputObjectsDelegate{
    
    @IBOutlet weak var qrCodeMarkView: UIImageView!
    
    let SELECT_REGISTER_ATTENDEE = 1 as Int
    let SELECT_SCAN_QRCODE = 2 as Int
    
    @IBOutlet weak var btnScanQRCode: UIButton!
    @IBOutlet weak var btnRegisterAttendee: UIButton!
    @IBOutlet weak var tblAttendees: UITableView!
    
    @IBOutlet weak var viewAttendeeLists: UIView!
    @IBOutlet weak var srchTextView: UISearchBar!
    
    
    var attendeesArray: Array<Dictionary<String, AnyObject>> = []
    
    var filtered: Array<Dictionary<String, AnyObject>> = []
    var desiredAttendeeItemDataDict = Dictionary<String, AnyObject>()
    var refreshController: UIRefreshControl!
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    var gl:CAGradientLayer!
    var searchActive : Bool = false
    var fromScanCode : Bool = false
    var event_id:String = ""
    
    let supportedCodeTypes = [AVMetadataObjectTypeUPCECode,
                              AVMetadataObjectTypeCode39Code,
                              AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeCode93Code,
                              AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypeEAN8Code,
                              AVMetadataObjectTypeEAN13Code,
                              AVMetadataObjectTypeAztecCode,
                              AVMetadataObjectTypePDF417Code,
                              AVMetadataObjectTypeQRCode]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblAttendees.delegate = self
        tblAttendees.dataSource = self
        
      
        
        let colorTop = UIColor.black.cgColor
        let colorBottom = UIColor(red: 65.0 / 255.0, green: 65.0 / 255.0, blue: 65.0 / 255.0, alpha: 1.0).cgColor
        
        self.gl = CAGradientLayer()
        self.gl.colors = [colorTop, colorBottom]
        self.gl.locations = [0.0, 1.0]
        
        self.attendeesArray.removeAll(keepingCapacity: false)
        self.filtered.removeAll(keepingCapacity: false)
        self.desiredAttendeeItemDataDict.removeAll(keepingCapacity: false)
        self.tblAttendees.allowsSelection = false;
        for i in 0 ..< 20{
            self.desiredAttendeeItemDataDict["attendeeName"] = String.init(format: "Test Name %d", i + 1) as AnyObject?
            if(i % 3 == 0){
                self.desiredAttendeeItemDataDict["checked"] = 1 as AnyObject?
            }else{
                self.desiredAttendeeItemDataDict["checked"] = 0 as AnyObject?
            }
            self.attendeesArray.append(self.desiredAttendeeItemDataDict)
            self.filtered.append(self.desiredAttendeeItemDataDict)
            self.tblAttendees.reloadData()
        }
        
        refreshController = UIRefreshControl()
        refreshController.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshController.backgroundColor  = UIColor.clear
        
        refreshController.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshController.tintColor = UIColor.white
        refreshController.attributedTitle = NSAttributedString(string: "Pull down to refresh...")
        tblAttendees.addSubview(refreshController)
        SVProgressHUD.show(withStatus: "Loading...")
        
        self.setupCamera()
        self.makeButtonState(state: SELECT_REGISTER_ATTENDEE)
        DispatchQueue.main.async{
            self.getInpiration()
        }
        
        // Do any additional setup after loading the view.
        
        /*
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0 , width: 50, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "logo_slim")
        imageView.image = image
        
        navigationItem.titleView = imageView*/
        
        self.navigationItem.title = "Manage Loyalty"
        
   
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back_arrow.png"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(popSelf), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let buttonRight = UIButton.init(type: .custom)
        buttonRight.setImage(UIImage.init(named: ""), for: UIControlState.normal)
        buttonRight.frame = CGRect.init(x: 7, y: 15, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        buttonRight.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15)
        let barButtonRight = UIBarButtonItem.init(customView: buttonRight)
        self.navigationItem.rightBarButtonItem = barButtonRight
        
        /* HIDE UINavigationBar Border */
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
    }
    
    func popSelf() {
        //self.dismiss(animated: true, completion: nil);
        // popViewController Dissmiss NavigationController
        _ = navigationController?.popViewController(animated: true);
    }
    
    func refresh() {
        // Code to refresh table view
        print("Reloading...")
        
        DispatchQueue.main.async(execute: { () -> Void in
            
            SVProgressHUD.show(withStatus: "Loading...")
            
            self.tblAttendees.reloadData()
            
            self.getInpiration()
            
            self.refreshController.endRefreshing()
        })
    }
    
    func performGetRequest(_ targetURL: URL!, completion:@escaping (_ data: Data?, _ HTTPStatusCode: Int, _ error: Error?) -> Void) {
        
        var request = URLRequest(url: targetURL)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let sessionConfiguration = URLSessionConfiguration.default
        
        let Session = URLSession(configuration: sessionConfiguration)
        
        let tasksession = Session.dataTask(with: request) { data, response, error in
            if data != nil{
                DispatchQueue.main.async {
                    completion(data, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            else
            {
                print("Connection Lost")
                DispatchQueue.main.async {
                    completion(nil, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            
        }
        tasksession.resume()
    }
    
    
    func getInpiration() {
        
        
        self.attendeesArray.removeAll(keepingCapacity: false)
        self.filtered.removeAll(keepingCapacity: false)
        
        self.desiredAttendeeItemDataDict.removeAll(keepingCapacity: false)
        
        self.tblAttendees.reloadData()
        
        var urlString: String!
        
        urlString = "https://thnapp.com/api/webservices/index.php?command=getAttendees&event_id=" + self.event_id
        
        // Create a NSURL object based on the above string.
        let targetURL = NSURL(string: urlString)
        
        // Fetch the playlist from Google.
        performGetRequest(targetURL as URL!, completion: { (data, HTTPStatusCode, error) -> Void in
            if HTTPStatusCode == 200 && error == nil {
                do {
                    // Convert the JSON data into a dictionary.
                    var resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, AnyObject>
                    
                    // Get all playlist items ("items" array).
                    var items: Array<Dictionary<String, AnyObject>> = resultsDict["result"] as! Array<Dictionary<String, AnyObject>>
                    
                    
                    // Use a loop to go through all video items.
                    for i in 0 ..< items.count {
                        
                        let playlistSnippetDict = (items[i] as Dictionary<String, AnyObject>)
                        
                        // Initialize a new dictionary and store the data of interest.
                        // var desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
                        //desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
                        self.desiredAttendeeItemDataDict["id"] = playlistSnippetDict["id"]
                        self.desiredAttendeeItemDataDict["device_id"] = playlistSnippetDict["device_id"]
                        self.desiredAttendeeItemDataDict["email"] = playlistSnippetDict["email"]
                        
                        self.desiredAttendeeItemDataDict["phone"] = playlistSnippetDict["phone"]
                        self.desiredAttendeeItemDataDict["tickets"] = playlistSnippetDict["tickets"]
                        self.desiredAttendeeItemDataDict["date"] = playlistSnippetDict["date"]
                        self.desiredAttendeeItemDataDict["event_id"] = playlistSnippetDict["event_id"]
                        self.desiredAttendeeItemDataDict["status"] = playlistSnippetDict["status"]
                        
                        
                        // Append the desiredPlaylistItemDataDict dictionary to the videos array.
                        self.attendeesArray.append(self.desiredAttendeeItemDataDict)
                        self.filtered.append(self.desiredAttendeeItemDataDict)
                        self.tblAttendees.reloadData()
                        
                    }
                } catch {
                    print(error)
                }
            }
            else {
                print("HTTP Status Code = \(HTTPStatusCode)")
                print("Error while loading channel videos: \(String(describing: error))")
            }
            
            // Hide the activity indicator.
            //self.viewWait.isHidden = true
            
            SVProgressHUD.dismiss()
            self.refreshController.endRefreshing()
            
            
            
        })
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupCamera(){
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = viewAttendeeLists.frame
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Move the message label and top bar to the front
            //            view.bringSubview(toFront: messageLabel)
            //            view.bringSubview(toFront: topbar)
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubview(toFront: qrCodeFrameView)
            }
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
    }
    
    /**
     func makeButtonState(state:Int){
     if(state == SELECT_REGISTER_ATTENDEE){
     viewAttendeeLists.isHidden = false
     videoPreviewLayer?.isHidden = true
     captureSession?.stopRunning()
     self.makeGradientButton(button: btnScanQRCode)
     btnRegisterAttendee.backgroundColor = UIColor(red:0.0/255.0, green:119.0 / 255.0, blue:204.0 / 255.0, alpha:1.0)
     }else{
     viewAttendeeLists.isHidden = true
     videoPreviewLayer?.isHidden = false
     captureSession?.startRunning()
     self.makeGradientButton(button: btnRegisterAttendee)
     btnScanQRCode.backgroundColor = UIColor(red:0.0/255.0, green:119.0 / 255.0, blue:204.0 / 255.0, alpha:1.0)
     }
     }*/
    
    func makeButtonState(state:Int){
        if(state == SELECT_REGISTER_ATTENDEE){
            viewAttendeeLists.isHidden = false
            videoPreviewLayer?.isHidden = true
            captureSession?.stopRunning()
            self.makeGradientButton(button: btnScanQRCode)
            btnRegisterAttendee.backgroundColor = UIColor(red:0.0/255.0, green:119.0 / 255.0, blue:204.0 / 255.0, alpha:1.0)
            qrCodeMarkView.isHidden = true
        }else{
            viewAttendeeLists.isHidden = true
            videoPreviewLayer?.isHidden = false
            captureSession?.startRunning()
            self.makeGradientButton(button: btnRegisterAttendee)
            btnScanQRCode.backgroundColor = UIColor(red:0.0/255.0, green:119.0 / 255.0, blue:204.0 / 255.0, alpha:1.0)
            qrCodeMarkView.isHidden = false
            self.view.bringSubview(toFront: qrCodeMarkView)
        }
    }
    
    func makeGradientButton(button:UIButton){
        gl.removeFromSuperlayer()
        gl.frame = button.bounds
        button.layer.insertSublayer(gl, at: 0)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
    @IBAction func registerAttendePressd(_ sender: Any) {
        self.makeButtonState(state: SELECT_REGISTER_ATTENDEE)
    }
    @IBAction func scanQRCodePressd(_ sender: Any) {
        
        self.makeButtonState(state: SELECT_SCAN_QRCODE)
    }
    
    // MARK: UITableView method implementation
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: ManageUserTableCell = ManageUserTableCell()
        //var cell: UITableViewCell!
        
        cell = tableView.dequeueReusableCell(withIdentifier: "idCellAttendee", for: indexPath) as! ManageUserTableCell
        
        let attendeeDetails = filtered[indexPath.row] as Dictionary<String, AnyObject>;()
        
        
        
        let checkKeyExists = attendeeDetails["status"] != nil
        if let val = attendeeDetails["status"] {
            if(val as! String == "1"){
                cell.btnCheckIN.backgroundColor = UIColor.init(red:119.0 / 255.0, green: 119.0 / 255.0, blue: 119.0 / 255.0, alpha: 1.0)
                cell.btnCheckIN.setTitle("ARRIVED", for: .normal)
                cell.btnCheckIN.isEnabled = false
            }else{
                cell.btnCheckIN.backgroundColor = UIColor.init(red:0.0 / 255.0, green: 109.0 / 255.0, blue: 214.0 / 255.0, alpha: 1.0)
                cell.btnCheckIN.setTitle("CHECK IN", for: .normal)
                cell.btnCheckIN.tag = indexPath.row
                cell.btnCheckIN.addTarget(self, action: #selector(self.checkInBtnPressed(_:)), for: .touchUpInside)
            }
            
        }
        
        cell.lblUserName.text = attendeeDetails["email"] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 53.0
        //return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //Delegate of SearchBar
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.filtered.removeAll(keepingCapacity: false)
        for i in 0 ..< attendeesArray.count{
            let attendeeDetails = attendeesArray[i]
            self.filtered.append(attendeeDetails)
        }
        self.tblAttendees.reloadData()
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filtered.removeAll(keepingCapacity: false)
        if(searchText.characters.count == 0){
            for i in 0 ..< attendeesArray.count{
                let attendeeDetails = attendeesArray[i]
                self.filtered.append(attendeeDetails)
            }
        }
        for i in 0 ..< attendeesArray.count{
            let attendeeDetails = attendeesArray[i]
            var attendeeName = attendeeDetails["attendeeName"] as? String
            attendeeName = attendeeName?.lowercased()
            let srchText = searchText.lowercased()
            if(attendeeName?.contains(srchText))!{
                self.filtered.append(attendeeDetails)
            }
        }
        self.tblAttendees.reloadData()
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            //messageLabel.text = "No QR/barcode is detected"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            var rt = barCodeObject!.bounds as CGRect
            rt.origin.y += viewAttendeeLists.frame.origin.y
            qrCodeFrameView?.frame = rt
            
            if metadataObj.stringValue != nil {
                //messageLabel.text = metadataObj.stringValue
                let ticketInfo = convertToDictionary(text:metadataObj.stringValue)
                self.fromScanCode = true
                if(ticketInfo != nil){
                    if let val = ticketInfo?["attendee_id"] {
                        qrCodeFrameView?.frame = CGRect.zero
                        self.makeButtonState(state: SELECT_REGISTER_ATTENDEE)
                        self.checkInAttendee(attendee_id: ticketInfo?["attendee_id"] as! String)
                    }
                }
            }
        }
        
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    @IBAction func checkInBtnPressed(_ sender: UIButton) {
        let id_in_arr:Int = sender.tag
        let attendeeDetails = self.attendeesArray[id_in_arr] as Dictionary<String, AnyObject>;()
        let att_id = attendeeDetails["id"] as! String
        self.fromScanCode = false
        self.checkInAttendee(attendee_id: att_id)
    }
    
    func checkInAttendee(attendee_id:String){
        var urlString: String!
        
        urlString = "https://thnapp.com/api/webservices/index.php?command=updateCheckin&attendee_id=" + attendee_id
        
        // Create a NSURL object based on the above string.
        let targetURL = NSURL(string: urlString)
        
        // Fetch the playlist from Google.
        performGetRequest(targetURL as URL!, completion: { (data, HTTPStatusCode, error) -> Void in
            if HTTPStatusCode == 200 && error == nil {
                do {
                    var attendeeDetails = Dictionary<String, AnyObject>()
                    var removedId : Int = 0
                    for i in 0 ..< self.attendeesArray.count{
                        attendeeDetails = self.attendeesArray[i]
                        let att_id = attendeeDetails["id"] as! String
                        if(attendee_id == att_id){
                            attendeeDetails["status"] = "1" as AnyObject
                            self.attendeesArray.remove(at: i)
                            removedId = i
                            break
                        }
                    }
                    self.attendeesArray.insert(attendeeDetails, at: removedId)
                    self.filtered.removeAll(keepingCapacity: false)
                    for i in 0 ..< self.attendeesArray.count{
                        let attendeeDetails1 = self.attendeesArray[i]
                        self.filtered.append(attendeeDetails1)
                    }
                    if(self.fromScanCode){
                        self.filtered.removeAll(keepingCapacity: false)
                        //                        self.srchTextView.text = attendeeDetails["email"] as? String
                        self.filtered.append(attendeeDetails)
                    }
                    self.tblAttendees.reloadData()
                } catch {
                    print(error)
                }
            }
            else {
                print("HTTP Status Code = \(HTTPStatusCode)")
                print("Error while loading channel videos: \(String(describing: error))")
            }
            
            // Hide the activity indicator.
            //self.viewWait.isHidden = true
            
            SVProgressHUD.dismiss()
            self.refreshController.endRefreshing()
            
            
            
        })
        
    }
    
}
