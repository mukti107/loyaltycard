//
//  ManageLocationController.swift
//  LoyaltyCard
//
//  Created by Rachindra Poudel on 7/19/17.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit
import MapKit
import SVProgressHUD

class ManageLocationController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var nameOfPlace: UITextField!
    @IBOutlet weak var latitude: UITextField!
    @IBOutlet weak var longitude: UITextField!
    @IBOutlet weak var map: MKMapView!
    
    var placeDetails:[String:String]=[:]
    let atnotation=MKPointAnnotation()
    
    var manager=CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let apply=UIButton.init(type:.custom)
        apply.setTitle("Save", for: .normal)
        apply.addTarget(self, action: #selector(self.save), for: .touchUpInside)
        apply.frame = CGRect.init(x: 0, y: 0, width: 80, height: 24)
        let rbarButton = UIBarButtonItem.init(customView: apply)
        self.navigationItem.rightBarButtonItem = rbarButton
        
        
        let uilgr = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress))
        uilgr.minimumPressDuration = 2.0
        map.addGestureRecognizer(uilgr)
        
        
        atnotation.title="Select Location"
        map.addAnnotation(atnotation)
        
        
        if (placeDetails["name"]?.isEmpty ?? true){
            manager.delegate=self
            manager.requestAlwaysAuthorization()
            manager.startUpdatingLocation()
        }else {
            
            nameOfPlace.text=placeDetails["name"]
            latitude.text=placeDetails["latitude"]
            longitude.text=placeDetails["longitude"]
            let location = CLLocationCoordinate2D(latitude:Double(placeDetails["latitude"]!)!, longitude:Double(placeDetails["longitude"]!)!)
            
            setLocation(location)
            setRegion(location)
            
        }
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKPointAnnotation {
            let pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "myPin")
            
            pinAnnotationView.pinTintColor = .purple
            pinAnnotationView.isDraggable = true
            pinAnnotationView.canShowCallout = true
            pinAnnotationView.animatesDrop = true
            
            return pinAnnotationView
        }
        
        return nil
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        if newState == MKAnnotationViewDragState.ending {
            let droppedAt = view.annotation?.coordinate
            setLocation(droppedAt!)
            
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        if let loc = manager.location{
            
            let location=loc.coordinate
            
            self.setLocation(location)
            self.setRegion(location)
            
            manager.stopUpdatingLocation()
        }
    }
    
    
    func setRegion(_ location:CLLocationCoordinate2D){
        let span:MKCoordinateSpan=MKCoordinateSpan(latitudeDelta: 0.1,longitudeDelta: 0.1)
        let region:MKCoordinateRegion=MKCoordinateRegion(center: location,span: span)
        map.setRegion(region, animated: true)
    }
    
    
    func longPress(gestureRecognizer:UIGestureRecognizer){
        if gestureRecognizer.state == UIGestureRecognizerState.began {
            let touchPoint = gestureRecognizer.location(in: map)
            let newCoordinates = map.convert(touchPoint, toCoordinateFrom: map)
            
            self.setLocation(newCoordinates)
        }
    }
    
    
    func setLocation(_ location:CLLocationCoordinate2D){
        placeDetails["latitude"] = "\(location.latitude )"
        placeDetails["longitude"] = "\(location.longitude )"
        
        atnotation.coordinate=location
        
        self.latitude.text=placeDetails["latitude"]
        self.longitude.text=placeDetails["longitude"]
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func save(){
        
        
        SVProgressHUD.show(withStatus: "Saving Settings...")
        
        placeDetails["name"]=nameOfPlace.text
        
        let id=placeDetails["id"]?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let name=placeDetails["name"]?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let lat=placeDetails["latitude"]?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let long=placeDetails["longitude"]?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        
        
        let endpointurl:String=Settings.baseURL+"/index.php?command=saveLocation&id=\(id ?? "")&name=\(name ?? "")&latitude=\(lat ?? "")&longitude=\(long ?? "")"
        let session = URLSession.shared
        let url=URL(string: endpointurl)
        
        session.dataTask(with: url!, completionHandler: {(data:Data?, response:URLResponse?, error:Error?) -> Void in
            guard let realResponse = response as? HTTPURLResponse,realResponse.statusCode == 200 else {
                print("not 200 response")
                return
            }
            
            
            
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
                SVProgressHUD.dismiss()
            }
            
            
        }).resume()
        
    }
    
    
    
}
