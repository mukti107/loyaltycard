//
//  ManageLocationsController.swift
//  LoyaltyCard
//
//  Created by Rachindra Poudel on 7/19/17.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit
import MapKit
import SVProgressHUD

class ManageLocationsController: UIViewController, UITableViewDelegate,UITableViewDataSource, CLLocationManagerDelegate{
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var table: UITableView!
    
    var locations:NSArray=NSArray()
    var location:[String:String]!
    var manager=CLLocationManager()
    
    var annotations:[MKPointAnnotation]=[]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let apply=UIButton.init(type:.custom)
        apply.setTitle("Add", for: .normal)
        apply.addTarget(self, action: #selector(self.addLocation), for: .touchUpInside)
        apply.frame = CGRect.init(x: 0, y: 0, width: 80, height: 24)
        let rbarButton = UIBarButtonItem.init(customView: apply)
        self.navigationItem.rightBarButtonItem = rbarButton
        
        // Do any additional setup after loading the view.
        
        
        manager.delegate=self
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadLocations()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let loc = manager.location{
            let location=loc.coordinate
            self.setRegion(location)
            manager.stopUpdatingLocation()
        }
    }
    
    func setRegion(_ location:CLLocationCoordinate2D){
        let span:MKCoordinateSpan=MKCoordinateSpan(latitudeDelta: 0.1,longitudeDelta: 0.1)
        let region:MKCoordinateRegion=MKCoordinateRegion(center: location,span: span)
        map.setRegion(region, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch (segue.identifier!){
        case "editLocation":
            let dest = segue.destination as! ManageLocationController
            dest.placeDetails["id"]=self.location["id"]
            dest.placeDetails["name"]=self.location["name"]
            dest.placeDetails["latitude"]=self.location["latitude"]
            dest.placeDetails["longitude"]=self.location["longitude"]
            break
        default:
            break
        }
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            
            self.location=self.locations[indexPath.row] as! [String:String]
            
            self.performSegue(withIdentifier: "editLocation", sender: self)
        }
        edit.backgroundColor = .black
        
        let delete = UITableViewRowAction(style: .normal, title: "delete") { action, index in
            print("share button tapped")
        }
        delete.backgroundColor = .red
        
        return [edit]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "locationCell")!
        
        let location=locations[indexPath.row] as! [String:String]
        
        cell.textLabel?.text=location["name"]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let location = locations[indexPath.row] as! [String:String]
        
        setRegion( CLLocationCoordinate2D(latitude:Double(location["latitude"]!)!, longitude:Double(location["longitude"]!)!))
        
        
    }
    
    
    
    
    func addLocation(){
        performSegue(withIdentifier: "addLocation", sender: self)
    }
    
    public func loadLocations(){
        
        SVProgressHUD.show(withStatus: "Loading locations...")
        
        let endpointurl:String=Settings.baseURL+"/index.php?command=getLocations"
        let session = URLSession.shared
        let url=URL(string: endpointurl)
        
        session.dataTask(with: url!, completionHandler: {(data:Data?, response:URLResponse?, error:Error?) -> Void in
            guard let realResponse = response as? HTTPURLResponse,realResponse.statusCode == 200 else {
                print("not 200 response")
                return
            }
            
            do{
                self.locations = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                
                print(self.locations)
                
            }catch{
                print ("Exception aayo")
            }
            
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.refreshLocations()
            }
            
            
        }).resume()
    }
    
    func refreshLocations(){
        
        map.removeAnnotations(annotations)
        annotations.removeAll()
        for item in locations{
            let location = item as! [String:String]
            let atnotation=MKPointAnnotation()
            atnotation.coordinate=CLLocationCoordinate2D(latitude:Double(location["latitude"]!)!, longitude:Double(location["longitude"]!)!)
            
            atnotation.title=location["name"]
            map.addAnnotation(atnotation)
            annotations.append(atnotation)
        }
        
        self.table.reloadData()
    }
    
    
}
