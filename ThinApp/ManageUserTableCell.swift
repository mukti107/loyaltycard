//
//  ManageUserTableCell.swift
//  Ear2Ground
//
//  Created by Bruce Khin on 12/03/2017.
//  Copyright © 2017 RiskEater. All rights reserved.
//

import UIKit

class ManageUserTableCell: UITableViewCell {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnCheckIN: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
