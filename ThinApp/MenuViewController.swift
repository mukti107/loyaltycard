

import Foundation
import UIKit

class MenuViewController: UIViewController, UIWebViewDelegate {
    
    fileprivate let ShowWKWebViewControllerSegueIdentifer = "showWKWebView"
    
    var refreshController: UIRefreshControl!
    
    @IBOutlet weak var webView: UIWebView!
    // This variable will hold the data being passed from the First View Controller
    var receivedString = ""
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var shareButton: UIButton!
    
    func refresh(sender:AnyObject) {
        // Code to refresh table view
        print("Reloading...")
        
        webView.reload()
        self.refreshController.removeFromSuperview()
        self.refreshController.endRefreshing()
    }
    
    @objc private func refreshWebView() {
        print("Reloading...")
        
        webView.reload()
      
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        webView.delegate = self
        
        webView.backgroundColor = UIColor.clear
        
        refreshController = UIRefreshControl()
        refreshController.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshController.backgroundColor  = UIColor.clear
        
        refreshController.addTarget(self, action: #selector(MenuViewController.refreshWebView), for: .valueChanged)
        
        refreshController.attributedTitle = NSAttributedString(string: "Pull down to refresh...")
        webView.scrollView.addSubview(refreshController)
    
        shareButton.isHidden = true
        
        self.textLabel.text = ""
        
        if let url = URL(string: receivedString) {
            let request = URLRequest(url: url)
            
            let status = Reach().connectionStatus()
            switch status {
            case .unknown, .offline:
                print("Not connected")
    
                /*// Create the alert controller
                let alertController = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                    self.dismiss(animated: true, completion: nil);
                }
                // Add the actions
                alertController.addAction(okAction)
                //alertController.addAction(cancelAction)
                 
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
                */
                
                self.textLabel.text = ""
            
                switch receivedString{
                    
                case "http://sodamodels.com/static/api/loyaltycard/content/about.html":// ABOUT
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"about_page_img")
                    let url1 = Bundle.main.url(forResource: "pricing", withExtension:"html")
                    let request = NSURLRequest(url: url1!)
                    webView.loadRequest(request as URLRequest)
                    break;
                case "http://sodamodels.com/static/api/loyaltycard/content/expertise.html":// FAQs
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"default_content_img")
                    let url3 = Bundle.main.url(forResource: "faqs", withExtension:"html")
                    let request = NSURLRequest(url: url3!)
                    webView.loadRequest(request as URLRequest)
                    break;
                case "http://sodamodels.com/static/api/loyaltycard/content/contact.html":// CONTACT
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"contact_page_img")
                    let url4 = Bundle.main.url(forResource: "contact", withExtension:"html")
                    let request = NSURLRequest(url: url4!)
                    webView.loadRequest(request as URLRequest)
                    break;
    
                default: break
                
                }
            
                
            case .online(.wwan):
                
                
                print("Connected via WWAN")
                
                print("Internet Connection Available!")
                
                switch receivedString{
                    
                case "http://sodamodels.com/static/api/loyaltycard/content/about.html":// ABOUT
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"about_page_img")
                    let url1 = Bundle.main.url(forResource: "pricing", withExtension:"html")
                    let request = NSURLRequest(url: url1!)
                    webView.loadRequest(request as URLRequest)
                    break;
                case "http://sodamodels.com/static/api/loyaltycard/content/expertise.html":// FAQs
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"default_content_img")
                    let url3 = Bundle.main.url(forResource: "faqs", withExtension:"html")
                    let request = NSURLRequest(url: url3!)
                    webView.loadRequest(request as URLRequest)
                    break;
                case "http://sodamodels.com/static/api/loyaltycard/content/contact.html":// CONTACT
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"contact_page_img")
                    let url4 = Bundle.main.url(forResource: "contact", withExtension:"html")
                    let request = NSURLRequest(url: url4!)
                    webView.loadRequest(request as URLRequest)
                    break;
                    
                default: break
                
                }
                
                webView.loadRequest(request)

                
            case .online(.wiFi):
                print("Connected via WiFi")
                print("Internet Connection Available!")
                
                switch receivedString{
                    
                case "http://sodamodels.com/static/api/loyaltycard/content/about.html":// ABOUT
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"about_page_img")
                    let url1 = Bundle.main.url(forResource: "pricing", withExtension:"html")
                    let request = NSURLRequest(url: url1!)
                    webView.loadRequest(request as URLRequest)
                    break;
                case "http://sodamodels.com/static/api/loyaltycard/content/expertise.html":// FAQs
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"default_content_img")
                    let url3 = Bundle.main.url(forResource: "faqs", withExtension:"html")
                    let request = NSURLRequest(url: url3!)
                    webView.loadRequest(request as URLRequest)
                    break;
                case "http://sodamodels.com/static/api/loyaltycard/content/contact.html":// CONTACT
                    self.textLabel.text = ""
                    imageView.image=UIImage(named:"contact_page_img")
                    let url4 = Bundle.main.url(forResource: "contact", withExtension:"html")
                    let request = NSURLRequest(url: url4!)
                    webView.loadRequest(request as URLRequest)
                    break;
                    
                default: break
                }
                
                webView.loadRequest(request)

            }
        }

    }
  
    @IBAction fileprivate func menuButtonPressed(_ buttn: UIButton) {
        self.dismiss(animated: true, completion: nil);
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        refreshController.endRefreshing()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        // SVProgressHUD.dismiss()
        refreshController.endRefreshing()
    }
    
    @IBAction fileprivate func brandBtnPressed(_ button: UIButton) {
        
        self.performSegue(withIdentifier:ShowWKWebViewControllerSegueIdentifer, sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == ShowWKWebViewControllerSegueIdentifer) {
            // set a variable in the second view controller with the String to pass
            let viewController = segue.destination as! WKWebViewVC
            viewController.URLString = "https://getthinapp.com/hi"
            
        }
    }


 }
