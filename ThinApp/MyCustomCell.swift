
import UIKit

class MyCustomCell: UITableViewCell {
    
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var myCellLabel: UILabel!
    @IBOutlet weak var categoryIcon : UIImageView!
    
}
