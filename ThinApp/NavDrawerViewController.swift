
import UIKit


class NavDrawerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // These strings will be the data for the table view cells
    let drawer: [String] = [
        "HOME",
        "FLIP THRU",
        "OUR WORK",
        "NEWS",
        "ABOUT",
        "EXPERTISE",
        "LIVE",
        "PEOPLE",
        "CONTACT",
        "VIDEO",
        "EVENTS",
        "MANAGE EVENTS",
        "MANAGE LOYALTY",
        "LOYALTY SETTINGS",
        "SEND PUSH",
        "LOYALTY STATUSES",
        "LOGIN"
    ]

    // These are the colors of the square views in our table view cells.
    // In a real project you might use UIImages.
    let colors = [UIColor.blue, UIColor.yellow, UIColor.magenta, UIColor.red, UIColor.brown]
    
    // Don't forget to enter this in IB also
    let cellReuseIdentifier = "cell"
    
    var selectedRow = -1
    
    var loadString = ""
    
    @IBOutlet var tableView: UITableView!
    
    let menuSegueIdentifier = "menuWebViewVC"
    
    let mainSegueIdentifier = "main"
    
    let gallerySegueIdentifier = "galleryVC"
    
    let newsSegueIdentifier = "newsVC"
    
    let categorySegueIdentifier = "categoryVC"
    
    let videoSegueIdentifer = "videoWebViewVC"
    
    let inspirationVCSegueIdentifier = "inpirationVC"
    
    let ShowWKWebViewControllerSegueIdentifer = "showWKWebView"

    let videoSegueIdentifier = "videoVC"
    
    let appWraprSegueIdentifier = "appWraprVC"
    
    let trendingVCSegueIdentifier = "trendingVC"
    
    let bookPageVCSegueIdentifier = "bookPageVC"
    
    let featuredShowVCSegueIdentifier = "featuredShowVC"
    
    let updateCardVCSegueIdentifier = "updateCardVC"
    
    let myCardVCSegueIdentifier = "myCardVC"
    
    let scheduleSegueIdentifier = "scheduleVC"
    
    let videoPlayerVCSegueIdentifier = "videoPlayerVC"
    
    let segueEvents = "sequeEvents"
    
    let segueManageEvents = "sequeManageEvents"
    
    let segueManageVisitors = "sequeManageVisitors"
    
    let segueShowSettings = "segueShowSettings"
    
    let seguePush = "seguePush"
    
    let segueLogin = "segueLogin"
    
    let segueContentPage = "segueContentPage"
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.reloadData()
    }
    
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.drawer.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let categoryName = drawer[indexPath.row]
        
        let cell:MyCustomCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! MyCustomCell
        
        //cell.myView.backgroundColor = self.colors[indexPath.row]
        cell.myCellLabel.text = self.drawer[indexPath.row]
        cell.categoryIcon.image = UIImage(named: categoryName)
       

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(!ADMIN && indexPath.row == 14){
            return 0
        }else{
            return 60
        }
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("You tapped cell number \(indexPath.row).")
      
        switch indexPath.row{
            
        case 0:// HOME
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
             break;
        case 1:// LOOK BOOK
            self.performSegue(withIdentifier: bookPageVCSegueIdentifier, sender: self)
            break;
        case 2:// GALLERIES
            self.performSegue(withIdentifier: categorySegueIdentifier, sender: self)
            break;
//        case 3:// GALLERY
//            self.performSegue(withIdentifier: gallerySegueIdentifier, sender: self)
//            break;
        case 3:// NEWS
             loadString = "http://sodamodels.com/static/api/loyaltycard/events/"
             self.performSegue(withIdentifier: featuredShowVCSegueIdentifier, sender: self)
            break;
        case 4:// ABOUT
            loadString="http://sodamodels.com/static/api/loyaltycard/content/about.html"
            performSegue(withIdentifier: menuSegueIdentifier,sender:self);
            break;
        case 5:// FAQ
            loadString="http://sodamodels.com/static/api/loyaltycard/content/expertise.html"
            performSegue(withIdentifier: menuSegueIdentifier,sender:self);
            break;
        case 6:// REELS
            performSegue(withIdentifier: videoSegueIdentifier,sender:self);
            break;
        case 7:// INSPIRATION
            self.performSegue(withIdentifier: inspirationVCSegueIdentifier, sender: self)
            break;
        case 8:// CONTACT
            loadString="http://sodamodels.com/static/api/loyaltycard/content/contact.html"
            self.performSegue(withIdentifier: menuSegueIdentifier, sender: self)
            break;
        case 9:// VIDEO
            self.performSegue(withIdentifier: videoPlayerVCSegueIdentifier, sender: self)
            break;
        case 10:// EVENTS
            loadString = "https://thnapp.com/api/webservices/index.php?command=getEvents"
            self.performSegue(withIdentifier: segueEvents, sender: self)
            break;
        case 11:// MANAGE EVENT
            loadString = "https://thnapp.com/api/webservices/index.php?command=getEvents"
            self.performSegue(withIdentifier: segueManageEvents, sender: self)
            break;
        case 12:// MANAGE VIITORS
            self.performSegue(withIdentifier: segueManageVisitors, sender: self)
            break;
        case 13:
            self.performSegue(withIdentifier: segueShowSettings, sender: self)
            break
        case 14: // SEND PUSH
            loadString="http://sodamodels.com/static/api/loyaltycard/pushnotifications/"
            self.performSegue(withIdentifier: menuSegueIdentifier, sender: self)
            break
        case 15: // Loyalty Statuses
            loadString="http://sodamodels.com/static/api/loyaltycard/content/statuses.html"
            performSegue(withIdentifier: segueContentPage,sender:self);
        case 16: // LOGIN PIN CODE
            self.performSegue(withIdentifier: segueLogin, sender: self)
            break
        default: break
        }
    }
    
    // This function is called before the segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let controller = segue.destination as? VideoWebVC,
            let URLString = sender as? String {
            controller.receivedString = URLString
            
            
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)

        }
            // get a reference to the second view controller
        if (segue.identifier == menuSegueIdentifier) {
            // set a variable in the second view controller with the String to pass
            
               let secondViewController = segue.destination as! MenuViewController
               secondViewController.receivedString = loadString
            
                /*let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
                UIView.transition(with: appDelegate.window!, duration: 0.5, options: .transitionFlipFromLeft , animations: { () -> Void in
                appDelegate.window!.rootViewController = secondViewController
                }, completion:nil)*/
            
                let drawerController = navigationController?.parent as? KYDrawerController
                drawerController?.setDrawerState(.closed, animated: true)
            
            //  let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //  let mainVC = storyboard.instantiateViewControllerWithIdentifier("secondVC") as! UIViewController
            //
            //  let secondViewController = segue.destination as! UINavigationController
            //  let targetController = secondViewController.topViewController as! MenuViewController
            //  targetController.receivedString = loadString
            
        }
        if (segue.identifier == mainSegueIdentifier) {
            // set a variable in the second view controller with the String to pass
            
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == gallerySegueIdentifier) {
            // set a variable in the second view controller with the String to pass
            
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == newsSegueIdentifier) {
            // set a variable in the second view controller with the String to pass
            // let secondViewController = segue.destination as! NewsVC
            // secondViewController.receivedString = loadString
            let secondViewController = segue.destination as! UINavigationController
            let targetController = secondViewController.topViewController as! NewsVC
            targetController.receivedString = loadString
            
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == categorySegueIdentifier) {
            // set a variable in the second view controller with the String to pass
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }

        if (segue.identifier == inspirationVCSegueIdentifier) {
            // set a variable in the second view controller with the String to pass
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == ShowWKWebViewControllerSegueIdentifer) {
            // set a variable in the second view controller with the String to pass
            let viewController = segue.destination as! WKWebViewVC
            viewController.URLString = loadString
            
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == videoSegueIdentifier) {
            
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == appWraprSegueIdentifier) {
            
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == trendingVCSegueIdentifier) {
            // set a variable in the second view controller with the String to pass
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == featuredShowVCSegueIdentifier) {
            
            //let viewController = segue.destination as! InspirationVC
            //viewController.URLString = loadString
            
            let secondViewController = segue.destination as! UINavigationController
            let targetController = secondViewController.topViewController as! FeaturedShowsVC
            targetController.URLString = loadString
            
            // set a variable in the second view controller with the String to pass
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        
        
        if (segue.identifier == bookPageVCSegueIdentifier) {

        }
        
        if (segue.identifier == updateCardVCSegueIdentifier) {
            
            //let viewController = segue.destination as! InspirationVC
            //viewController.URLString = loadString
        
            // set a variable in the second view controller with the String to pass
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == myCardVCSegueIdentifier) {
            
            //let viewController = segue.destination as! InspirationVC
            //viewController.URLString = loadString
            
            // set a variable in the second view controller with the String to pass
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }

        if (segue.identifier == scheduleSegueIdentifier) {
            
            //let viewController = segue.destination as! InspirationVC
            //viewController.URLString = loadString
            
            let secondViewController = segue.destination as! UINavigationController
            let targetController = secondViewController.topViewController as! ScheduleVC
            targetController.URLString = loadString
            
            // set a variable in the second view controller with the String to pass
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        
        if (segue.identifier == videoPlayerVCSegueIdentifier) {
            
            //let viewController = segue.destination as! InspirationVC
            //viewController.URLString = loadString
            
            // set a variable in the second view controller with the String to pass
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == segueEvents) {
            
            let secondViewController = segue.destination as! UINavigationController
            let targetController = secondViewController.topViewController as! EventsShowVC
            targetController.URLString = loadString
            
            // set a variable in the second view controller with the String to pass
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == segueManageEvents) {
               
            let secondViewController = segue.destination as! UINavigationController
            let targetController = secondViewController.topViewController as! ManageEventsVC
            targetController.URLString = loadString
            
            // set a variable in the second view controller with the String to pass
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == seguePush) {
            
            //let secondViewController = segue.destination as! UINavigationController
            //let targetController = secondViewController.topViewController as! ManageEventsVC
            //targetController.URLString = loadString
            
            // set a variable in the second view controller with the String to pass
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == segueLogin) {
     
            
            // set a variable in the second view controller with the String to pass
            let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.closed, animated: true)
        }
        
        if (segue.identifier == segueContentPage) {
            // set a variable in the second view controller with the String to pass
            
            let vc = segue.destination as! ContentPageViewController
            vc.receivedString = loadString
            
        }
    }
}

