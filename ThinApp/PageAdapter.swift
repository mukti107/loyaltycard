
import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol PageAdapterDelegate: class {
    func pageAdapter(_ pageAdapter: PageAdapter, didMoveToViewControllerAtIndex index: Int)
}

class PageAdapter: NSObject, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    weak var delegate: PageAdapterDelegate?
    
    var pageViewController: UIPageViewController? {
        didSet {
            self.pageViewController?.delegate = self
            self.pageViewController?.dataSource = self
            
            self.setupViewControllers()
        }
    }
    var storedViewControllers: [UIViewController]?
    var pageIndex: Int = 0
    
    func setupViewControllers() {}
    
    func moveToViewControllerAtIndex(_ index: Int) {
        if index > self.storedViewControllers?.count {
            return
        }
        if let controllers = self.storedViewControllers {
            let navigationDirection: UIPageViewControllerNavigationDirection = index < self.pageIndex ?
                .reverse : .forward
            self.pageViewController?.setViewControllers([controllers[index]], direction: navigationDirection,
                animated: true, completion: nil)
            self.pageIndex = index
        }
    }
    
    //MARK: UIPageViewControllerDataSource methods
    
    func pageViewController(_ pageViewController: UIPageViewController,
        viewControllerBefore viewController: UIViewController) -> UIViewController? {
            if let controllers = self.storedViewControllers,
                let index = controllers.index(of: viewController) {
                    self.pageIndex = index
                    let previousIndex = index - 1
                    if previousIndex >= 0 {
                        return controllers[previousIndex]
                    }
            }
            return nil
    }
    
    //MARK: UIPageViewControllerDelegate method
    
    func pageViewController(_ pageViewController: UIPageViewController,
        viewControllerAfter viewController: UIViewController) -> UIViewController? {
            if let controllers = self.storedViewControllers,
                let index = controllers.index(of: viewController) {
                    self.pageIndex = index
                    let nextIndex = index + 1
                    if nextIndex < controllers.count {
                        return controllers[nextIndex]
                    }
            }
            return nil
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool,
        previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
            if let viewController = pageViewController.viewControllers?[0],
                let controllers = self.storedViewControllers,
                let index = controllers.index(of: viewController) {
                    self.delegate?.pageAdapter(self, didMoveToViewControllerAtIndex: index)
            }
    }

}
