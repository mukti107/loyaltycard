//
//  QRCodeViewController.swift
//  LoyaltyCard
//
//  Created by Rachindra Poudel on 8/28/17.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit
import QRCode

class QRCodeViewController: UIViewController {

    var text:String!
    @IBOutlet weak var qrCodeImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let image = QRCode(text)
        qrCodeImage.image=image?.image
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
