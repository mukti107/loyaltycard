//
//  ReverseScannerController.swift
//  LoyaltyCard
//
//  Created by Rachindra Poudel on 8/27/17.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit
import AVFoundation
import CoreLocation
import SVProgressHUD


class ReverseScannerController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var qrCodeMarkView: UIImageView!
    
    var scannedValue:String?
    var locValue:CLLocationCoordinate2D?
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    let locationManager = CLLocationManager()

    
    let supportedCodeTypes = [AVMetadataObjectTypeUPCECode,
                              AVMetadataObjectTypeCode39Code,
                              AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeCode93Code,
                              AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypeEAN8Code,
                              AVMetadataObjectTypeEAN13Code,
                              AVMetadataObjectTypeAztecCode,
                              AVMetadataObjectTypePDF417Code,
                              AVMetadataObjectTypeQRCode]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate=self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        self.navigationItem.title = "Scan for Rewards"
        
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back_arrow.png"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(popSelf), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        
        /* HIDE UINavigationBar Border */
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)

        // Do any additional setup after loading the view.
        
        
        setupCamera()
        
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locValue = manager.location!.coordinate
        print("locations = \(locValue?.latitude) \(locValue?.longitude)")
        manager.stopUpdatingLocation()
        
        requestIfReady()
        
    }
    
    func popSelf() {
        self.dismiss(animated: true, completion: nil);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupCamera(){
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = self.view.frame
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Move the message label and top bar to the front
            //            view.bringSubview(toFront: messageLabel)
            //            view.bringSubview(toFront: topbar)
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubview(toFront: qrCodeFrameView)
            }
            view.bringSubview(toFront: qrCodeMarkView)
            
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        captureSession?.startRunning()
        
    }

 
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            var rt = barCodeObject!.bounds as CGRect
            rt.origin.y += (qrCodeMarkView?.frame.origin.y)!
            qrCodeFrameView?.frame = rt
            
            if metadataObj.stringValue != nil {
                self.scannedValue = metadataObj.stringValue
                requestIfReady()
        }
        
        captureSession?.stopRunning()
    }
    }
    
    
    func requestIfReady(){
        print("request")
        if let lv=locValue,let qr=scannedValue{
            
            
            var udid = (UIDevice.current.identifierForVendor?.uuidString)!
            var name=UIDevice.current.name
            
            let code=qr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                udid=udid.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                name=name.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            
            let url=Settings.baseURL+"/index.php?command=reverseScan&coupon=\(code)&latitude=\(lv.latitude)&longitude=\(lv.longitude)&udid=\(udid)&name=\(name)"
            let targetURL=URL(string:url)
            
            performGetRequest(targetURL, completion: {(data,HTTPStatusCode, erro) -> Void in
                
                SVProgressHUD.dismiss()
                
                if HTTPStatusCode == 200 && erro == nil {
                    do{
                        let response = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        print(response)
                        if let success = response["success"] as? String {
                            self.popSelf()
                        }else if let error = response["error"] as? String{
                            self.showAlert("Error", error)
                        }
                        
                    }catch {
                        print(error)
                    }
                    
                }else{
                    self.showAlert("Error", "Please check the connection")
                }
            } )
        }
    }
    
    
    func performGetRequest(_ targetURL: URL!, completion:@escaping (_ data: Data?, _ HTTPStatusCode: Int, _ error: Error?) -> Void) {
        
        var request = URLRequest(url: targetURL)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let sessionConfiguration = URLSessionConfiguration.default
        
        let Session = URLSession(configuration: sessionConfiguration)
        
        let tasksession = Session.dataTask(with: request) { data, response, error in
            if data != nil{
                DispatchQueue.main.async {
                    completion(data, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            else
            {
                print("Connection Lost")
                DispatchQueue.main.async {
                    completion(nil, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            
        }
        tasksession.resume()
    }
    
    func showAlert(_ title: String, _ message: String) {
        
        // create the alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {action in
            self.popSelf()
        }))
        
        // show the alert
        self.present(alert, animated: true, completion:nil )
    }


}
