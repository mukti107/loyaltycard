//
//  TicketQRCodeVC.swift
//  Ear2Ground
//
//  Created by Bruce Khin on 11/03/2017.
//  Copyright © 2017 RiskEater. All rights reserved.
//

import UIKit
import QRCode

class RewardsViewController: UIViewController {

    @IBOutlet weak var lblEventTitle: UILabel!
    
    @IBOutlet weak var info: UILabel!
    
    @IBOutlet weak var imgQRCode: UIImageView!
    
    @IBOutlet weak var totalStamps: UILabel!
    
    @IBOutlet weak var stampImage: UIImageView!
    var udid:String!
    var coupons:[String] = []
    
    var loadString = ""
    
    let segueContentPage = "segueContentPage"
    
    @IBOutlet weak var rewardDescription: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateRewards), name: NSNotification.Name(rawValue: "rewardReceived"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver("ticketReceived")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateRewards), name: .UIApplicationWillEnterForeground, object: nil)
        
        
        if let storedCoupons=UserDefaults.standard.array(forKey: "coupons") as? [String]{
            coupons=storedCoupons
        }
        
        if let data=UserDefaults.standard.data(forKey: "stamp"){
            self.stampImage.image = UIImage(data:data)!
        }
        
        udid = UIDevice.current.identifierForVendor?.uuidString
        
//        let imageView = UIImageView(frame: CGRect(x: 0, y: 0 , width: 50, height: 40))
//        imageView.contentMode = .scaleAspectFit
//        //imageView.center = (self.navigationController?.navigationBar.center)!
//        let image = UIImage(named: "logo_slim.png")
//        imageView.image = image
//        
//        navigationItem.titleView = imageView
        
        
    
        totalStamps.text="\(UserDefaults.standard.integer(forKey:"total_visits"))"
        
        
        updateRewards()
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0 , width: 128, height: 20))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "my_rewards_img.png")
        imageView.image = image
        
        navigationItem.titleView = imageView
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back_arrow_red.png"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(backPressed), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 15, y: 20, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        // button.imageEdgeInsets = UIEdgeInsetsMake(-3, -15, 3, 15)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let buttonRight = UIButton.init(type: .custom)
        buttonRight.setImage(UIImage.init(named: ""), for: UIControlState.normal)
        buttonRight.frame = CGRect.init(x: 15, y: 15, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        // buttonRight.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15)
        let barButtonRight = UIBarButtonItem.init(customView: buttonRight)
        self.navigationItem.rightBarButtonItem = barButtonRight

        
//        navigationItem.titleView = imageView
        
        self.generateQRCode()
        // Do any additional setup after loading the view.
        
        
        if let description=UserDefaults.standard.string(forKey:"reward_description"){
            rewardDescription.text=description
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
    
    
    
  
    @IBAction func segueContetPagePressed(_ sender: Any) {
      
        loadString="http://sodamodels.com/static/api/loyaltycard/content/statuses.html"
        performSegue(withIdentifier: segueContentPage,sender:self);
        
        
    }
    
    
    /*
    // MARK: - Navigation
 */
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == segueContentPage) {
            // set a variable in the second view controller with the String to pass
            
            let vc = segue.destination as! ContentPageViewController
            vc.receivedString = loadString
    
        }
    }
 

    func generateQRCode(){
        
        if coupons.count==0{
            info.text="You dont have any rewards yet."
            imgQRCode.image=nil
            return
        }
        
    
        let boldText  = String(coupons.count)
        let attrs = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 15)]
        let couponCnt = NSMutableAttributedString(string:boldText, attributes:attrs)
        
        info.text="You have \(coupons.count) reward(s)!"
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: ["type":"coupons","coupons":self.coupons], options: .prettyPrinted)
            
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            let qrCode = QRCode(jsonString)
            imgQRCode.image = qrCode?.image
        }catch{
            print(error.localizedDescription)
        }
    }
    
    
    func updateRewards(){
        
//        showCoupons(UserDefaults.standard.integer(forKey: "tickets"))
        
        let udidEncoded = udid.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let url=Settings.baseURL+"/index.php?command=myRewardCoupons&udid="+udidEncoded!
        let targetURL=URL(string:url)
        
        
        performGetRequest(targetURL, completion: {(data,HTTPStatusCode, erro) -> Void in
            
            if HTTPStatusCode == 200 && erro == nil {
                do{
                    let response = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    print(response)
                    
                    if let coupons = response["coupons"] as? [String] {
                        self.coupons=coupons
                        UserDefaults.standard.set(coupons, forKey:"coupons")
                        DispatchQueue.main.async {
                            self.generateQRCode()
                        }
                    }
                    
                }catch {
                    print(error)
                }
                
            }else{
            }
        } )
    }
    
    
    func performGetRequest(_ targetURL: URL!, completion:@escaping (_ data: Data?, _ HTTPStatusCode: Int, _ error: Error?) -> Void) {
        
        var request = URLRequest(url: targetURL)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let sessionConfiguration = URLSessionConfiguration.default
        
        let Session = URLSession(configuration: sessionConfiguration)
        
        let tasksession = Session.dataTask(with: request) { data, response, error in
            if data != nil{
                DispatchQueue.main.async {
                    completion(data, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            else
            {
                print("Connection Lost")
                DispatchQueue.main.async {
                    completion(nil, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            
        }
        tasksession.resume()
    }

    

}
