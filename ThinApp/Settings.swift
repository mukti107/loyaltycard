
import Foundation

class Settings {
    
    static let appSettings = Settings(userDefaults: UserDefaults.standard)
    
    static let baseURL:String = "http://loyaltycard.dev11.com/api/webservices"
    
    fileprivate init(userDefaults: UserDefaults? = nil) {
        self.userDefaults = userDefaults
    }
    fileprivate let userDefaults: UserDefaults?
    
    
}
