//
//  SettingsViewController.swift
//  LoyaltyCard
//
//  Created by Rachindra Poudel on 7/16/17.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import Photos
import TOCropViewController


class SettingsViewController: UIViewController,UINavigationControllerDelegate , UIImagePickerControllerDelegate,UITextViewDelegate {
    
    @IBOutlet weak var vCardMode: UISwitch!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var emailAddress: UITextField!
    @IBOutlet weak var url: UITextField!
    @IBOutlet weak var smsTextNumber: UITextField!
    @IBOutlet weak var reverseScannerMode: UISwitch!
    @IBOutlet weak var limitUserPerDay: UITextField!
    @IBOutlet weak var adminQRValue: UITextField!
    
    
    @IBOutlet weak var stampsRequired: UITextField!
    @IBOutlet weak var rewardDescription: UITextView!
    @IBOutlet weak var nearbyNotificationText: UITextView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var stamp: UIImageView!
    @IBOutlet weak var background: UIImageView!
    var imageFor:String!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var imagePicker = UIImagePickerController()
    
    var logoImage:UIImage=UIImage()
    var stampImage:UIImage=UIImage()
    var backgroundImage:UIImage=UIImage()
    
    
    @IBAction func manageLocations(_ sender: Any) {
        performSegue(withIdentifier: "manageLocations", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let data=UserDefaults.standard.data(forKey: "bg") {
            backgroundImage = UIImage(data:data)!
        }
        
        if let data=UserDefaults.standard.data(forKey: "logo")  {
            logoImage = UIImage(data:data)!
        }
        
        if let data=UserDefaults.standard.data(forKey: "stamp") {
            stampImage = UIImage(data:data)!
        }
        
        showImages()
        
        stampsRequired.text="\(UserDefaults.standard.integer(forKey:"tickets_for_reward"))"
        if let description=UserDefaults.standard.string(forKey:"reward_description"){
            rewardDescription.text=description
        }
        
        if let nearby=UserDefaults.standard.string(forKey:"nearby_notification"){
            nearbyNotificationText.text=nearby
        }
        
        vCardMode.setOn(UserDefaults.standard.bool(forKey: "vcard_mode"), animated: true)
        
        if let description=UserDefaults.standard.string(forKey:"phone_number"){
            phoneNumber.text=description
        }
        if let description=UserDefaults.standard.string(forKey:"email_address"){
            emailAddress.text=description
        }
        if let description=UserDefaults.standard.string(forKey:"custom_url"){
            url.text=description
        }
        if let description=UserDefaults.standard.string(forKey:"sms_text_number"){
            smsTextNumber.text=description
        }
        if let description=UserDefaults.standard.string(forKey:"limit_user_per_day"){
            limitUserPerDay.text=description
        }
        if let description=UserDefaults.standard.string(forKey:"admin_qr_value"){
            adminQRValue.text=description
        }
        
        
        reverseScannerMode.setOn(UserDefaults.standard.bool(forKey: "reverse_scanner_mode"), animated: true)
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap))
        view.addGestureRecognizer(tapGesture)
        
        let selectLogo = UITapGestureRecognizer(target: self, action: #selector(self.selectLogo))
        let selectStamp = UITapGestureRecognizer(target: self, action: #selector(self.selectStamp))
        let selectBackground = UITapGestureRecognizer(target: self, action: #selector(self.selectBackground))
        
        logo.isUserInteractionEnabled=true
        logo.addGestureRecognizer(selectLogo)
        
        stamp.isUserInteractionEnabled=true
        stamp.addGestureRecognizer(selectStamp)
        
        background.isUserInteractionEnabled=true
        background.addGestureRecognizer(selectBackground)
        
        
        
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back_arrow.png"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(popSelf), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let apply=UIButton.init(type:.custom)
        apply.setTitle("Apply", for: .normal)
        apply.addTarget(self, action: #selector(self.apply), for: .touchUpInside)
        apply.frame = CGRect.init(x: 0, y: 0, width: 80, height: 24)
        let rbarButton = UIBarButtonItem.init(customView: apply)
        self.navigationItem.rightBarButtonItem = rbarButton
        
        // Do any additional setup after loading the view.
    }
    
    
    func showImages(){
        stamp.image=stampImage
        logo.image=logoImage
        background.image=backgroundImage
    }
    
    func apply(){
        
        SVProgressHUD.show(withStatus: "Saving Settings...")
        
        let logoString = UIImagePNGRepresentation(logoImage)
        let stampString = UIImagePNGRepresentation(stampImage)
        let backgroundString = UIImagePNGRepresentation(backgroundImage)
        let url = try! URLRequest(url: URL(string:"\(Settings.baseURL)/index.php?command=saveSettings")!, method: .post, headers: nil)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(logoString!, withName: "logo", fileName: "logo.png", mimeType: "image/png")
                multipartFormData.append(backgroundString!, withName: "bg", fileName: "bg.png", mimeType: "image/png")
                multipartFormData.append(stampString!, withName: "stamp", fileName: "stamp.png", mimeType: "image/png")
                multipartFormData.append(self.rewardDescription.text.data(using: .utf8)!, withName: "reward_description")
                multipartFormData.append(self.nearbyNotificationText.text.data(using: .utf8)!, withName: "nearby_notification")
                multipartFormData.append((self.stampsRequired.text?.data(using: .utf8))!, withName: "stamps_required")
                
                multipartFormData.append(self.vCardMode.isOn.description.data(using: .utf8)!, withName: "vcard_mode")
                multipartFormData.append((self.phoneNumber.text?.data(using: .utf8)!)!, withName: "phone_number")
                multipartFormData.append((self.emailAddress.text?.data(using: .utf8)!)!, withName: "email_address")
                multipartFormData.append((self.url.text?.data(using: .utf8)!)!, withName: "custom_url")
                multipartFormData.append((self.smsTextNumber.text?.data(using: .utf8)!)!, withName: "sms_text_number")
                multipartFormData.append(self.reverseScannerMode.isOn.description.data(using: .utf8)!, withName: "reverse_scanner_mode")
                multipartFormData.append((self.limitUserPerDay.text?.data(using: .utf8)!)!, withName: "limit_user_per_day")
                multipartFormData.append((self.adminQRValue.text?.data(using: .utf8)!)!, withName: "admin_qr_value")
        },
            with: url,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    print("success")
                    upload.responseJSON { response in
                        SVProgressHUD.dismiss()
                        self .showAlert("Saved","Settings Saved succesfully")
                    }
                case .failure( _):
                    print("failure")
                    SVProgressHUD.dismiss()
                    self .showAlert("Error","Error saving settings")
                    break
                }
        })
        
    }
    
    func showAlert(_ title:String, _ message:String){
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
    self.present(alert, animated: true, completion: nil)
    }
    
    func popSelf() {
        self.dismiss(animated: true, completion: nil);
    }
    
    func selectLogo(){
        selectImage(imageFor: "logo")
    }
    
    func selectStamp(){
        selectImage(imageFor: "stamp")
    }
    
    func selectBackground(){
        selectImage(imageFor: "background")
    }
    
    func selectImage(imageFor:String){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            
            
            self.imageFor=imageFor
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        if rewardDescription.canResignFirstResponder{
            rewardDescription.resignFirstResponder()
        }
        if stampsRequired.canResignFirstResponder{
            stampsRequired.resignFirstResponder()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker .dismiss(animated: true, completion: nil)
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        self.presentCropViewController(image: chosenImage)
        
        
    }
    
    
    func presentCropViewController(image:UIImage)
    {
        let cropViewController = TOCropViewController.init(image: image)
        cropViewController.delegate = self
        self.present(cropViewController, animated: true, completion: nil)
        cropViewController.toolbar.clampButtonHidden = true
        cropViewController.toolbar.rotateButton.isHidden = true
        cropViewController.toolbar.rotateClockwiseButtonHidden = true
        cropViewController.toolbar.rotateCounterclockwiseButtonHidden = true
        cropViewController.cropView.setAspectRatio(self.imageRatioFor(imageFor), animated: false)
        cropViewController.cropView.cropBoxResizeEnabled = false
        
    }
    
    func imageRatioFor(_ type:String) -> CGSize{
        switch type{
            case "logo" ,"stamp":
                return CGSize(width: 1, height: 1)
        default:
            return CGSize(width: 9, height: 16)
        }
        
    }
    
    
    
    @IBAction func showRewardsQrCode(_ sender: Any) {
        performSegue(withIdentifier: "showQR", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier!
        {
            case "showQR":
                let controller = segue.destination as! QRCodeViewController
                controller.text = adminQRValue.text!
            break
            
            default:
            return
        }
        
        
        
    }
    
}


extension SettingsViewController : TOCropViewControllerDelegate{
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        //self.croppedFrame = cropRect
        //self.angle = angle
        //self.imgview.image = image
        //imageChanged = true
        
        switch(imageFor){
        case "logo":
            self.logoImage=image
            logo.image=logoImage
            break
        case "stamp":
            
            stampImage=image
            stamp.image=stampImage
            break
        case "background":
            backgroundImage=image
            background.image=backgroundImage
            break
        default:
            break
        }
        
        //self.btnAdd.setTitle("", for: UIControlState.normal)
        dismiss(animated:true, completion: nil) //5
    }
}
