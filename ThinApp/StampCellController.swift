//
//  StampCellCollectionViewController.swift
//  LoyaltyCard
//
//  Created by Rachindra Poudel on 7/17/17.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit

class StampCellController: UICollectionViewCell {
    @IBOutlet weak var stamp: UIImageView!
    
    public func isAvailable(_ show:Bool){
        stamp.isHidden = !show
    }
    
}
