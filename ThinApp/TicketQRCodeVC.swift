//
//  TicketQRCodeVC.swift
//  Ear2Ground
//
//  Created by Bruce Khin on 11/03/2017.
//  Copyright © 2017 RiskEater. All rights reserved.
//

import UIKit
import QRCode

class TicketQRCodeVC: UIViewController {

    @IBOutlet weak var lblEmailAddress: UILabel!
    @IBOutlet weak var lblEventTitle: UILabel!
    
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    @IBOutlet weak var imgQRCode: UIImageView!
    
    var strEmail:String = ""
    var strPhoneNumber:String = ""
    var strAttendee_id:String = ""
    var intTicketCnt :String = "0"
    var ticketDetailInfo = Dictionary<String, AnyObject>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0 , width: 50, height: 40))
        imageView.contentMode = .scaleAspectFit
        //imageView.center = (self.navigationController?.navigationBar.center)!
        let image = UIImage(named: "logo_slim.png")
        imageView.image = image
        
        navigationItem.titleView = imageView
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back_arrow_red.png"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(backPressed), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 15, y: 20, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        // button.imageEdgeInsets = UIEdgeInsetsMake(-3, -15, 3, 15)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let buttonRight = UIButton.init(type: .custom)
        buttonRight.setImage(UIImage.init(named: ""), for: UIControlState.normal)
        buttonRight.frame = CGRect.init(x: 15, y: 15, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        // buttonRight.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15)
        let barButtonRight = UIBarButtonItem.init(customView: buttonRight)
        self.navigationItem.rightBarButtonItem = barButtonRight

        
        navigationItem.titleView = imageView
        
        lblEmailAddress.text = strEmail
        lblPhoneNumber.text = strPhoneNumber
        self.ticketDetailInfo["email"] = strEmail as AnyObject?
        self.ticketDetailInfo["phone"] = strPhoneNumber as AnyObject?
        self.ticketDetailInfo["ticketCnt"] = intTicketCnt as AnyObject?
        self.ticketDetailInfo["attendee_id"] = strAttendee_id as AnyObject?
        self.generateQRCode()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func generateQRCode(){
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self.ticketDetailInfo, options: .prettyPrinted)
            
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            let qrCode = QRCode(jsonString)
            imgQRCode.image = qrCode?.image
        }catch{
            print(error.localizedDescription)
        }
    }

}
