//
//  TicketVC.swift
//  Ear2Ground
//
//  Created by Bruce Khin on 12/03/2017.
//  Copyright © 2017 RiskEater. All rights reserved.
//

import Foundation

import UIKit
import SVProgressHUD
import SDWebImage

class TicketVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var URLString: String!
    
    @IBOutlet weak var tblTickets: UITableView!
    
    var ticketsArray: Array<Dictionary<String, AnyObject>> = []
    
    var selectedVideoIndex: Int!
    
    fileprivate let ShowTicketDetailControllerSegueIdentifer = "ticketDetailVC"
    
    var refreshController: UIRefreshControl!
    
    
    var desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0 , width: 50, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "app_logo.png")
        imageView.image = image
        
        navigationItem.titleView = imageView
        
        tblTickets.delegate = self
        tblTickets.dataSource = self

        
        /* self.navigationController?.navigationBar.backItem?.title = " "
         UINavigationBar.appearance().barTintColor =  UIColor(red:0.0, green:0.0, blue:0.0, alpha:0.5)
         UINavigationBar.appearance().tintColor = UIColor.white
         UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
         */
        
        //  self.navigationController?.navigationBar.barTintColor = UIColor(red:0.0, green:0.0, blue:0.0, alpha:0.5)
        //self.navigationController?.navigationBar.clipsToBounds = true
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back_arrow_red.png"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(popSelf), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 15, y: 20, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        // button.imageEdgeInsets = UIEdgeInsetsMake(-3, -15, 3, 15)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let buttonRight = UIButton.init(type: .custom)
        buttonRight.setImage(UIImage.init(named: ""), for: UIControlState.normal)
        buttonRight.frame = CGRect.init(x: 15, y: 15, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        // buttonRight.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15)
        let barButtonRight = UIBarButtonItem.init(customView: buttonRight)
        self.navigationItem.rightBarButtonItem = barButtonRight
        
        refreshController = UIRefreshControl()
        refreshController.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshController.backgroundColor  = UIColor.clear
        
        refreshController.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshController.tintColor = UIColor.white
        refreshController.attributedTitle = NSAttributedString(string: "Pull down to refresh...")
        tblTickets.addSubview(refreshController)
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        
        DispatchQueue.main.async{
            self.getInpiration()
        }
        
    }
    
    func refresh() {
        // Code to refresh table view
        print("Reloading...")
        
        DispatchQueue.main.async(execute: { () -> Void in
            
            SVProgressHUD.show(withStatus: "Loading...")
            
            self.tblTickets.reloadData()
            
            self.getInpiration()
            
            self.refreshController.endRefreshing()
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
    func popSelf() {
        self.dismiss(animated: true, completion: nil);
    }
    
    func performGetRequest(_ targetURL: URL!, completion:@escaping (_ data: Data?, _ HTTPStatusCode: Int, _ error: Error?) -> Void) {
        
        var request = URLRequest(url: targetURL)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let sessionConfiguration = URLSessionConfiguration.default
        
        let Session = URLSession(configuration: sessionConfiguration)
        
        let tasksession = Session.dataTask(with: request) { data, response, error in
            if data != nil{
                DispatchQueue.main.async {
                    completion(data, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            else
            {
                print("Connection Lost")
                DispatchQueue.main.async {
                    completion(nil, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            
        }
        tasksession.resume()
    }
    
    func getInpiration() {
        
        
        self.ticketsArray.removeAll(keepingCapacity: false)
        
        self.desiredPlaylistItemDataDict.removeAll(keepingCapacity: false)
        
        self.tblTickets.reloadData()
        
        var urlString: String!
        
        urlString = URLString
        
        // Create a NSURL object based on the above string.
        let targetURL = NSURL(string: urlString)
        
        // Fetch the playlist from Google.
        performGetRequest(targetURL as URL!, completion: { (data, HTTPStatusCode, error) -> Void in
            if HTTPStatusCode == 200 && error == nil {
                do {
                    // Convert the JSON data into a dictionary.
                    var resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, AnyObject>
                    
                    // Get all playlist items ("items" array).
                    
                    if resultsDict["result"] != nil {
                        var items: Array<Dictionary<String, AnyObject>> = resultsDict["result"] as! Array<Dictionary<String, AnyObject>>
                        
                        
                        // Use a loop to go through all video items.
                        for i in 0 ..< items.count {
                            
                            let playlistSnippetDict = (items[i] as Dictionary<String, AnyObject>)
                            
                            // Initialize a new dictionary and store the data of interest.
                            // var desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
                            //desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
                            self.desiredPlaylistItemDataDict["id"] = playlistSnippetDict["id"]
                            
                            self.desiredPlaylistItemDataDict["device_id"] = playlistSnippetDict["device_id"]
                            
                            self.desiredPlaylistItemDataDict["attendee_id"] = playlistSnippetDict["attendee_id"]
                            
                            self.desiredPlaylistItemDataDict["email"] = playlistSnippetDict["email"]
                            
                            self.desiredPlaylistItemDataDict["phone"] = playlistSnippetDict["phone"]
                            
                            self.desiredPlaylistItemDataDict["tickets"] = playlistSnippetDict["tickets"]
                            
                            self.desiredPlaylistItemDataDict["date"] = playlistSnippetDict["date"]
                            
                            self.desiredPlaylistItemDataDict["event_id"] = playlistSnippetDict["event_id"]
                            
                            self.desiredPlaylistItemDataDict["status"] = playlistSnippetDict["status"]
                            self.desiredPlaylistItemDataDict["title"] = playlistSnippetDict["title"]
                            self.desiredPlaylistItemDataDict["short"] = playlistSnippetDict["short"]
                            
                            self.desiredPlaylistItemDataDict["content"] = playlistSnippetDict["content"]
                            self.desiredPlaylistItemDataDict["image"] = playlistSnippetDict["image"]
                            self.desiredPlaylistItemDataDict["link"] = playlistSnippetDict["link"]
                            self.desiredPlaylistItemDataDict["label"] = playlistSnippetDict["label"]
                            
                            self.desiredPlaylistItemDataDict["custom"] = playlistSnippetDict["custom"]
                            self.desiredPlaylistItemDataDict["mm"] = playlistSnippetDict["mm"]
                            self.desiredPlaylistItemDataDict["dd"] = playlistSnippetDict["dd"]
                            self.desiredPlaylistItemDataDict["day"] = playlistSnippetDict["day"]
                            
                            print ("playlistSnippetDict:\(self.desiredPlaylistItemDataDict)")
                            
                            
                            // Append the desiredPlaylistItemDataDict dictionary to the videos array.
                            self.ticketsArray.append(self.desiredPlaylistItemDataDict)
                            
                            self.tblTickets.reloadData()
                            
                        }
                    }
                } catch {
                    print(error)
                }
            }
            else {
                print("HTTP Status Code = \(HTTPStatusCode)")
                print("Error while loading channel videos: \(String(describing: error))")
            }
            
            // Hide the activity indicator.
            //self.viewWait.isHidden = true
            
            SVProgressHUD.dismiss()
            self.refreshController.endRefreshing()
            
            
            
        })
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: IBAction method implementation
    @IBAction func changeContent(_ sender: AnyObject) {
        tblTickets.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableViewRowAnimation.fade)
    }
    
    // MARK: UITableView method implementation
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ticketsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell = UITableViewCell()
        //var cell: UITableViewCell!
        
        cell = tableView.dequeueReusableCell(withIdentifier: "idCellVideo", for: indexPath)
        
        let videoDetails = ticketsArray[indexPath.row]
        
        let videoTitle = cell.viewWithTag(10) as! UILabel
        
        let videoContent = cell.viewWithTag(11) as! UILabel
        
        let videoThumbnail = cell.viewWithTag(12) as! UIImageView
        
        //let mmContent = cell.viewWithTag(13) as! UILabel
        
        //let ddContent = cell.viewWithTag(14) as! UILabel
        
        //let dayContent = cell.viewWithTag(15) as! UILabel
        
        let shortDate = cell.viewWithTag(16) as! UILabel
        
        videoTitle.text = videoDetails["title"] as? String
        
        videoContent.text = videoDetails["custom"] as? String
        
        shortDate.text = videoDetails["short"] as? String
        
        
        
        //mmContent.text = videoDetails["mm"] as? String
        //ddContent.text = videoDetails["dd"] as? String
        //dayContent.text = videoDetails["day"] as? String
        
        if let url = URL(string: videoDetails["image"] as! String ){
            DispatchQueue.main.async {
                SDWebImageManager.shared().downloadImage(with: url, options: .continueInBackground, progress: {
                    (receivedSize :Int, ExpectedSize :Int) in
                    videoThumbnail.image = UIImage(named: "loading_img")
                }, completed: {
                    (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    videoThumbnail.image = image
                    
                })
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 215.0
        //return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedVideoIndex = indexPath.row
        
        performSegue(withIdentifier: ShowTicketDetailControllerSegueIdentifer, sender: self)
        
    }
    
    // MARK: UITextFieldDelegate method implementation
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    // override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // get a reference to the second view controller
        if (segue.identifier == ShowTicketDetailControllerSegueIdentifer) {
            // set a variable in the second view controller with the String to pass
            
            
            let secondViewController = segue.destination as! UINavigationController
            let ticketDetailVC = secondViewController.topViewController as! TicketQRCodeVC
            
           // let ticketDetailVC = segue.destination as! TicketQRCodeVC
            ticketDetailVC.strEmail = ticketsArray[selectedVideoIndex]["email"] as! String
            ticketDetailVC.strPhoneNumber = ticketsArray[selectedVideoIndex]["phone"] as! String
            ticketDetailVC.intTicketCnt = ticketsArray[selectedVideoIndex]["tickets"] as! String
            ticketDetailVC.strAttendee_id = ticketsArray[selectedVideoIndex]["attendee_id"] as! String
            
            
            
           
        }
    }
}
