
import UIKit

class TutorialViewController: UIViewController {
    
    @IBOutlet fileprivate weak var coverImageView: UIImageView?
    
    var imageName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let imageName = self.imageName {
            self.coverImageView?.image = UIImage(named: imageName)
        }
    }
}
