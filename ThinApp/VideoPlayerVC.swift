//
//  VideoPlayerVC.swift
//  PearlFisher
//
//  Created by Brian Roy Villanueva on 19/06/2017.
//  Copyright © 2017 ThinApp. All rights reserved.
//

import UIKit

import SwiftVideoBackground

class VideoPlayerVC: UIViewController {
    

    @IBOutlet weak var backgroundVideo: BackgroundVideo!
    
    let categoryVCSegueIdentifier = "categoryVC"
    
    let segueContact = "segueContact"
    
    var loadString = ""
 
    var button: UIButton! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgroundVideo.createBackgroundVideo(name: "Background", type: "mov", alpha: 0.5)
        
        //let image = UIImage(named: "back_btn") as UIImage?
        button = UIButton(frame: CGRect(x: 15, y: 20, width: 24, height: 24))
        //button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(popSelf), for: .touchUpInside)
        
        //NORMAL
        //button.setImage(UIImage(named:"back_image.png"),for:UIControlState.normal)
        
        // TINT BUTTON COLOR
        let origImage = UIImage(named: "back_arrow.png");
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        button.setImage(tintedImage, for: .normal)
        //button.tintColor = UIColor.red
        button.tintColor =  UIColor(red: 255/0.0, green: 0/0.0, blue: 0/0.0, alpha: 1.0)

        self.view.addSubview(button)
    }
    
    func popSelf() {
        
        //print("Button tapped")
        super.dismiss(animated: true, completion: nil);
        
    }
    
    @IBAction fileprivate func actionBtnPressed(_ button: UIButton) {
        
        //self.performSegue(withIdentifier:categoryVCSegueIdentifier, sender: self)
        
        loadString="http://sodamodels.com/static/api/loyaltycard/content/contact.html"
        self.performSegue(withIdentifier: segueContact, sender: self)
        
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == categoryVCSegueIdentifier) {
            
        }
        
        if (segue.identifier == segueContact) {
            // set a variable in the second view controller with the String to pass
            
            let secondViewController = segue.destination as! ContactVC
            secondViewController.URLString = loadString
            
            /*let appDelegate = UIApplication.shared.delegate as! AppDelegate
             
             UIView.transition(with: appDelegate.window!, duration: 0.5, options: .transitionFlipFromLeft , animations: { () -> Void in
             appDelegate.window!.rootViewController = secondViewController
             }, completion:nil)*/
            
         
            
        }
    }
    

}
