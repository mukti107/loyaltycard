
import UIKit

class WelcomePageAdapter: PageAdapter {
    
    fileprivate let TutorialViewControllerIdentifier = "TutorialViewControllerIdentifier"
    var imagesNames: [String] = []
    
    override func setupViewControllers() {
        var viewControllers: [UIViewController] = []
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        for imageName in self.imagesNames {
            if let controller = mainStoryboard.instantiateViewController(
                withIdentifier: TutorialViewControllerIdentifier) as? TutorialViewController {
                controller.imageName = imageName
                viewControllers.append(controller)
            }
        }

        self.storedViewControllers = viewControllers
        if let viewController = viewControllers.first {
            self.pageViewController?.setViewControllers([viewController], direction: .forward, animated: true,
                                                        completion: nil)
        }
    }
}
