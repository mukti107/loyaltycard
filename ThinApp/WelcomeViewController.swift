
import UIKit
import CoreLocation

import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import AVFoundation
import SmileLock


class WelcomeViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var actionButton: UIButton!
    
    @IBOutlet weak var menu: UIButton!
   
    let galleryVCSegueIdentifier = "galleryVC"
    
    var loadString: String!

       var music: AVAudioPlayer!

    fileprivate let imagesNames = ["slide1", "slide2", "slide3", "slide4"]

    var currentIndex = 1;
    
    
    var timer = Timer();
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var pageControl: UIPageControl!
    
    var pageCurrent = 0;
    
    var playBtnSelect = false
    
    
    
    @IBOutlet fileprivate weak var playButton: UIButton!
    
    @IBOutlet weak var btnLogin: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.actionButton?.layer.cornerRadius = 5
        //self.actionButton?.layer.borderWidth = 1
        //self.actionButton?.layer.borderColor = UIColor.white.cgColor
        
        self.edgesForExtendedLayout = []
        
        self.scrollView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height)
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        let scrollViewHeight:CGFloat = self.scrollView.frame.height
        
        let imgOne = UIImageView(frame: CGRect(x:0, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgOne.image = UIImage(named: "slide1")
        let imgTwo = UIImageView(frame: CGRect(x:scrollViewWidth, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgTwo.image = UIImage(named: "slide2")
        let imgThree = UIImageView(frame: CGRect(x:scrollViewWidth*2, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgThree.image = UIImage(named: "slide3")
        let imgFour = UIImageView(frame: CGRect(x:scrollViewWidth*3, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgFour.image = UIImage(named: "slide4")
        
        self.scrollView.addSubview(imgOne)
        self.scrollView.addSubview(imgTwo)
        self.scrollView.addSubview(imgThree)
        self.scrollView.addSubview(imgFour)
        //4
        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width * 4, height:self.scrollView.frame.height)
    
        self.pageControl.currentPage = 0
        
        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        
        playButton.setImage(UIImage(named: "playsound"), for: .normal)
        playBtnSelect = true
        playSound()
        
    }
  
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
        // [START subscribe_topic]
        FIRMessaging.messaging().subscribe(toTopic: "/topics/ios")
        print("Subscribed to news topic")
        // [END subscribe_topic]
        self.btnLogin.isHidden = ADMIN
    }
    

    
    // PLAY SOUND
    func playSound() {
        
        let url = Bundle.main.url(forResource: "thinapp", withExtension: "mp3")!
        
        do {
            music = try AVAudioPlayer(contentsOf: url)
            guard let player = music else { return }
            
            player.prepareToPlay()
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    
    func stopSound() {
        if music != nil {
            music.stop()
            music = nil
        }
    }
    
    @IBAction func Play(_ button: UIButton) {
        
        if(!playBtnSelect)
        {
            playButton.setImage(UIImage(named: "playsound"), for: .normal)
            playBtnSelect = true
            playSound()
        }
        else
        {
            playButton.setImage(UIImage(named: "stopsound"), for: .normal)
            playBtnSelect = false
            stopSound()
        }
    }
    
    @IBAction func Stop(_ button: UIButton) {
        stopSound()
    }
    
    @IBAction fileprivate func menuButtonPressed(_ buttn: UIButton) {
        let drawerController = navigationController?.parent as? KYDrawerController
        drawerController?.setDrawerState(.opened, animated: true)
    }
    
    @IBAction fileprivate func actionBtnPressed(_ button: UIButton) {
        
      self.performSegue(withIdentifier:galleryVCSegueIdentifier, sender: self)
        
    }
    
    

    
    func moveToNextPage (){
        
        timer = Timer.scheduledTimer(timeInterval: 0.0, target: self, selector: #selector(self.checkCurrentImage), userInfo: nil, repeats: true)
        
        let pageWidth:CGFloat = self.scrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 4
        let contentOffset:CGFloat = self.scrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth{
            slideToX = 0
        }
        
        //   self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scrollView.frame.height), animated: true)
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scrollView.frame.height), animated: false)
            }, completion: nil)
        }
    }
    
    func checkCurrentImage()
    {
        
        let pageWidth:CGFloat = scrollView.frame.width
        
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        
        self.pageControl?.currentPage = Int(currentPage)
    
    }
    
    //MARK: UIScrollView Delegate
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == galleryVCSegueIdentifier) {
            
        }
    }
    
}

