//
//  CaseStudiesViewController.swift
//  MayaStojan
//

import UIKit
import SVProgressHUD
import SDWebImage

class TrendingVC: UIViewController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var news_scrollview: UIScrollView!
    @IBOutlet weak var tblVideos: UITableView!
    var timer = Timer()
    var refreshController: UIRefreshControl!
    
    @IBOutlet weak var main_scrollview: UIScrollView!
    var newsArray: Array<Dictionary<String, AnyObject>> = []
    var featureComingArray: Array<Dictionary<String, AnyObject>> = []
    var featureExistingArray: Array<Dictionary<String, AnyObject>> = []
    var caseStudiesArray: Array<Dictionary<String, AnyObject>> = []
    var selectedVideo = Dictionary<String, AnyObject>()
    
    fileprivate let ShowWebViewControllerSegueIdentifer = "inspirationDetailVC"
    
    var desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0 , width: 50, height: 40))
        imageView.contentMode = .scaleAspectFit
        //imageView.center = (self.navigationController?.navigationBar.center)!
        let image = UIImage(named: "logo_slim.png")
        imageView.image = image
        
        navigationItem.titleView = imageView
        
        /* self.navigationController?.navigationBar.backItem?.title = " "
         UINavigationBar.appearance().barTintColor =  UIColor(red:0.0, green:0.0, blue:0.0, alpha:0.5)
         UINavigationBar.appearance().tintColor = UIColor.white
         UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
         */
        
        //  self.navigationController?.navigationBar.barTintColor = UIColor(red:0.0, green:0.0, blue:0.0, alpha:0.5)
        //self.navigationController?.navigationBar.clipsToBounds = true
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back_arrow.png"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(popSelf), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 15, y: 20, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        // button.imageEdgeInsets = UIEdgeInsetsMake(-3, -15, 3, 15)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let buttonRight = UIButton.init(type: .custom)
        buttonRight.setImage(UIImage.init(named: ""), for: UIControlState.normal)
        buttonRight.frame = CGRect.init(x: 15, y: 15, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        // buttonRight.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15)
        let barButtonRight = UIBarButtonItem.init(customView: buttonRight)
        self.navigationItem.rightBarButtonItem = barButtonRight
        DispatchQueue.main.async {
            self.getNews()
        }
        
        refreshController = UIRefreshControl()
        refreshController.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshController.backgroundColor  = UIColor.clear
        
        refreshController.addTarget(self, action: #selector(getNews), for: .valueChanged)
        refreshController.tintColor = UIColor.white
        refreshController.attributedTitle = NSAttributedString(string: "Pull down to refresh...")
        main_scrollview.addSubview(refreshController)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
    func popSelf() {
        self.dismiss(animated: true, completion: nil);
    }
    
    func performGetRequest(_ targetURL: URL!, completion:@escaping (_ data: Data?, _ HTTPStatusCode: Int, _ error: Error?) -> Void) {
        
        var request = URLRequest(url: targetURL)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let sessionConfiguration = URLSessionConfiguration.default
        
        let Session = URLSession(configuration: sessionConfiguration)
        
        let tasksession = Session.dataTask(with: request) { data, response, error in
            if data != nil{
                DispatchQueue.main.async {
                    completion(data, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            else
            {
                print("Connection Lost")
                DispatchQueue.main.async {
                    completion(nil, (response as! HTTPURLResponse).statusCode, error)
                }
            }
            
        }
        tasksession.resume()
    }
    
    func getNews() {
        
        SVProgressHUD.show()
        
        self.newsArray.removeAll(keepingCapacity: false)
        self.desiredPlaylistItemDataDict.removeAll(keepingCapacity: false)
        
        //        self.tblVideos.reloadData()
        
        var urlString: String!
        
        urlString = "https://getthinapp.com/push/thinapp/api_news.php"
        
        // Create a NSURL object based on the above string.
        let targetURL = NSURL(string: urlString)
        
        // Fetch the playlist from Google.
        performGetRequest(targetURL as URL!, completion: { (data, HTTPStatusCode, error) -> Void in
            if HTTPStatusCode == 200 && error == nil {
                do {
                    // Convert the JSON data into a dictionary.
                    var resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, AnyObject>
                    
                    // Get all playlist items ("items" array).
                    var items: Array<Dictionary<String, AnyObject>> = resultsDict["ITEMS"] as! Array<Dictionary<String, AnyObject>>
                    
                    
                    // Use a loop to go through all video items.
                    for i in 0 ..< items.count {
                        
                        let playlistSnippetDict = (items[i] as Dictionary<String, AnyObject>)
                        
                        // Initialize a new dictionary and store the data of interest.
                        // var desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
                        //desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
                        self.desiredPlaylistItemDataDict["id"] = playlistSnippetDict["id"]
                        self.desiredPlaylistItemDataDict["title"] = playlistSnippetDict["title"]
                        self.desiredPlaylistItemDataDict["content"] = playlistSnippetDict["content"]
                        self.desiredPlaylistItemDataDict["author"] = playlistSnippetDict["author"]
                        self.desiredPlaylistItemDataDict["image"] = playlistSnippetDict["image"]
                        self.desiredPlaylistItemDataDict["link"] = playlistSnippetDict["link"]
                        self.desiredPlaylistItemDataDict["label"] = playlistSnippetDict["label"]
                        
                        print ("playlistSnippetDict:\(self.desiredPlaylistItemDataDict)")
                        
                        
                        // Append the desiredPlaylistItemDataDict dictionary to the videos array.
                        self.newsArray.append(self.desiredPlaylistItemDataDict)
                        
                    }
                    
                    
                    self.getFeaturesComing()
                } catch {
                    print(error)
                }
            }
            else {
                print("HTTP Status Code = \(HTTPStatusCode)")
                print("Error while loading channel videos: \(String(describing: error))")
            }
            
            // Hide the activity indicator.
            //self.viewWait.isHidden = true
            
            //            SVProgressHUD.dismiss()
            //            self.refreshController.endRefreshing()
        })
        self.refreshController.endRefreshing()
    }
    
    func getFeaturesComing() {
        SVProgressHUD.show()
        
        self.featureComingArray.removeAll(keepingCapacity: false)
        self.desiredPlaylistItemDataDict.removeAll(keepingCapacity: false)
        
        //        self.tblVideos.reloadData()
        
        var urlString: String!
        
        urlString = "https://getthinapp.com/push/thinapp/api_coming.php"
        
        // Create a NSURL object based on the above string.
        let targetURL = NSURL(string: urlString)
        
        // Fetch the playlist from Google.
        performGetRequest(targetURL as URL!, completion: { (data, HTTPStatusCode, error) -> Void in
            if HTTPStatusCode == 200 && error == nil {
                do {
                    // Convert the JSON data into a dictionary.
                    var resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, AnyObject>
                    
                    // Get all playlist items ("items" array).
                    var items: Array<Dictionary<String, AnyObject>> = resultsDict["ITEMS"] as! Array<Dictionary<String, AnyObject>>
                    
                    
                    // Use a loop to go through all video items.
                    for i in 0 ..< items.count {
                        
                        let playlistSnippetDict = (items[i] as Dictionary<String, AnyObject>)
                        
                        // Initialize a new dictionary and store the data of interest.
                        // var desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
                        //desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
                        self.desiredPlaylistItemDataDict["id"] = playlistSnippetDict["id"]
                        self.desiredPlaylistItemDataDict["title"] = playlistSnippetDict["title"]
                        self.desiredPlaylistItemDataDict["content"] = playlistSnippetDict["content"]
                        self.desiredPlaylistItemDataDict["author"] = playlistSnippetDict["author"]
                        self.desiredPlaylistItemDataDict["image"] = playlistSnippetDict["image"]
                        self.desiredPlaylistItemDataDict["link"] = playlistSnippetDict["link"]
                        self.desiredPlaylistItemDataDict["label"] = playlistSnippetDict["label"]
                        
                        print ("playlistSnippetDict:\(self.desiredPlaylistItemDataDict)")
                        
                        
                        // Append the desiredPlaylistItemDataDict dictionary to the videos array.
                        self.featureComingArray.append(self.desiredPlaylistItemDataDict)
                        
                    }
                    
                    
                    self.getFeaturesExisting()
                } catch {
                    print(error)
                }
            }
            else {
                print("HTTP Status Code = \(HTTPStatusCode)")
                print("Error while loading channel videos: \(String(describing: error))")
            }
            
            // Hide the activity indicator.
            //self.viewWait.isHidden = true
            
            //            SVProgressHUD.dismiss()
            //            self.refreshController.endRefreshing()
        })
    }
    
    func getFeaturesExisting() {
        SVProgressHUD.show()
        
        self.featureExistingArray.removeAll(keepingCapacity: false)
        self.desiredPlaylistItemDataDict.removeAll(keepingCapacity: false)
        
        //        self.tblVideos.reloadData()
        
        var urlString: String!
        
        urlString = "https://getthinapp.com/push/thinapp/api_features.php"
        
        // Create a NSURL object based on the above string.
        let targetURL = NSURL(string: urlString)
        
        // Fetch the playlist from Google.
        performGetRequest(targetURL as URL!, completion: { (data, HTTPStatusCode, error) -> Void in
            if HTTPStatusCode == 200 && error == nil {
                do {
                    // Convert the JSON data into a dictionary.
                    var resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, AnyObject>
                    
                    // Get all playlist items ("items" array).
                    var items: Array<Dictionary<String, AnyObject>> = resultsDict["ITEMS"] as! Array<Dictionary<String, AnyObject>>
                    
                    
                    // Use a loop to go through all video items.
                    for i in 0 ..< items.count {
                        
                        let playlistSnippetDict = (items[i] as Dictionary<String, AnyObject>)
                        
                        // Initialize a new dictionary and store the data of interest.
                        // var desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
                        //desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
                        self.desiredPlaylistItemDataDict["id"] = playlistSnippetDict["id"]
                        self.desiredPlaylistItemDataDict["title"] = playlistSnippetDict["title"]
                        self.desiredPlaylistItemDataDict["content"] = playlistSnippetDict["content"]
                        self.desiredPlaylistItemDataDict["author"] = playlistSnippetDict["author"]
                        self.desiredPlaylistItemDataDict["image"] = playlistSnippetDict["image"]
                        self.desiredPlaylistItemDataDict["link"] = playlistSnippetDict["link"]
                        self.desiredPlaylistItemDataDict["label"] = playlistSnippetDict["label"]
                        
                        print ("playlistSnippetDict:\(self.desiredPlaylistItemDataDict)")
                        
                        
                        // Append the desiredPlaylistItemDataDict dictionary to the videos array.
                        self.featureExistingArray.append(self.desiredPlaylistItemDataDict)
                        
                    }
                    
                    self.getCaseStudies()
                } catch {
                    print(error)
                }
            }
            else {
                print("HTTP Status Code = \(HTTPStatusCode)")
                print("Error while loading channel videos: \(String(describing: error))")
            }
            
            // Hide the activity indicator.
            //self.viewWait.isHidden = true
            
            //            SVProgressHUD.dismiss()
            //            self.refreshController.endRefreshing()
        })
    }
    
    func getCaseStudies() {
        SVProgressHUD.show()
        
        self.caseStudiesArray.removeAll(keepingCapacity: false)
        self.desiredPlaylistItemDataDict.removeAll(keepingCapacity: false)
        
        //        self.tblVideos.reloadData()
        
        var urlString: String!
        
        urlString = "https://getthinapp.com/push/thinapp/casestudies.php"
        
        // Create a NSURL object based on the above string.
        let targetURL = NSURL(string: urlString)
        
        // Fetch the playlist from Google.
        performGetRequest(targetURL as URL!, completion: { (data, HTTPStatusCode, error) -> Void in
            if HTTPStatusCode == 200 && error == nil {
                do {
                    // Convert the JSON data into a dictionary.
                    var resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, AnyObject>
                    
                    // Get all playlist items ("items" array).
                    var items: Array<Dictionary<String, AnyObject>> = resultsDict["ITEMS"] as! Array<Dictionary<String, AnyObject>>
                    
                    
                    // Use a loop to go through all video items.
                    for i in 0 ..< items.count {
                        
                        let playlistSnippetDict = (items[i] as Dictionary<String, AnyObject>)
                        
                        // Initialize a new dictionary and store the data of interest.
                        // var desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
                        //desiredPlaylistItemDataDict = Dictionary<String, AnyObject>()
                        self.desiredPlaylistItemDataDict["id"] = playlistSnippetDict["id"]
                        self.desiredPlaylistItemDataDict["title"] = playlistSnippetDict["title"]
                        self.desiredPlaylistItemDataDict["content"] = playlistSnippetDict["content"]
                        self.desiredPlaylistItemDataDict["author"] = playlistSnippetDict["author"]
                        self.desiredPlaylistItemDataDict["image"] = playlistSnippetDict["image"]
                        self.desiredPlaylistItemDataDict["link"] = playlistSnippetDict["link"]
                        self.desiredPlaylistItemDataDict["label"] = playlistSnippetDict["label"]
                        
                        print ("playlistSnippetDict:\(self.desiredPlaylistItemDataDict)")
                        
                        
                        // Append the desiredPlaylistItemDataDict dictionary to the videos array.
                        self.caseStudiesArray.append(self.desiredPlaylistItemDataDict)
                        
                    }
                    
                    self.load_news()
                    self.tblVideos.reloadData()
                } catch {
                    print(error)
                }
            }
            else {
                print("HTTP Status Code = \(HTTPStatusCode)")
                print("Error while loading channel videos: \(String(describing: error))")
            }
            
            // Hide the activity indicator.
            //self.viewWait.isHidden = true
            
            SVProgressHUD.dismiss()
            //            self.refreshController.endRefreshing()
        })
    }
    
    //    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //
    //    }
    //
    //    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
    //
    //
    //    }
    //
    //    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    //        let pageWidth = self.news_scrollview.frame.size.width/2
    //        let fractionPage = self.news_scrollview.contentOffset.x / pageWidth
    //        let pageCurrent = lround(Double(fractionPage))
    //
    //        print("current page:", pageCurrent)
    //        self.page_control.currentPage = pageCurrent
    //        self.news_scrollview.contentOffset = CGPoint(x: pageWidth * CGFloat(pageCurrent), y: 0.0)
    //    }
    //
    //
    func load_news() {
        self.edgesForExtendedLayout = []
        self.news_scrollview.contentSize = CGSize.init(width: self.view.frame.size.width * CGFloat(self.newsArray.count), height: self.view.frame.size.height)
        var indx = 0.0
        
        for news in self.newsArray {
            self.createPageWithImage(news["image"] as! String, page: CGFloat(indx), title: news["title"] as! String)
            indx = indx + 1.0
        }
        
        timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(TrendingVC.slideNext), userInfo: nil, repeats: true)
        
        self.news_scrollview.contentSize = CGSize(width: self.news_scrollview.frame.size.width * CGFloat(newsArray.count), height: self.news_scrollview.frame.size.height)
        self.main_scrollview.contentSize = CGSize(width: self.view.frame.size.width, height: self.news_scrollview.frame.size.height * 4+80)
    }
    
    func slideNext() {
        let pageWidth = self.news_scrollview.frame.size.width
        let fractionPage = self.news_scrollview.contentOffset.x / pageWidth
        var pageCurrent = lround(Double(fractionPage))+1
        if pageCurrent == self.newsArray.count {
            pageCurrent = 0
        }
        self.news_scrollview.setContentOffset(CGPoint(x: pageWidth * CGFloat(pageCurrent), y: 0.0), animated: true)
    }
    
    func createPageWithImage(_ imageURL: String, page: CGFloat, title: String) {
        let imageView = UIImageView(frame: CGRect.init(x: self.news_scrollview.frame.size.width * page, y: 0.0, width: self.news_scrollview.frame.size.width, height: self.news_scrollview.frame.size.height))
        
        DispatchQueue.main.async {
            SDWebImageManager.shared().downloadImage(with: URL(string: imageURL), options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                imageView.image = UIImage(named: "loading_img")
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if image != nil {
                    imageView.image = image
                }
            })
        }
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.layer.masksToBounds = true
        
        let lbl_title = UILabel(frame: CGRect(x: imageView.frame.origin.x, y: imageView.frame.height - 50, width: imageView.frame.size.width, height: 50.0))
        lbl_title.text = title
        lbl_title.textColor = UIColor.white
        lbl_title.font = UIFont.systemFont(ofSize: 22)
        lbl_title.textAlignment = .center
        self.news_scrollview .addSubview(imageView)
        self.news_scrollview.addSubview(lbl_title)
        let button = UIButton(frame: imageView.frame)
        self.news_scrollview.addSubview(button)
        button.tag = Int(page)
        button.addTarget(self, action: #selector(TrendingVC.onclick(_:)), for: .touchUpInside)
        
    }
    
    func onclick(_ sender: UIButton) {
        
        
        selectedVideo = newsArray[sender.tag]
        
        performSegue(withIdentifier: ShowWebViewControllerSegueIdentifer, sender: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return self.news_scrollview.frame.size.height+40
        }
        else if indexPath.row == 1 {
            return self.news_scrollview.frame.size.height
        }
        else  {
            return self.news_scrollview.frame.size.height+40
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        cell = tableView.dequeueReusableCell(withIdentifier: "videoCell", for: indexPath)
        
        let scrollview = cell.viewWithTag(2) as! UIScrollView
        let lbl_title = cell.viewWithTag(1001) as! UILabel
        switch indexPath.row {
        case 0:
            lbl_title.text = "Features (Coming Soon)"
            break
        case 1:
            lbl_title.text = "Features (Existing)"
            break
        case 2:
            lbl_title.text = "Case Studies (Our Work)"
            break
        default:
            break
        }
        
        self.loadTableView(indexPath.row, scrollView: scrollview)
        return cell
    }
    
    func loadTableView(_ point: Int, scrollView: UIScrollView) {
        if point == 0 {
            var page = 0
            for item in featureComingArray {
                self.createPageWithImage1(item["image"] as! String, page: CGFloat(page), title: item["title"] as! String, scrollView: scrollView)
                page = page+1
            }
            let w = scrollView.frame.size.width*2/7
            scrollView.contentSize = CGSize(width: w*CGFloat(featureComingArray.count), height: scrollView.frame.size.height)
        }
        else if point == 1 {
            var page = 0
            for item in featureExistingArray {
                self.createPageWithImage2(item["image"] as! String, page: CGFloat(page), title: item["title"] as! String, scrollView: scrollView)
                page = page+1
            }
            let w = scrollView.frame.size.width*5/8
            scrollView.contentSize = CGSize(width: w*CGFloat(featureExistingArray.count), height: scrollView.frame.size.height)
        }
        else if point == 2 {
            var page = 0
            for item in caseStudiesArray {
                self.createPageWithImage3(item["image"] as! String, page: CGFloat(page), title: item["title"] as! String, scrollView: scrollView)
                page = page+1
            }
            let w = scrollView.frame.size.width*2/7
            scrollView.contentSize = CGSize(width: w*CGFloat(caseStudiesArray.count), height: scrollView.frame.size.height)
        }
    }
    
    func createPageWithImage1(_ imageURL: String, page: CGFloat, title: String, scrollView: UIScrollView) {
        let w = scrollView.frame.size.width*2/7
        let imageView = UIImageView(frame: CGRect.init(x: w * page+5, y: 0.0, width: w-10, height: scrollView.frame.size.height-30))
        
        DispatchQueue.main.async {
            SDWebImageManager.shared().downloadImage(with: URL(string: imageURL), options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                imageView.image = UIImage(named: "loading_img")
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if image != nil {
                    imageView.image = image
                }
            })
        }
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.layer.masksToBounds = true
        
        let lbl_title = UILabel(frame: CGRect(x: imageView.frame.origin.x, y: imageView.frame.height+5, width: imageView.frame.size.width, height: 20.0))
        lbl_title.text = title
        lbl_title.textColor = UIColor.white
        lbl_title.font = UIFont.systemFont(ofSize: 12)
        lbl_title.textAlignment = .center
        scrollView.addSubview(imageView)
        scrollView.addSubview(lbl_title)
        let button = UIButton(frame: imageView.frame)
        scrollView.addSubview(button)
        button.tag = Int(page)
        button.addTarget(self, action: #selector(TrendingVC.onclick1(_:)), for: .touchUpInside)
    }
    
    func onclick1(_ sender: UIButton) {
        
        selectedVideo = featureComingArray[sender.tag]
        
        performSegue(withIdentifier: ShowWebViewControllerSegueIdentifer, sender: self)
    }
    
    func createPageWithImage2(_ imageURL: String, page: CGFloat, title: String, scrollView: UIScrollView) {
        let w = scrollView.frame.size.width*5/8
        let imageView = UIImageView(frame: CGRect.init(x: w * page+5, y: 30.0, width: w-10, height: scrollView.frame.size.height-30))
        
        DispatchQueue.main.async {
            SDWebImageManager.shared().downloadImage(with: URL(string: imageURL), options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                imageView.image = UIImage(named: "loading_img")
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if image != nil {
                    imageView.image = image
                }
            })
        }
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.layer.cornerRadius = 5
        imageView.layer.masksToBounds = true
        
        let lbl_title = UILabel(frame: CGRect(x: imageView.frame.origin.x, y: 0.0, width: imageView.frame.size.width, height: 20.0))
        lbl_title.text = title
        lbl_title.textColor = UIColor.white
        lbl_title.font = UIFont.systemFont(ofSize: 15)
        lbl_title.textAlignment = .left
        scrollView.addSubview(imageView)
        scrollView.addSubview(lbl_title)
        let button = UIButton(frame: imageView.frame)
        scrollView.addSubview(button)
        button.tag = Int(page)
        button.addTarget(self, action: #selector(TrendingVC.onclick2(_:)), for: .touchUpInside)
    }
    func onclick2(_ sender: UIButton) {
        
        selectedVideo = featureExistingArray[sender.tag]
        
        performSegue(withIdentifier: ShowWebViewControllerSegueIdentifer, sender: self)
    }
    
    
    func createPageWithImage3(_ imageURL: String, page: CGFloat, title: String, scrollView: UIScrollView) {
        let w = scrollView.frame.size.width*2/7
        let imageView = UIImageView(frame: CGRect.init(x: w * page+5, y: 0.0, width: w-10, height: scrollView.frame.size.height-30))
        
        DispatchQueue.main.async {
            SDWebImageManager.shared().downloadImage(with: URL(string: imageURL), options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                imageView.image = UIImage(named: "loading_img")
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if image != nil {
                    imageView.image = image
                }
            })
        }
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.layer.masksToBounds = true
        
        let lbl_title = UILabel(frame: CGRect(x: imageView.frame.origin.x, y: imageView.frame.height+5, width: imageView.frame.size.width, height: 20.0))
        lbl_title.text = title
        lbl_title.textColor = UIColor.white
        lbl_title.font = UIFont.systemFont(ofSize: 12)
        lbl_title.textAlignment = .center
        scrollView.addSubview(imageView)
        scrollView.addSubview(lbl_title)
        let button = UIButton(frame: imageView.frame)
        scrollView.addSubview(button)
        button.tag = Int(page)
        button.addTarget(self, action: #selector(TrendingVC.onclick3(_:)), for: .touchUpInside)
    }
    func onclick3(_ sender: UIButton) {
        
        selectedVideo = caseStudiesArray[sender.tag]
        
        performSegue(withIdentifier: ShowWebViewControllerSegueIdentifer, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // get a reference to the second view controller
        if (segue.identifier == ShowWebViewControllerSegueIdentifer) {
            // set a variable in the second view controller with the String to pass
            let inspirationDetailVC = segue.destination as! InspirationDetailVC
            
            inspirationDetailVC.mID = selectedVideo["id"] as! String
            inspirationDetailVC.mTitle = selectedVideo["title"] as! String
            inspirationDetailVC.mContent = selectedVideo["content"] as! String
            inspirationDetailVC.mAuthor = selectedVideo["author"] as! String
            inspirationDetailVC.mImage = selectedVideo["image"] as! String
            inspirationDetailVC.mLink = selectedVideo["link"] as! String
            inspirationDetailVC.mReadMore = selectedVideo["label"] as! String
            
            
            print("ID = %@", selectedVideo["id"] as! String)
            print("TITLE = %@", selectedVideo["title"] as! String)
            print("CONTENT = %@", selectedVideo["content"] as! String)
            print("AUTHOR = %@", selectedVideo["author"] as! String)
            print("IMAGE = %@", selectedVideo["image"] as! String)
            print("LINK = %@", selectedVideo["link"] as! String)
            print("ReadMore = %@", selectedVideo["label"] as! String)
            
        }
    }
    //
    //    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    //
    //    }
    //
    //    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
    //
    //    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
