//
//  UpdateCardVC.swift
//  MayaStojan
//

import Foundation
import UIKit


class UpdateCardVC: UIViewController {
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var cardNo: UITextField!
     @IBOutlet weak var phoneNo: UITextField!
    
    var urlString: String!
    
    fileprivate let ShowWKWebViewControllerSegueIdentifer = "showWKWebView"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0 , width: 50, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "logo_slim.png")
        imageView.image = image
        
        navigationItem.titleView = imageView
        
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back_arrow.png"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(popSelf), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 15, y: 20, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let buttonRight = UIButton.init(type: .custom)
        buttonRight.setImage(UIImage.init(named: ""), for: UIControlState.normal)
        buttonRight.frame = CGRect.init(x: 15, y: 20, width: 24, height: 24) //CGRectMake(0, 0, 30, 30)
        buttonRight.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15)
        let barButtonRight = UIBarButtonItem.init(customView: buttonRight)
        self.navigationItem.rightBarButtonItem = barButtonRight
        
        let userDefaults = UserDefaults.standard
        
        self.cardNo.text = userDefaults.string(forKey: "cardNo");
        
        self.phoneNo.text = userDefaults.string(forKey: "phoneNo");

        
    }
    
    func popSelf() {
        self.dismiss(animated: true, completion: nil);
    }
    
    @IBAction fileprivate func saveBtnPressed(_ button: UIButton) {
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(self.cardNo.text, forKey: "cardNo")
         userDefaults.set(self.phoneNo.text, forKey: "phoneNo")
        userDefaults.synchronize()
        
        
        // Create the alert controller
        let alertController = UIAlertController(title: "", message: "Saved successfully", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            //self.dismiss(animated: true, completion: nil);
             self.dismiss(animated: true, completion: nil);
        }
        // Add the actions
        alertController.addAction(okAction)
        //alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction fileprivate func viewBtnPressed(_ button: UIButton) {
        let userDefaults = UserDefaults.standard
        
        urlString = userDefaults.string(forKey: "URLLINK");
        print("URLLINK: ",urlString)
        performSegue(withIdentifier: ShowWKWebViewControllerSegueIdentifer,sender:"http://"+urlString);
        
    }
    
    @IBAction fileprivate func appWrapperHtmlBtnPressed(_ button: UIButton) {
        
        performSegue(withIdentifier: ShowWKWebViewControllerSegueIdentifer,sender:"https://getthinapp.com/push/thinapp/appwrapr.html");
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? WKWebViewVC,
            let URLString = sender as? String {
            controller.URLString = URLString
        }
    }
}

