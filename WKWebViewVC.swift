//
//  WKWebViewVC.swift
//  MayaStojan
//

import UIKit
import WebKit
import SVProgressHUD

class WKWebViewVC: UIViewController , WKNavigationDelegate  {
    

    var URLString: String!
    
    @IBOutlet var backView: UIView!
    
    fileprivate var currentURL: URL?
    
    var webView: WKWebView?
    
    fileprivate let SCREEN_SIZE = UIScreen.main.bounds
    fileprivate let GAP_BETWEEN_VIEWS:CGFloat = 0.08
    
    
    var button: UIButton! = nil
    
    var isHighLighted:Bool = false
    
    override func viewDidDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
    func setupWebView() {
        
        webView = WKWebView()
        self.view = webView!
        webView!.navigationDelegate = self
        webView!.backgroundColor = UIColor.black
        webView!.backgroundColor = UIColor(red:0.0, green:0.0, blue:0.0, alpha:1)
        webView!.scrollView.backgroundColor = UIColor(red:0.0, green:0.0, blue:0.0, alpha:1)
  
        //webView?.isOpaque = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupWebView()
        
        //let image = UIImage(named: "back_btn") as UIImage?
        button = UIButton(frame: CGRect(x: 15, y: 20, width: 24, height: 24))
        //button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(popSelf), for: .touchUpInside)
        
        //NORMAL
        //button.setImage(UIImage(named:"back_image.png"),for:UIControlState.normal)
        
        // TINT BUTTON COLOR
        let origImage = UIImage(named: "back_arrow_red.png");
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        button.setImage(tintedImage, for: .normal)
        //button.tintColor = UIColor.red
        button.tintColor =  UIColor(red: 255/0.0, green: 0/0.0, blue: 0/0.0, alpha: 1.0)
        
        
        //SELECTED
        //button.setImage(UIImage(named:"back_btn.png"),for:UIControlState.highlighted)

        self.view.addSubview(button)
        
        //button.isHidden = true
       
        self.reloadWebViewWithURLString(URLString)
        
        print("URLString: ",URLString)
        
        
    }
    
    func popSelf() {
        
        //print("Button tapped")
        super.dismiss(animated: true, completion: nil);
    
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)?)
    {
        if self.presentedViewController != nil
        {
            super.dismiss(animated: flag, completion: completion)
        }
    }
    
    fileprivate func reloadWebViewWithURLString(_ string: String) {
        guard let requestURL = URL(string: string) else {
            return
        }
        
        //let request = URLRequest(url: requestURL, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 15.0)
        let request = URLRequest(url: requestURL, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 15.0)
        
        self.webView!.load(request)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        SVProgressHUD.show(withStatus: "Loading...")
        //button.isHidden = true
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
        //button.isHidden = false
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        SVProgressHUD.dismiss()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if (navigationAction.request.url!.absoluteString.hasPrefix("completed://")) {
            self.navigationController!.popViewController(animated: true)
        }
        decisionHandler(WKNavigationActionPolicy.allow)
    }

}
