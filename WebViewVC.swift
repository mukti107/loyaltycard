//
//  WebViewVC.swift
//  MayaStojan
//

import UIKit
import WebKit
import SVProgressHUD



class WebViewVC: UIViewController, UIWebViewDelegate {
    var refreshController: UIRefreshControl!
    @IBOutlet weak var webView: UIWebView!
    // This variable will hold the data being passed from the First View Controller
    var receivedString = ""
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
    @objc private func refreshWebView() {
        print("Reloading...")
        
        webView.reload()
    
        self.refreshController.endRefreshing()
        
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        
        webView.backgroundColor = UIColor.white
        
        refreshController = UIRefreshControl()
        refreshController.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshController.backgroundColor  = UIColor.white
        
        refreshController.addTarget(self, action: #selector(WebViewVC.refreshWebView), for: .valueChanged)
        
        refreshController.attributedTitle = NSAttributedString(string: "Pull down to refresh...")
        webView.scrollView.addSubview(refreshController)
        
        if let url = URL(string: receivedString) {
            
            let request = URLRequest(url: url)
            
            let status = Reach().connectionStatus()
            switch status {
            case .unknown, .offline:
                print("Not connected")
                
                // Create the alert controller
                let alertController = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                    self.dismiss(animated: true, completion: nil);
                }
                // Add the actions
                alertController.addAction(okAction)
                //alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
                
            case .online(.wwan):
                print("Connected via WWAN")
                print("Internet Connection Available!")
                webView.loadRequest(request)
                print(request)
                
            case .online(.wiFi):
                print("Connected via WiFi")
                print("Internet Connection Available!")
                webView.loadRequest(request)
                print(request)
            }
        }
    }
    
    
    @IBAction fileprivate func backButtonPressed(_ buttn: UIButton) {
        self.dismiss(animated: true, completion: nil);
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        SVProgressHUD.show(withStatus: "Loading...")
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        SVProgressHUD.dismiss()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        SVProgressHUD.dismiss()
    }
}
