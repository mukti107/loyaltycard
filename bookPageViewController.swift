//
//  bookPageViewController.swift
//  MayaStojan
//

import UIKit
import SDWebImage
import SVProgressHUD

var IS_LOOK_BOOK = false
class bookPageViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate {
    var pageController: UIPageViewController!
    var controllers = [UIViewController]()
    @IBOutlet weak var pageImage: UIImageView?
    var totalPages: Int = 12;
    var currentPage : Int = 1;
    
    var photos: [GalleryModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageController = UIPageViewController(transitionStyle: .pageCurl, navigationOrientation: .horizontal, options: nil)
        pageController.dataSource = self
        pageController.delegate = self
        
        addChildViewController(pageController)
        view.addSubview(pageController.view)
        
        
        self.edgesForExtendedLayout = []
        
        let btn: UIButton = UIButton(frame: CGRect(x: 15, y: 30, width: 24, height: 24))
        btn.setImage(UIImage.init(named: "back_arrow_red.png"), for: UIControlState.normal)
        btn.addTarget(self, action: #selector(popSelf), for: .touchUpInside)
        self.view.addSubview(btn)
        
        let views = ["pageController": pageController.view] as [String: AnyObject]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[pageController]|", options: [], metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[pageController]|", options: [], metrics: nil, views: views))
        
        loadImages()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadImages()
        if(ADMIN){
            let btn: UIButton = UIButton(frame: CGRect(x: self.view.frame.size.width - 15 - 24, y: 30, width: 24, height: 24))
            btn.setImage(UIImage.init(named: "icon_edit_red"), for: UIControlState.normal)
            btn.addTarget(self, action: #selector(openGalleryPage), for: .touchUpInside)
            self.view.addSubview(btn)
        }
    }
    func loadImages(){

        let params = ["app_id":KAppID, "category_id":0, "is_look_book":true] as [String : Any]
        SVProgressHUD.show()
        NetworkManager.sharedClient.getRequest(tag: "gallery", parameters: params) { (error, result) in
            SVProgressHUD.dismiss()
            
            if (error != "") {
                self.showAlert(withTitle: "Error", message: error)
            }
            else{
                self.photos.removeAll()
                var items = result.value(forKey: "photos") as! Array<Dictionary<String, AnyObject>>
                
                for i in 0 ..< items.count {
                    let item = (items[i] as Dictionary<String, AnyObject>)
                    let model = GalleryModel()
                    model.initWithDictionary(data: item)
                    self.photos.append(model)
                }
                self.initScrollView()
            }
        }
        
    }
    
    func initScrollView(){
        if(self.photos.count == 0){
            return
        }

        controllers.removeAll()
        for model in photos{
            
            let vc = BookPageVC()
            vc.initPageWithImage(image: model.image)
           
            controllers.append(vc)
        }
        
        pageController.setViewControllers([controllers[0]], direction: .forward, animated: false)
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        if let theImage = scrollView.viewWithTag(2) as? UIImageView {
            
            return theImage
        }
            
        else {
            
            return nil
            
        }
        
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let index = controllers.index(of: viewController) {
            if index > 0 {
                currentPage = (index - 1) + 1
                
                return controllers[index - 1]
                
            } else {
                return nil
            }
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let index = controllers.index(of: viewController) {
            if index < controllers.count - 1 {
                currentPage = (index + 1) + 1
                return controllers[index + 1]
                
            } else {
                return nil
            }
            
        }
        
        return nil
    }
    
    func randomCGFloat() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
    
    func randomColor() -> UIColor {
        return UIColor(red: randomCGFloat(), green: randomCGFloat(), blue: randomCGFloat(), alpha: 1)
    }
    
    func popSelf() {
        //self.dismiss(animated: true, completion: nil);
        //  navigationController?.popToRootViewController(animated: true)
        //_ = navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil);
        
    }
    
    func openGalleryPage(){
        IS_LOOK_BOOK = true
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "GalleryNav") as! UINavigationController
        self.present(nextViewController, animated:true, completion:nil)
    }
}

class BookPageVC: UIViewController{
    func initPageWithImage(image:String){
        let pageImage = UIImageView()
        
        let imageUrl  =  downloadUrlWithFileName(fileName: image)
        let placeHolder:UIImage = UIImage(named:"default_image")!
        pageImage.sd_setImage(with: imageUrl, placeholderImage: placeHolder)
        
        pageImage.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        
        
        pageImage.contentMode = UIViewContentMode.scaleAspectFill
        
        let scrollImg: UIScrollView = UIScrollView()
        //scrollImg.delegate = self
        scrollImg.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height)
        
        scrollImg.alwaysBounceVertical = false
        scrollImg.alwaysBounceHorizontal = false
        scrollImg.showsVerticalScrollIndicator = true
        scrollImg.flashScrollIndicators()
        scrollImg.contentSize = CGSize(width: self.view.bounds.width, height: self.view.bounds.height)
        
        
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
        
        pageImage.tag = 2
        scrollImg.addSubview(pageImage)
        self.view.addSubview(scrollImg)
    }
}

